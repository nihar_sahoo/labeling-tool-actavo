import arcpy
import math
import sys
from datetime import datetime
from operator import sub
import os
import arcgis
from arcgis.gis import GIS



x=10000
sys.setrecursionlimit(x)

now = datetime.now()
start_time = now.strftime("%H:%M:%S")

same = lambda p1, p2, tol: sum(x**2 for x in map(sub, p1, p2)) <= tol**2

arcpy.env.preserveGlobalIds = True

# arcpy.env.workspace = r"C:\Users\user\Documents\ArcGIS\Projects\Test\Test.gdb"
# Splice_Closures = "Splice_Closure"
# Fibre_Sheath = "Fibre_Sheath"

Splice_Closures = arcpy.GetParameterAsText(0)
Fibre_Sheath = arcpy.GetParameterAsText(1)
check_label = arcpy.GetParameterAsText(2)


def TruncateWebLayer(gis=None, target=None):
    try:
        lyr = arcgis.features.FeatureLayer(target, gis)
        arcpy.AddMessage(lyr)
        #lyr.manager.truncate()
        arcpy.AddMessage("Successfully truncated layer: " + str(target))
    except:
        arcpy.AddMessage("Failed truncating: exception")
        arcpy.AddMessage("Failed truncating: " + str(target))
        sys.exit()

def ApplyUpdates_old(arr_list, fls, field):
    status = False
    me = mygis.users.me
    arcpy.AddMessage("Updating Sp2_Label field to Server.")
    try:
        for rec in arr_list:
            fset = fls.query(where="OBJECTID=" + str(rec[0]))

            sfo_feature = [f for f in fset][0]
            sfo_feature.attributes

            sfo_edit = sfo_feature
            sfo_edit.attributes[field[0]] = rec[1]
            #sfo_edit.attributes[field[1]] = rec[2]
            sfo_edit.attributes['Editor'] = me.fullName

            update = fls.edit_features(updates=[sfo_edit])

            status = update["updateResults"][0]["success"]
    except Exception as ex:
        arcpy.AddMessage(ex)

    return status

# Function to apply updates
def ApplyUpdates(fl, ft, flds, dict):
    features_to_be_updated = []  # Create empty list for features to be updated
    # Create list of all features that match globalid in gID list
    cntTotalUpdates = 0  # Reset total rumber of updates
    cntPacket = 0  # Reset data packet number
    cntFeatures = 0  # Count total features searched
    while cntTotalUpdates < len(list(dict.keys())):
        cntUpdates = 0  # Set updates for new data packet to zero
        features_to_be_updated = []  # Create empty list for features to be updated
        # for edit_item in features: # Loop items in the features object
        while cntUpdates <= 2000 and cntFeatures < len(ft):  # Loop through 2000 records at a time
            edit_item = ft[cntFeatures]  # Set the item to edit
            gID = edit_item.attributes['GlobalID']  # Set the gloabl id
            if gID in list(dict.keys()):  # If feature gid is in the list of Gids
                toEdit = 0  # Reset flag to inditcate if record is to be updated/edited
                # if there is only one field to update...
                if len(flds) == 1:
                    item = flds[0]
                    valExist = edit_item.attributes[item]  # Exising value
                    valProp = dict[gID]  # Proposed value
                    if str(valExist) != str(valProp):  # Check if the existing value is the same as the proposed
                        edit_item.attributes[
                            item] = valProp  # If not then update the existing value to the proposed
                        toEdit = 1
                else:  # If there is more than one field to update
                    cntLstFlds = 0  # Set list item count to 0
                    for item in flds:  # loop all items in list of fields
                        valExist = edit_item.attributes[item]  # Exising value
                        valProp = dict[gID][cntLstFlds]  # Proposed value
                        if str(valExist) != str(valProp):  # Check if the existing value is the same as the proposed
                            edit_item.attributes[
                                item] = valProp  # If not then update the existing value to the proposed
                            toEdit = 1
                        cntLstFlds = cntLstFlds + 1  # increwment the list of fields index
                if toEdit == 1:
                    features_to_be_updated.append(edit_item)  # Add this edit to list of edits
                    cntUpdates = cntUpdates + 1  # increment the updates by packet counter
                    cntTotalUpdates = cntTotalUpdates + 1  # increment the total updates counter

            cntFeatures = cntFeatures + 1  # Increment the feature count

        if cntFeatures == len(
                ft) and cntUpdates == 0:  # if all records have been checked and there are no records to update then break out of while loop
            break
        cntPacket = cntPacket + 1  # increment the count of number of data packets
        # update the feature with the relevant edits
        cnt = 1  # Set the update attempt counter to 1
        while True and len(
                features_to_be_updated) > 0:  # While the features to be updated list has values perform the update
            arcpy.AddMessage(
                "Updating Feature Layer: data packet " + str(cntPacket) + ", attempt " + str(cnt) + "...")
            try:
                update_result = fl.edit_features(updates=features_to_be_updated)  # Apply updates to feature layer
            except Exception as e:  # Exception if there is an error updating
                arcpy.AddMessage(str(e))
                cnt = cnt + 1
            else:
                break
            if cnt == 20:
                print("Exiting after %s failed attempts" % cnt, ERROR=True)
                exit()

    arcpy.AddMessage("Total features updated: " + str(cntTotalUpdates))

aprx = arcpy.mp.ArcGISProject("CURRENT")

# Get pole item from AGOL
desc = arcpy.Describe(Splice_Closures)
arcpy.AddMessage(desc)

#to_array = [char for char in desc.name]

#scPath = desc.path +'/'+ to_array[1] # Get url to pole feature service
scPath = desc.catalogPath # Get url to pole feature service

arcpy.AddMessage("scpath")
arcpy.AddMessage(scPath)

gdbLocal = aprx.defaultGeodatabase
arcpy.AddMessage(gdbLocal)
gdbPath = aprx.homeFolder
gdbName = os.path.basename(gdbPath)
if not arcpy.Exists(gdbLocal):
    arcpy.CreateFileGDB_management(gdbPath, gdbName)

# Create local features and set these to variables
mygis = arcgis.gis.GIS("Pro")

# the URL of a single feature layer within a collection in an AGOL portal
publishedWebLayer = scPath


TruncateWebLayer(mygis, publishedWebLayer)

# reference the empty layer as FeatureLayer object from the ArcGIS Python API
fl = arcgis.features.FeatureLayer(publishedWebLayer, mygis)

arcpy.AddMessage("Creating local copies of the parameters...")

# Export only chambers connecting to Sheaths or Drops
arcpy.env.preserveGlobalIds = True  # Set the preserveGlobalIds environment to True
#
ScName = "Splice_Closure"
FcName = "Fibre_Sheath"
#
#
p1 = aprx.defaultGeodatabase

p2 = p1 + "\\" + ScName
p3 = p1 + "\\" + FcName

# Local Copy of Splice Closure
localSplice = os.path.join(gdbLocal, Splice_Closures)  # set the local feature class path
if arcpy.Exists(localSplice):
    arcpy.Delete_management(localSplice)
feats = arcpy.FeatureSet(table=Splice_Closures)
feats.save(p2)
arcpy.SelectLayerByAttribute_management(Splice_Closures,"CLEAR_SELECTION")


# Local Copy of Splice Closure
localFibre = os.path.join(gdbLocal, Fibre_Sheath)  # set the local feature class path
if arcpy.Exists(localFibre):
    arcpy.Delete_management(localFibre)
feats1 = arcpy.FeatureSet(table=Fibre_Sheath)
feats1.save(p3)
arcpy.SelectLayerByAttribute_management(Fibre_Sheath,"CLEAR_SELECTION")

def remove_dup_by_first_element(list1):
    first_element_list = []
    list2 = []
    for x in list1:
        if x[0] not in first_element_list:
            list2.append(x)
            first_element_list.append(x[0])
    return list2

try:
    lstFields_sc = arcpy.ListFields(ScName)
    field_names_sc = [f.name for f in lstFields_sc]
    if "OriginalOID" not in field_names_sc:
        arcpy.AddField_management(ScName, "OriginalOID", "LONG", 10)
        arcpy.CalculateField_management(ScName, "OriginalOID", "!OBJECTID!", "PYTHON3")
    else:
        pass
except Exception as ex:
    arcpy.AddMessage(ex)

try:
    lstFields_fc = arcpy.ListFields(FcName)
    field_names_fc = [f.name for f in lstFields_sc]
    if "OriginalOID" not in field_names_fc:
        arcpy.AddField_management(FcName, "OriginalOID", "LONG", 10)
        arcpy.CalculateField_management(FcName, "OriginalOID", "!OBJECTID!", "PYTHON3")
    else:
        pass
except Exception as ex:
    arcpy.AddMessage(ex)

#fields_sc = ("OriginalOID", "label", "sp1_label", "sp2_label", "SHAPE@XY", "enc_type")
#fields_fs = ("OriginalOID", "type", "SHAPE@XY")
#fields_fs = ("OriginalOID", "type", "ODP1_Count", "SHAPE@XY", "Cable_type", "ODP2_Count")
fields_sc = ("OriginalOID", "label", "sp1_label", "sp2_label", "SHAPE@XY", "enc_type")
fields_fs = ("OriginalOID", "type", "SHAPE@XY", "Cable_type")
fields_fs1 = ("OriginalOID", "label", "temp_cable_class", "SHAPE@XY")

arr_sc = arcpy.da.FeatureClassToNumPyArray(ScName, fields_sc, skip_nulls=False)
print(arr_sc)

arr_fs = arcpy.da.FeatureClassToNumPyArray(FcName, fields_fs1, skip_nulls=True, explode_to_points=True)

##########################
arr_fs_start = remove_dup_by_first_element(arr_fs)
#print(arr_fs_start)
## arr_fs has multiple entries as the number of vertices for each fiber polyline
## reverse it and remove duplicates to filter the entries with only one entry containing end coordinates for each fiber
arr_fs_end1 = arr_fs
arr_fs_end2 = arr_fs_end1[::-1]
arr_fs_end2 = remove_dup_by_first_element(arr_fs_end2)
arr_fs_end = arr_fs_end2[::-1]

print('len(arr_fs_end)')
print(len(arr_fs_end))
print(arr_fs_end)
#arcpy.AddMessage(arr_sc)
# for x in arr_sc:
#     print(str(x[0])+'-'+str(x[4]))

# Find coordinate of the fibre sheath
def find_coor_from_id_fs(id_fs1, arr_fs1):
    global coor1
    for x in arr_fs1:
        if x[0] == id_fs1:
            coor1 = x[3]
    return coor1

# ##find next splice closure id by comparing coordinates
def find_next_sc(coor_end1, arr_sc1):
    global id_sc1
    for x in arr_sc1:
        if same(coor_end1, x[4], 0.1):
            id_sc1 = x[0]
    return id_sc1



def find_next_sc_id_from_fs_id(id_fs, arr_sc, arr_fs_end):
    global id_sc
    coor_sc = find_coor_from_id_fs(id_fs,arr_fs_end)
    print(id_fs)
    print(coor_sc)
    for x in arr_sc:
        if same(x[4], coor_sc, 0.1):
            id_sc = x[0]
    return id_sc

#Find coordinate of the splice closure
def find_coor_from_id_sc(id_sc1,arr_sc1):
    global coor1
    for x in arr_sc1:
        if x[0] == id_sc1:
            coor1 = x[4]
    return coor1

def find_enc_type(id_sc1,arr_sc1):
    global enc_type1
    for x in arr_sc1:
        if x[0] == id_sc1:
            enc_type1 = x[5]
    return enc_type1

def find_front_fs_from_sc(id_sc1, arr_sc1, arr_fs_start):
    #create a blank array
    id_fs1_list = []
    #print(arr_fs_start)
    coor_sc1 = find_coor_from_id_sc(id_sc1, arr_sc1)
    print(id_sc1)
    print(coor_sc1)
    ##check equality with start coordinate of fiber sheath array
    for x in arr_fs_start:
        if same(coor_sc1, x[3], 0.1):
            if x[0] not in id_fs1_list:
                id_fs1_list.append(x[0])
    return id_fs1_list

def find_front_fs_from_sc_secondary(id_sc1, arr_sc1, arr_fs_start, array):
    #create a blank array
    id_fs1_list = []
    #print(arr_fs_start)
    coor_sc1 = find_coor_from_id_sc(id_sc1, arr_sc1)
    ##check equality with start coordinate of fiber sheath array
    for x in arr_fs_start:
        if same(coor_sc1, x[3], 0.1):
            if x[0] not in id_fs1_list:
                id_fs1_list.append(x[0])
    return id_fs1_list

# # Recursive function to collect all ODP1 splice closure
# # it will take input of splice closure and find the sheaths connected with from Label and From fields
# def find_next(fiber_list_checked, id_sc1, arr_sc1, arr_fs_end, ODP1_list, arr_fs_start, primary_splitter):
#     fiber_front_list = find_front_fs_from_sc(id_sc1, arr_sc1, arr_fs_start)
#     #print(fiber_front_list)
#     #remove fibers which are already covered
#     for x in fiber_front_list:
#         if x in fiber_list_checked:
#             fiber_front_list.remove(x)
#     #drop_path_list = drop_path_list1
#     if len(fiber_front_list) > 0:
#         for x in fiber_front_list:
#             if x not in fiber_list_checked:
#                 fiber_list_checked.append(x)
#                 print(x)
#                 sc_ids = find_next_sc_id_from_fs_id1(x, arr_sc1, arr_fs_end)
#                 print(sc_ids)
#                 if sc_ids != None:
#                     # if id_sc1 in primary_splitter:
#                     #     break
#                     ODP1_list.append(sc_ids)
#                     ODP1_list = find_next(fiber_list_checked, sc_ids, arr_sc1, arr_fs_end, ODP1_list, arr_fs_start, primary_splitter)
#     #print(ODP1_list)
#     #print("check")
#     return ODP1_list

# def find_next(fiber_list_checked, id_sc1, arr_sc1, arr_fs_end, ODP1_list, arr_fs_start, array_sc_id1, counter, check_count):
#     fiber_front_list = find_front_fs_from_sc(id_sc1, arr_sc1, arr_fs_start)
#     print(fiber_front_list)
#     #remove fibers which are already covered
#     for x in fiber_front_list:
#         if x in fiber_list_checked:
#             fiber_front_list.remove(x)
#     if len(fiber_front_list) > 0:
#         for x in fiber_front_list:
#             if x not in fiber_list_checked:
#                 fiber_list_checked.append(x)
#                 id_sc1 = find_next_sc_id_from_fs_id(x, arr_sc1, arr_fs_end)
#                 arcpy.AddMessage(id_sc1)
#                 if id_sc1 != None:
#                     # if len(ODP1_list) == 8:
#                     #     break
#                     #enclosure_type = find_enc_type(id_sc1, arr_sc)
#                     # if enclosure_type == 1 or enclosure_type == 5 and id_sc1 not in ODP1_list:
#                     #     break
#                     # if id_sc1 in array_sc_id1:
#                     #     arcpy.AddMessage("sc id")
#                     #     arcpy.AddMessage(id_sc1)
#                     #     array_sc_id1.remove(int(id_sc1))
#                     #     break
#                     #check_count = check_count + 1
#                     #if str(check_count) == str(counter):
#                     #    break
#                     for x in fiber_front_list:
#                         if x in fiber_list_checked:
#                             fiber_front_list.remove(x)
#                     if len(fiber_front_list) > 0:
#                         for x in fiber_front_list:
#                             if x not in fiber_list_checked:
#                                 fiber_list_checked.append(x)
#                                 id_sc1 = find_next_sc_id_from_fs_id(x, arr_sc1, arr_fs_end)
#                     ODP1_list.append(id_sc1)
#                 else:
#                     ODP1_list.append(id_sc1)
#
#                 ODP1_list = find_next(fiber_list_checked, id_sc1, arr_sc1, arr_fs_end, ODP1_list, arr_fs_start, array_sc_id1, counter, check_count)
#     print(ODP1_list)
#     #print("check")
#     return ODP1_list

def find_next(fiber_list_checked, id_sc1, arr_sc1, arr_fs_end, ODP1_list, primary_list, cable_list):
    fiber_front_list = find_front_fs_from_sc(id_sc1, arr_sc1, arr_fs_start)
    if len(cable_list) != 0:
        for k in cable_list:
            if k in fiber_front_list:
                fiber_front_list.remove(k)
                fiber_front_list.append(k)
    print(fiber_front_list)
    #remove fibers which are already covered
    for x in fiber_front_list:
        if x in fiber_list_checked:
            fiber_front_list.remove(x)
    #drop_path_list = drop_path_list1
    if len(fiber_front_list) > 0:
        for x in fiber_front_list:
            if x not in fiber_list_checked:
                fiber_list_checked.insert(1, x)
                last_id = id_sc1
                id_sc1 = find_next_sc_id_from_fs_id(x, arr_sc1, arr_fs_end)
                #print(id_sc1)
                if id_sc1 != None:
                    # if len(ODP1_list) == 8:
                    #      break
                    if id_sc1 not in primary_list:
                        ODP1_list.append(id_sc1)
                    print("ODP1_list")
                    print(ODP1_list)
                    print(len(fiber_front_list))
                    enclosure_type = find_enc_type(id_sc1, arr_sc)
                    if len(ODP1_list) == 0 and (enclosure_type == 1 or enclosure_type == 5) and id_sc1 in primary_list:
                        if len(fiber_front_list) >= 1:
                            is_all_primary = False
                            p_id_list = []
                            p_list = []
                            for rec_x in fiber_front_list:
                                sc_id = find_next_sc_id_from_fs_id(rec_x, arr_sc1, arr_fs_end)
                                p_id_list.append(sc_id)
                                if sc_id in primary_list:
                                    is_all_primary = True
                                    p_list.append(is_all_primary)

                            if len(p_id_list) == len(p_list) and len(p_id_list) != 0:
                                break
                            if len(fiber_front_list) == 0:
                                break
                        fiber_list_checked.remove(x)
                        cable_list.append(x)
                        print("last_id"+str(last_id))
                        id_sc1 = last_id

                        #ODP1_list = find_next([], last_id, arr_sc1, arr_fs_end, ODP1_list, primary_list, cable_list)
                    if len(ODP1_list) != 0:
                        if (enclosure_type == 1 or enclosure_type == 5) and id_sc1 in primary_list and len(fiber_front_list) == 1:
                            break
                    if len(ODP1_list) != 0:
                        if id_sc1 not in primary_list:
                            if len(fiber_front_list) >= 1:
                                is_all_primary1 = False
                                p_id_list1 = []
                                p_list1 = []
                                for rec_x in fiber_front_list:
                                    sc_id = find_next_sc_id_from_fs_id(rec_x, arr_sc1, arr_fs_end)
                                    p_id_list1.append(sc_id)
                                    if sc_id in primary_list:
                                        is_all_primary1 = True
                                        p_list1.append(is_all_primary1)

                                if len(p_id_list1) == len(p_list1) and len(p_id_list1) != 0:
                                    break

                    # if enclosure_type == 1 or enclosure_type == 5:
                    ODP1_list = find_next(fiber_list_checked, id_sc1, arr_sc1, arr_fs_end, ODP1_list, primary_list, cable_list)
    print(ODP1_list)
    #print("check")
    return ODP1_list


def find_next_primary(fiber_list_checked, id_sc1, arr_sc1, arr_fs_end, ODP1_list, primary_list, list, array_id):
    fiber_front_list = find_front_fs_from_sc_secondary(id_sc1, arr_sc1, arr_fs_start, fiber_list_checked)
    if len(list) != 0:
        fiber_front_list = list
        list = []
    print(fiber_front_list)
    # remove fibers which are already covered
    for x in fiber_front_list:
        if x in fiber_list_checked:
            fiber_front_list.remove(x)
    # drop_path_list = drop_path_list1
    if len(fiber_front_list) > 0:
        for x in fiber_front_list:
            if x not in fiber_list_checked:
                fiber_list_checked.append(x)
                id_sc1 = find_next_sc_id_from_fs_id(x, arr_sc1, arr_fs_end)
                # print(id_sc1)
                if id_sc1 != None:
                    enclosure_type = find_enc_type(id_sc1, arr_sc)

                    if enclosure_type == 2:
                        ODP1_list.append([id_sc1, x])
                    if enclosure_type == 1 or enclosure_type == 5:
                        id_sc1 = array_id[0]
                    ODP1_list = find_next_primary(fiber_list_checked, id_sc1, arr_sc1, arr_fs_end, ODP1_list, primary_list, list, array_id)
    print(ODP1_list)
    print("check")
    return ODP1_list



try:
    lstFields_sc = arcpy.ListFields(ScName)
    field_names_sc = [f.name for f in lstFields_sc]
    if "OriginalOID" not in field_names_sc:
        arcpy.AddField_management(ScName, "OriginalOID", "LONG", 10)
        arcpy.CalculateField_management(ScName, "OriginalOID", "!OBJECTID!", "PYTHON3")
    if "OriginalRefOID" not in field_names_sc:
        arcpy.AddField_management(ScName, "OriginalRefOID", "LONG", 10)
        arcpy.CalculateField_management(ScName, "OriginalRefOID", "!OBJECTID!", "PYTHON3")
    else:
        pass
except Exception as ex:
    arcpy.AddMessage(ex)

if arcpy.Exists("primary_splitter"):
    arcpy.Delete_management("primary_splitter")
    arcpy.Select_analysis(ScName, "primary_splitter", '"enc_type" in (1, 5)')
else:
    arcpy.Select_analysis(ScName, "primary_splitter", '"enc_type" in (1, 5)')

if arcpy.Exists("primary_splitter"):
    arcpy.AddField_management("primary_splitter", "fs_cable_id", "Text", 100)
    arcpy.AddField_management("primary_splitter", "sc_id", "Text", 1000)

if arcpy.Exists("secondary_splitter"):
    arcpy.Delete_management("secondary_splitter")
arcpy.Select_analysis(Splice_Closures, "secondary_splitter", '"enc_type" in (1, 5, 2)')

Status_check = False

arcpy.AddMessage("Checking Primary Splitter sp1_label field..")
array_splice_ribbon = arcpy.da.FeatureClassToNumPyArray("primary_splitter", ("OriginalOID", "sp1_label"), skip_nulls=False)
array_splitter = []
for data in array_splice_ribbon:
    if str(data[1]) == "None" or str(data[1]) == "":
        Status_check = True
        array_splitter.append(data[0])

if Status_check == False:
    try:
        with arcpy.da.UpdateCursor("primary_splitter", ['OriginalOID', 'temp_ribbon', 'label', 'sp1_label', 'sp2_label', 'sp1_type', 'sp1_type', 'sp2_type']) as cur:
            for row in cur:
                if row[3] != None and row[7] not in(5, ""):
                    label = row[3] + '/1'
                    row[4] = label
                cur.updateRow(row)
            del cur
    except Exception as e:
        arcpy.AddMessage(e)

    #array_primary_splice = arcpy.da.FeatureClassToNumPyArray("primary_splitter", ("OriginalOID", "sp1_label", "sp2_label"), skip_nulls=False)

    array_p_splitter = arcpy.da.FeatureClassToNumPyArray("primary_splitter", ("OriginalOID", "sp1_label", "sp2_label"), skip_nulls=False)

    array_sc_id = []
    array_primary_splitter = []
    for i in array_p_splitter:
        array_sc_id.append(i[0])
        array_primary_splitter.append(i[0])

    if arcpy.Exists("primary_splitter_sheath_join"):
        arcpy.Delete_management("primary_splitter_sheath_join")
    arcpy.SpatialJoin_analysis("primary_splitter", Fibre_Sheath, "primary_splitter_sheath_join", join_operation="JOIN_ONE_TO_MANY", join_type="KEEP_ALL", match_option="INTERSECT")

    array_sheath = []

    array_sc_fs = arcpy.da.FeatureClassToNumPyArray("primary_splitter_sheath_join", ('TARGET_FID', 'JOIN_FID', 'OriginalOID'), skip_nulls=True)


    array_fs_id = []
    array_map = []
    for i in array_sc_fs:
        #array_sc_id.append(i[2])
        array_fs_id.append(i[1])
        array_map.append({i[1]:i[2]})

    array_secondary_splitter = []
    array = []

    print(array_sc_id)

    #ODP1_list = []
    def find_all_secondary_splitter(fs_id, primary_splitter, arr_sc, array_secondary_splitter, arr_fs_end, arr_fs_start):
        id_sc = find_next_sc_id_from_fs_id(fs_id, arr_sc, arr_fs_end, arr_fs_start, primary_splitter)
        print(id_sc)

    # find_all_secondary_splitter(1, array_primary_splitter, arr_sc, array_secondary_splitter, arr_fs_end, arr_fs_start)

    def get_fs_id_coordinate(cordinate, list_id, list, arr_fs_end):
        for x in arr_fs_end:
            if same(x[2], cordinate, 0.1):
                if x[0] not in list_id:
                    list.append(x[0])
                cordinate = x[2]
                #continue
        print(list)

    #Find coordinate of the secondary fibre sheath
    def find_coor_from_id_fs_secondary(id_fs1,arr_fs1):
        global coor1
        for x in arr_fs1:
            if x[0] == id_fs1:
                coor1 = x[3]
        return coor1

    def find_next_sc_id_from_fs_id_secondary(id_fs1, arr_sc1, arr_fs_end):
        global id_sc1
        coor_sc1 = find_coor_from_id_fs_secondary(id_fs1,arr_fs_end)
        for x in arr_sc1:
            if same(x[4], coor_sc1, 0.1):
                id_sc1 = x[0]
        return id_sc1

    def get_fs_ids(sc_id, array_sc, array_fs_id, counter, array_sc_id1):
        array = []
        list = []
        check_count = 1
        # for id in array_sc:
        #     if id[2] == sc_id:
        #         array.append(id[1])
        # for x in array:
        #     #id_sc1 = find_next(x, arr_sc, arr_fs_end)
        # id_sc = find_next_sc_id_from_fs_id_secondary(x, arr_sc, arr_fs_end)
        #print(id_sc)

        # arcpy.AddMessage("array_sc_id")
        # arcpy.AddMessage(array_sc_id1)
        #list1 = find_next([], sc_id, arr_sc, arr_fs_end, list, arr_fs_start, array_sc_id1, counter, check_count)
        rec = find_front_fs_from_sc(sc_id, arr_sc, arr_fs_start)
        list1 = find_next_primary([], sc_id, arr_sc, arr_fs_end, list, array_sc_id1, rec, [sc_id])
        # list.append(sc_id)
        # print(list1)
        for j in list1:
            if j not in list:
                list.append(j)
            #print(list)
            # end_coordinate = find_coor_from_id_fs(i, arr_fs_end)
            # print(end_coordinate)
            # id_array = get_fs_id_coordinate(end_coordinate, array, [], arr_fs_end)
            # #print()
            # find_all_secondary_splitter(i, array_primary_splitter, arr_sc, array_secondary_splitter, arr_fs_end,
            #                             arr_fs_start)
        return list

    def get_val_dict(array, id):
        values = ''
        for key, val in array.items():
            if str(key) == str(id):
                values = val
        return values
    ################## Update Branching Cable IDS To Primary Splitters #########################
    arcpy.SpatialJoin_analysis("primary_splitter", Fibre_Sheath, "splitter_sheath_join", join_operation="JOIN_ONE_TO_ONE", join_type="KEEP_ALL", match_option="INTERSECT")
    array_sheath = arcpy.da.FeatureClassToNumPyArray("splitter_sheath_join", ('OriginalOID', 'JOIN_COUNT'), skip_nulls=True)

    #arcpy.AddMessage(array_sheath)

    for j in array_sheath:
        with arcpy.da.UpdateCursor("primary_splitter", ['OriginalOID', 'fs_cable_id', 'sc_id']) as cur:
            for row in cur:
                if str(row[0]) != str(j[0]):
                    row[1] = int(j[1])
                    cur.updateRow(row)
            del cur

    array_primary_splice = {}
    with arcpy.da.SearchCursor("primary_splitter", ['OriginalOID', 'fs_cable_id', 'sc_id']) as cur:
        for row in cur:
            if row[0] != None:
                arcpy.AddMessage("primary splitter")
                arcpy.AddMessage(row[0])
                # if row[0] in array_sc_id:
                #     array_sc_id.remove(int(row[0]))
                arr_id = get_fs_ids(row[0], array_sc_fs, array_fs_id, row[1], array_sc_id)
                #arr_id = find_next([], row[0], arr_sc, arr_fs_end, list, array_sc_id, [])
                if len(arr_id) > 0:
                    sc_ids = []
                    for id in arr_id:
                        #sc_ids[row[0]] = id
                        sc_ids.append(id)
                        # if sc_ids == "":
                        # else:
                        #     if len(sc_ids) < 500:
                                #sc_ids = str(sc_ids) + "," + str(id)

                    array_primary_splice[row[0]] = sc_ids
                    #encode_sc = rle_encode(str(sc_ids))
                    #print(encode_sc)
                    #if len(sc_ids) < 500:
                    #    row[2] = sc_ids
            #cur.updateRow(row)
        del cur
    arcpy.AddMessage(array_primary_splice)


    array_records = arcpy.da.FeatureClassToNumPyArray("primary_splitter", ('OriginalOID', 'sp1_label', 'sp2_label'), skip_nulls=True)

    #array(secondary Splitter)
    def get_cable_type(all_array, array):
        records = []
        sorted_list = []
        if len(array) > 0 and len(all_array) > 0:
            for rec in array:
                for record in all_array:
                    if str(record[0]) == str(rec[0]):
                        records.append([rec[0], record[1]])
            sorted_list = sorted(records, key=lambda record: record[1])
        return sorted_list

    for recs in array_records:
        sp2_label = recs[2]
        with arcpy.da.UpdateCursor(ScName,
                                   ['OriginalOID', 'enc_type', 'sp1_label', 'sp2_label', 'sp2_type', 'sp1_type']) as cur:
            for row in cur:
                if row[0] != None:
                    if str(row[0]) == str(recs[0]) and row[4] != None:
                        row[3] = sp2_label
                cur.updateRow(row)
            del cur

    array_splitter = []
    for rec in array_records:
        sp1_label_no = rec[1].split('/')
        #sp1_label = rec[1]
        sp2_label = rec[2]
        arcpy.AddMessage("sp2_label")
        arcpy.AddMessage(sp2_label)
        counter = 0
        if sp2_label != "":
            counter = 1
        #arr_splitter = rec[1].split(',')
        arr_splitter = get_val_dict(array_primary_splice, rec[0])
        arcpy.AddMessage(rec[0])
        arcpy.AddMessage(arr_splitter)

        sorted_array_cable_type = get_cable_type(arr_sc, arr_splitter)

        arcpy.AddMessage(sorted_array_cable_type)

        if len(sorted_array_cable_type) > 0:
            for i in sorted_array_cable_type:
                #array_splitter.append(i)
                with arcpy.da.UpdateCursor(ScName, ['OBJECTID', 'enc_type', 'sp1_label', 'sp2_label', 'sp2_type', 'sp1_type', 'label']) as cur:
                    for row in cur:
                        if row[0] != None:
                            if str(row[0]) == str(i[0]) and row[1] == 2:
                                arcpy.AddMessage(row[0])
                                sp1_label = row[6]+"/"+sp1_label_no[len(sp1_label_no) - 1]
                                counter = counter + 1
                                #sp1 = sp1_label + '/' + str(counter)
                                sp1 = sp1_label + '/' + str(counter)
                                if row[4] not in (5, "", None):
                                    counter = counter + 1
                                    #sp2 = sp1_label + '/' + str(counter)
                                    sp2 = sp1_label + '/' + str(counter)
                                    arcpy.AddMessage("sp2")
                                    arcpy.AddMessage(sp2)
                                    if str(row[3]) == "":
                                        row[3] = sp2
                                arcpy.AddMessage("sp1")
                                arcpy.AddMessage(sp1)
                                if str(row[2]) == "":
                                    row[2] = sp1

                        cur.updateRow(row)
                    del cur

        # if len(array_primary_splice) > 0:
        #     for i in arr_splitter:
        #         array_splitter.append(i)
        #         with arcpy.da.UpdateCursor(ScName, ['OBJECTID', 'enc_type', 'sp1_label', 'sp2_label', 'sp2_type', 'sp1_type']) as cur:
        #             for row in cur:
        #                 if row[0] != None:
        #                     if str(row[0]) == str(i[0]) and row[1] == 2:
        #                         if i[1] != "":
        #                             cable_type = get_cable_type(i[1], arr_fs_end)
        #                         arcpy.AddMessage(row[0])
        #                         counter = counter + 1
        #                         sp1 = sp1_label + '/' + str(counter)
        #                         if row[4] not in (5, "", None):
        #                             counter = counter + 1
        #                             sp2 = sp1_label + '/' + str(counter)
        #                             arcpy.AddMessage("sp2")
        #                             arcpy.AddMessage(sp2)
        #                             if str(row[3]) == "":
        #                                 row[3] = sp2
        #                         arcpy.AddMessage("sp1")
        #                         arcpy.AddMessage(sp1)
        #                         if str(row[2]) == "":
        #                             row[2] = sp1
        #
        #                 cur.updateRow(row)
        #             del cur


    # arcpy.AddMessage(array_splitter)
    try:
        arr_sc_record = arcpy.da.FeatureClassToNumPyArray(ScName, ("GlobalID", "sp1_label", "sp2_label"), skip_nulls=False)

        fset = fl.query()  # Return a set of features from the layer
        features = fset.features  # Get features object
        if len(features) == 0:
            print("No features found in '%s' layer", warning=True)
            print('Exiting ...')
            exit()
        else:
            # print("Extracted %s features from '%s' layer" % (len(features)))
            pass

        dictLengths = {}
        for item in features:
            gID = item.attributes["GlobalID"]
            match_str = '{' + gID + '}'
            #arcpy.AddMessage(match_str)
            gID1 = match_str.upper()
            #arcpy.AddMessage(gID1)
            for data in arr_sc_record:
                if str(gID1) == str(data[0]):
                    #arcpy.AddMessage("match id" + str(gID))
                    dictLengths[gID] = (data[1], data[2])

        arcpy.AddMessage(len(dictLengths))
        # Update feature layer
        if len(list(dictLengths.keys())) > 0:
            ApplyUpdates(fl, features, ["sp1_label", "sp2_label"], dictLengths)
        else:
            arcpy.AddMessage("No records to process")

        # sts = ApplyUpdates_old(arr_sc_record, fl, ['sp1_label', 'sp2_label'])
        # ########### Update Live Data ############
        # if sts == True:
        #    arcpy.AddMessage("Secondary Splitter labeling has been done.")
    except Exception as ex:
        arcpy.AddMessage(ex)




    # try:
    #     arr_prmry_spltr = arcpy.da.FeatureClassToNumPyArray("primary_splitter", ("OriginalOID", "sp2_label"), skip_nulls=True)
    #     arcpy.AddMessage("arr_prmry_spltr")
    #     arcpy.AddMessage(arr_prmry_spltr)
    #     # arr_prmry_spltr = arcpy.da.FeatureClassToNumPyArray("primary_splitter", ("OriginalOID", "sp1_label"), skip_nulls=True)
    #     #
    #     # for x in arr_prmry_spltr:
    #     #     sc_id = x[0]
    #     #     with arcpy.da.UpdateCursor(ScName, ['OBJECID', 'sp1_label']) as cur:
    #     #         for row in cur:
    #     #             if row[0] == sc_id:
    #     #                 label = x[1]
    #     #                 row[1] = label
    #     #             cur.updateRow(row)
    #     #     del cur
    #     #
    #     status = ApplyUpdates_old(arr_prmry_spltr, fl, ['sp2_label'])
    #
    #     if status == True:
    #         arcpy.AddMessage("Primary Splitter label updated successfully.")
    #         arcpy.Delete_management("primary_splitter")
    #
    #     # fset = fl.query()  # Return a set of features from the layer
    #     # features = fset.features  # Get features object
    #     # if len(features) == 0:
    #     #     print("No features found in '%s' layer", warning=True)
    #     #     print('Exiting ...')
    #     #     exit()
    #     # else:
    #     #     # print("Extracted %s features from '%s' layer" % (len(features)))
    #     #     pass
    #     #
    #     # dictLengths = {}
    #     # for item in features:
    #     #     gID = item.attributes["GlobalID"]
    #     #     match_str = '{' + gID + '}'
    #     #     arcpy.AddMessage(match_str)
    #     #     gID1 = match_str.upper()
    #     #     arcpy.AddMessage(gID1)
    #     #     for data in arr_prmry_spltr:
    #     #         if str(gID1) == str(data[0]):
    #     #             dictLengths[gID] = data[1]
    #     # # do not round all dictionary items to the nearest integer
    #     # # for item in dictLengths:
    #     # #     dictLengths[item] = int(round(dictLengths[item]))
    #     #
    #     # arcpy.AddMessage(len(dictLengths))
    #     # # Update feature layer
    #     # if len(list(dictLengths.keys())) > 0:
    #     #     ApplyUpdates(fl, features, ["sp2_label"], dictLengths)
    #     # else:
    #     #     arcpy.AddMessage("No records to process")
    #
    # except Exception as ex:
    #     arcpy.AddMessage(ex)
else:
    if len(array_splitter) != 0:
        for rec_id in array_splitter:
            arcpy.AddMessage("splice_closures id - "+str(rec_id)+" sp1_label field not filled.")

if arcpy.Exists("primary_splitter_sheath_join"):
    arcpy.Delete_management("primary_splitter_sheath_join")
if arcpy.Exists("primary_splitter"):
    arcpy.Delete_management("primary_splitter")
if arcpy.Exists("secondary_splitter"):
    arcpy.Delete_management("secondary_splitter")
if arcpy.Exists("splitter_sheath_join"):
    arcpy.Delete_management("splitter_sheath_join")
if arcpy.Exists(ScName):
    arcpy.Delete_management(ScName)
if arcpy.Exists(FcName):
    arcpy.Delete_management(FcName)