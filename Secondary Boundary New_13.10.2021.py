import arcpy
import math
import sys
from datetime import datetime
from operator import sub
import os
import arcgis
from arcgis.gis import GIS



x=10000
sys.setrecursionlimit(x)

now = datetime.now()
start_time = now.strftime("%H:%M:%S")

same = lambda p1, p2, tol: sum(x**2 for x in map(sub, p1, p2)) <= tol**2

# arcpy.env.workspace = r"C:\Users\user\Documents\ArcGIS\Projects\MyProject2\MyProject2.gdb"
# Splice_Closures = "SpliceClosure"
# Fibre_Sheath = "FibreSheath"
# Boundaries = "Boundaries"


Splice_Closures = arcpy.GetParameterAsText(0)
Boundaries = arcpy.GetParameterAsText(1)
check_label = arcpy.GetParameterAsText(2)


def TruncateWebLayer(gis=None, target=None):
    try:
        lyr = arcgis.features.FeatureLayer(target, gis)
        arcpy.AddMessage(lyr)
        #lyr.manager.truncate()
        arcpy.AddMessage("Successfully truncated layer: " + str(target))
    except:
        arcpy.AddMessage("Failed truncating: exception")
        arcpy.AddMessage("Failed truncating: " + str(target))
        sys.exit()

def ApplyUpdates_old(arr_list, fls, field):
    status = False
    me = mygis.users.me
    arcpy.AddMessage("Updating boundary name field to Server.")
    try:
        for rec in arr_list:
            fset = fls.query(where="OBJECTID=" + str(rec[0]))

            sfo_feature = [f for f in fset][0]
            sfo_feature.attributes

            sfo_edit = sfo_feature
            sfo_edit.attributes[field[0]] = rec[1]
            sfo_edit.attributes['Editor'] = me.fullName

            update = fls.edit_features(updates=[sfo_edit])

            status = update["updateResults"][0]["success"]
    except Exception as ex:
        arcpy.AddMessage(ex)

    return status

# Function to apply updates
def ApplyUpdates(fl, ft, flds, dict):
    features_to_be_updated = []  # Create empty list for features to be updated
    # Create list of all features that match globalid in gID list
    cntTotalUpdates = 0  # Reset total rumber of updates
    cntPacket = 0  # Reset data packet number
    cntFeatures = 0  # Count total features searched
    while cntTotalUpdates < len(list(dict.keys())):
        cntUpdates = 0  # Set updates for new data packet to zero
        features_to_be_updated = []  # Create empty list for features to be updated
        # for edit_item in features: # Loop items in the features object
        while cntUpdates <= 2000 and cntFeatures < len(ft):  # Loop through 2000 records at a time
            edit_item = ft[cntFeatures]  # Set the item to edit
            gID = edit_item.attributes['GlobalID']  # Set the gloabl id
            if gID in list(dict.keys()):  # If feature gid is in the list of Gids
                toEdit = 0  # Reset flag to inditcate if record is to be updated/edited
                # if there is only one field to update...
                if len(flds) == 1:
                    item = flds[0]
                    valExist = edit_item.attributes[item]  # Exising value
                    valProp = dict[gID]  # Proposed value
                    if str(valExist) != str(valProp):  # Check if the existing value is the same as the proposed
                        edit_item.attributes[
                            item] = valProp  # If not then update the existing value to the proposed
                        toEdit = 1
                else:  # If there is more than one field to update
                    cntLstFlds = 0  # Set list item count to 0
                    for item in flds:  # loop all items in list of fields
                        valExist = edit_item.attributes[item]  # Exising value
                        valProp = dict[gID][cntLstFlds]  # Proposed value
                        if str(valExist) != str(valProp):  # Check if the existing value is the same as the proposed
                            edit_item.attributes[
                                item] = valProp  # If not then update the existing value to the proposed
                            toEdit = 1
                        cntLstFlds = cntLstFlds + 1  # increwment the list of fields index
                if toEdit == 1:
                    features_to_be_updated.append(edit_item)  # Add this edit to list of edits
                    cntUpdates = cntUpdates + 1  # increment the updates by packet counter
                    cntTotalUpdates = cntTotalUpdates + 1  # increment the total updates counter

            cntFeatures = cntFeatures + 1  # Increment the feature count

        if cntFeatures == len(
                ft) and cntUpdates == 0:  # if all records have been checked and there are no records to update then break out of while loop
            break
        cntPacket = cntPacket + 1  # increment the count of number of data packets
        # update the feature with the relevant edits
        cnt = 1  # Set the update attempt counter to 1
        while True and len(
                features_to_be_updated) > 0:  # While the features to be updated list has values perform the update
            arcpy.AddMessage(
                "Updating Feature Layer: data packet " + str(cntPacket) + ", attempt " + str(cnt) + "...")
            try:
                update_result = fl.edit_features(updates=features_to_be_updated)  # Apply updates to feature layer
            except Exception as e:  # Exception if there is an error updating
                arcpy.AddMessage(str(e))
                cnt = cnt + 1
            else:
                break
            if cnt == 20:
                print("Exiting after %s failed attempts" % cnt, ERROR=True)
                exit()

    arcpy.AddMessage("Total features updated: " + str(cntTotalUpdates))

def get_val_dict(array, id):
    values = []
    for key, val in array.items():
        if str(key) == str(id):
            values = val
    return values

aprx = arcpy.mp.ArcGISProject("CURRENT")

arcpy.env.preserveGlobalIds = True

# Get pole item from AGOL
desc = arcpy.Describe(Boundaries)
#arcpy.AddMessage(desc)

#to_array = [char for char in desc.name]

#BPath = desc.path +'/'+ to_array[1] # Get url to pole feature service
BPath = desc.catalogPath

# arcpy.AddMessage("Bpath")
# arcpy.AddMessage(BPath)

gdbLocal = aprx.defaultGeodatabase
# arcpy.AddMessage(gdbLocal)
gdbPath = aprx.homeFolder
gdbName = os.path.basename(gdbPath)
if not arcpy.Exists(gdbLocal):
    arcpy.CreateFileGDB_management(gdbPath, gdbName)

# Create local features and set these to variables
mygis = arcgis.gis.GIS("Pro")

# the URL of a single feature layer within a collection in an AGOL portal
publishedWebLayer = BPath


TruncateWebLayer(mygis, publishedWebLayer)

# reference the empty layer as FeatureLayer object from the ArcGIS Python API
fl = arcgis.features.FeatureLayer(publishedWebLayer, mygis)

arcpy.AddMessage("Creating local copies of the parameters...")

# Export only chambers connecting to Sheaths or Drops
arcpy.env.preserveGlobalIds = True  # Set the preserveGlobalIds environment to True


Boundary = "Boundaries"
ScName = "SpliceClosure"

p1 = aprx.defaultGeodatabase

p2 = p1 + "\\" + Boundary
p3 = p1 + "\\" + ScName

# Local Copy of Splice Closure
localSplice = os.path.join(gdbLocal, ScName)  # set the local feature class path
if arcpy.Exists(localSplice):
    arcpy.Delete_management(localSplice)
feats = arcpy.FeatureSet(table=Splice_Closures)
feats.save(p3)
arcpy.SelectLayerByAttribute_management(Splice_Closures,"CLEAR_SELECTION")



localBoundary = os.path.join(gdbLocal, Boundary)  # set the local feature class path
if arcpy.Exists(localBoundary):
    arcpy.Delete_management(localBoundary)

feats1 = arcpy.FeatureSet(table=Boundaries)
feats1.save(p2)
arcpy.SelectLayerByAttribute_management(Boundaries, "CLEAR_SELECTION")

arcpy.CalculateField_management(localBoundary, "OriginalID_B", "!OBJECTID!", "PYTHON3")

lstFields_sc = arcpy.ListFields(p3)
field_names_sc = [f.name for f in lstFields_sc]
if "OriginalOID" not in field_names_sc:
    arcpy.AddField_management(p3, "OriginalOID", "LONG", 10)
    arcpy.CalculateField_management(p3, "OriginalOID", "!OBJECTID!", "PYTHON3")
else:
    pass

################## Make Spatial Join with Primary Splitter #####################
arcpy.Select_analysis(localSplice, "secondary_splitter", '"enc_type" in (2)')
arcpy.Select_analysis(localSplice, "primary_splitter", '"enc_type" in (1, 5)')
if arcpy.Exists("secondary_splitter"):
    arcpy.SpatialJoin_analysis(localBoundary, "secondary_splitter", "secondary_splitter_boundary", join_operation="JOIN_ONE_TO_ONE", join_type="KEEP_ALL", match_option="INTERSECT")

    array_psplitter_boundary = arcpy.da.FeatureClassToNumPyArray("secondary_splitter_boundary", ("OriginalID_B", "JOIN_COUNT", "sp1_label"), skip_nulls=True)

    for rec in array_psplitter_boundary:
        with arcpy.da.UpdateCursor(localBoundary, ['OBJECTID', 'name', 'level_']) as cur:
            for row in cur:
                if str(row[0]) == str(rec[0]) and str(rec[1]) == "1" and row[2] == 3 and str(rec[2]) != "":
                    row[1] = str(rec[2])
                    cur.updateRow(row)
            del cur

if arcpy.Exists("primary_splitter"):
    arcpy.SpatialJoin_analysis(localBoundary, "primary_splitter", "primary_splitter_boundary",
                               join_operation="JOIN_ONE_TO_ONE", join_type="KEEP_ALL", match_option="INTERSECT")

    array_splitter_boundary = arcpy.da.FeatureClassToNumPyArray("primary_splitter_boundary",
                                                                 ("OriginalID_B", "JOIN_COUNT", "sp2_label"),
                                                                 skip_nulls=True)

    for rec in array_splitter_boundary:
        with arcpy.da.UpdateCursor(localBoundary, ['OBJECTID', 'name', 'level_']) as cur:
            for row in cur:
                if str(row[0]) == str(rec[0]) and str(rec[1]) == "1" and row[2] == 3 and str(rec[2]) != "":
                    row[1] = str(rec[2])
                    cur.updateRow(row)
            del cur

    arcpy.Select_analysis(p2, "secondary_boundary_new", '"level_" = 3')

    arcpy.SpatialJoin_analysis("secondary_boundary_new", "secondary_splitter", "secondary_boundary_splitter", join_operation="JOIN_ONE_TO_ONE", join_type="KEEP_ALL", match_option="INTERSECT")

    array_sec_boundary = arcpy.da.FeatureClassToNumPyArray("secondary_boundary_splitter", ("OriginalID_B", "JOIN_COUNT", "sp1_label", "OriginalOID"), skip_nulls=True)

    arcpy.AddMessage(array_sec_boundary)

    #arcpy.SelectLayerByLocation_management(localBoundary, "WITHIN", p3)

    #array_sec_boundary = arcpy.da.FeatureClassToNumPyArray("secondary_boundary_new", ("OriginalID_B", "JOIN_COUNT", "sp2_label"), skip_nulls=True)
    data_array = {}
    cursor = arcpy.UpdateCursor("secondary_boundary_new")
    for rowid in cursor:
        if rowid.level_ == 3:
            poly = rowid.Shape
            array_poly = []
            with arcpy.da.SearchCursor(p3, ['OBJECTID', 'sp1_label', "SHAPE@XY", "enc_type"]) as cur1:
                for row1 in cur1:
                    if row1[3] == 2:
                        geom = arcpy.Point(row1[2][0], row1[2][1])
                        if poly.contains(geom):
                            #arcpy.AddMessage(row1[1])
                            array_poly.append(row1[1])
                        data_array[rowid.OriginalID_B] = array_poly
                del cur1
    #arcpy.AddMessage(array)

    array_splice_used = []
    for data in array_sec_boundary:
        arr_splitter = get_val_dict(data_array, data[0])
        if str(data[1]) == str(len(arr_splitter)):
            #arcpy.AddMessage(arr_splitter)
            for rec in array_splice_used:
                if rec in arr_splitter:
                    arr_splitter.remove(rec)

            # if str(data[1]) == str(len(arr_splitter)):
            #     arcpy.AddMessage(str(data[0]) + "-" + str(data[1]))
            #     arcpy.AddMessage(len(arr_splitter))
                #arcpy.AddMessage(arr_splitter)
            with arcpy.da.UpdateCursor(localBoundary, ['OriginalID_B', 'name', 'level_']) as cur:
                for row in cur:
                    if str(row[0]) == str(data[0]):
                        if len(arr_splitter) > 0:
                            sp1_label = arr_splitter[0]
                            arcpy.AddMessage(arr_splitter[0])
                            array_splice_used.append(sp1_label)
                            row[1] = sp1_label
                            cur.updateRow(row)
                del cur

    try:
        arr_boundary_record = arcpy.da.FeatureClassToNumPyArray(localBoundary, ("GlobalID", "name"), skip_nulls=True)

        fset = fl.query()  # Return a set of features from the layer
        features = fset.features  # Get features object
        if len(features) == 0:
            print("No features found in '%s' layer", warning=True)
            print('Exiting ...')
            exit()
        else:
            # print("Extracted %s features from '%s' layer" % (len(features)))
            pass

        dictLengths = {}
        for item in features:
            gID = item.attributes["GlobalID"]
            match_str = '{' + gID + '}'
            arcpy.AddMessage(match_str)
            gID1 = match_str.upper()
            arcpy.AddMessage(gID1)
            for data in arr_boundary_record:
                if str(gID1) == str(data[0]):
                    arcpy.AddMessage("match id " + str(gID))
                    dictLengths[gID] = data[1]
        # do not round all dictionary items to the nearest integer
        # for item in dictLengths:
        #     dictLengths[item] = int(round(dictLengths[item]))

        arcpy.AddMessage(len(dictLengths))
        # Update feature layer
        if len(list(dictLengths.keys())) > 0:
            ApplyUpdates(fl, features, ["name"], dictLengths)
        else:
            arcpy.AddMessage("No records to process")

        # arr_boundary_record = arcpy.da.FeatureClassToNumPyArray(localBoundary, ("OBJECTID", "name"), skip_nulls=True)
        #
        # sts = ApplyUpdates(arr_boundary_record, fl, ['name'])
        # ############ Update Live Data ############
        # if sts == True:
        #     arcpy.AddMessage("Secondary Boundary labeling has been done.")
    except Exception as ex:
        arcpy.AddMessage(ex)

if arcpy.Exists("secondary_splitter_boundary"):
    arcpy.Delete_management("secondary_splitter_boundary")
if arcpy.Exists("secondary_splitter"):
    arcpy.Delete_management("secondary_splitter")
if arcpy.Exists("primary_splitter"):
    arcpy.Delete_management("primary_splitter")
if arcpy.Exists("primary_splitter_boundary"):
    arcpy.Delete_management("primary_splitter_boundary")
if arcpy.Exists("secondary_boundary_new"):
    arcpy.Delete_management("secondary_boundary_new")
if arcpy.Exists("secondary_boundary_splitter"):
    arcpy.Delete_management("secondary_boundary_splitter")
if arcpy.Exists(p2):
    arcpy.Delete_management(p2)
if arcpy.Exists(p3):
    arcpy.Delete_management(p3)
