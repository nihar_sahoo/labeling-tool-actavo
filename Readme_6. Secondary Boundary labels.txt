ToolBox Name: NetworkLabling_v1o4.tbx

ToolSet Name: Overall

Script Name: Secondary Boundary New_13.10.2021.py

Input: Splice Closure, Boundaries

Tool test: 6. Secondary Boundary Labels

Output: Boundaries (layer present in the Input geodatabse layer present in the Input geodatabse which is the live layer)

Field(s) to look for: name (Boundaries)

Important Note:
1. Tool takes 15 minutes approximately to run based on the number of features.
2. Tool is dependent on "sp1_label", "sp2_label" field of Primary and Secondary splitters, in case "sp1_label", "sp2_label" field is not filled or not updated then "name" field on Boundaries wont be updated correctly.
3. After running the tool remove and add the entire geodatabase again in the TOC to view the changes.
