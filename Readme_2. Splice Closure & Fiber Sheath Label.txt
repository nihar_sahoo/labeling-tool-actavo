ToolBox Name: NetworkLabling_v1o4.tbx

ToolSet Name: Overall

Script Name: 2. Splice Closure & Fiber Sheath Label_13.10.2021.py

Input: Splice Closure, Fibre Sheath & OLT

Tool test: 2. Splice Closure & Fiber Sheath Label

Output: Fibre_Sheath (layer present in the Input geodatabse layer present in the Input geodatabse which is the live layer )
	Splice_Closure (layer present in the Input geodatabse layer present in the Input geodatabse which is the live layer))

Field(s) to look for: label (Fibre_Sheath) & temp_cable_class (Fibre_Sheath)
		      label (Splice_Closure)

Important Note:
1. Tool takes 1 hours approximately to run based on the number of features.
2. After running the tool remove and add the entire geodatabase again in the TOC to view the changes.
3. Tool checks for Overlapping Splice_Closures and generates message and fails to run further if present.
4. Tool checks for looped Fibre_sheath cable conditions and generates message and fails to run if present.
5. Tool checks if Fibre_Sheath snapped to Splice_Closure and generates message and fails to run if present.
6. In some cases Ribbon field will not be updated correctly for both Fibre_Sheath and Splice_Closures due to geometry issues, those cases need to be inspected and checked for 
	-snapping issue (both Fibre_sheath start and end points with splice_closure)
	-direction issue (fibre_sheath cable is in opposite direction to what it should be).
7. Label will not be updated correctly for cases where temp_ribbon field is blank.
8. Snap Checks between Splice & Fibre Sheath should be proper.
9. Checks added to show message where temp_ribbon field for the splice_closure is filled or not.