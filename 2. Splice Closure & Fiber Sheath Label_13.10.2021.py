import arcpy
import math
import sys
from datetime import datetime
from operator import sub
import os
import arcgis
from arcgis.gis import GIS

x=10000
sys.setrecursionlimit(x)

now = datetime.now()
start_time = now.strftime("%H:%M:%S")

arcpy.env.preserveGlobalIds = True


## function to check equality of two coodiate pairs
## returns True or False
## p1, p2 = point coordinates, tol = tolerance
same = lambda p1, p2, tol: sum(x**2 for x in map(sub, p1, p2)) <= tol**2
arcpy.env.overwriteOutput = True
#####################
#arcpy.env.workspace = r"C:\Users\user\Documents\ArcGIS\Projects\MyProject2\MyProject2.gdb"
# arcpy.env.workspace = r"C:\Users\user\Documents\ArcGIS\Projects\MyProject7\MyProject7.gdb"
# Splice_Closures = "Splice_Closure"
# Fibre_Sheath = "Fibre_Sheath"
# OLT = "OLT"


Splice_Closures_new = arcpy.GetParameterAsText(0)
Fibre_Sheath_new = arcpy.GetParameterAsText(1)
OLT = arcpy.GetParameterAsText(2)
#Boundaries = arcpy.GetParameterAsText(3)
#####################

def TruncateWebLayer(gis=None, target=None):
    try:
        lyr = arcgis.features.FeatureLayer(target, gis)
        arcpy.AddMessage(lyr)
        # lyr.manager.truncate()
        arcpy.AddMessage("Successfully truncated layer: " + str(target))
    except:
        arcpy.AddMessage("Failed truncating: exception")
        arcpy.AddMessage("Failed truncating: " + str(target))
        sys.exit()

aprx = arcpy.mp.ArcGISProject("CURRENT")

#Get item from AGOL
desc_fibre = arcpy.Describe(Fibre_Sheath_new)
desc_splice = arcpy.Describe(Splice_Closures_new)


fsPath = desc_fibre.catalogPath # Get url to pole feature service
scPath = desc_splice.catalogPath # Get url to pole feature service

arcpy.AddMessage("fs path")
arcpy.AddMessage(fsPath)



arcpy.AddMessage("sp path")
arcpy.AddMessage(scPath)

gdbLocal = aprx.defaultGeodatabase
arcpy.AddMessage(gdbLocal)
gdbPath = aprx.homeFolder
gdbName = os.path.basename(gdbPath)
if not arcpy.Exists(gdbLocal):
    arcpy.CreateFileGDB_management(gdbPath, gdbName)

# Create local features and set these to variables
mygis = arcgis.gis.GIS("Pro")
#mygis1 = arcgis.gis.GIS("Pro")

# the URL of a single feature layer within a collection in an AGOL portal
publishedWebLayer = fsPath
publishedWebLayer1 = scPath

# a feature class on the local system with the same schema as the portal layer
updateFeatures = gdbPath

TruncateWebLayer(mygis, publishedWebLayer)
TruncateWebLayer(mygis, publishedWebLayer1)

# reference the empty layer as FeatureLayer object from the ArcGIS Python API
fl = arcgis.features.FeatureLayer(publishedWebLayer, mygis)
fl1 = arcgis.features.FeatureLayer(publishedWebLayer1, mygis)

arcpy.AddMessage("Creating local copies of the parameters...")

# Export only chambers connecting to Sheaths or Drops
arcpy.env.preserveGlobalIds = True  # Set the preserveGlobalIds environment to True
Splice_Closures = "Splice_Closure"
Fibre_Sheath = "Fibre_Sheath"

p1 = aprx.defaultGeodatabase

p2 = p1 + "\\" + Splice_Closures
p3 = p1 + "\\" + Fibre_Sheath

# Local Copy of Splice Closure
localSplice = os.path.join(gdbLocal, Splice_Closures)  # set the local feature class path
if arcpy.Exists(localSplice):
   arcpy.Delete_management(localSplice)
#arcpy.FeatureClassToFeatureClass_conversion(Splice_Closures_new, gdbLocal,Splice_Closures)  # Export the layer to the local geodatabase
#arcpy.conversion.FeatureClassToFeatureClass(Splice_Closures_new, gdbLocal,Splice_Closures,'', r'label "Label" true true false 255 Text 0 0,First,#,Adare_Test_Layers\Splice Closures,label,0,255;enc_type "Enclosure Type" true true false 0 Long 0 0,First,#,Adare_Test_Layers\Splice Closures,enc_type,-1,-1;enc_size "Enclosure Size" true true false 0 Long 0 0,First,#,Adare_Test_Layers\Splice Closures,enc_size,-1,-1;stru_label "Structure Label" true true false 255 Text 0 0,First,#,Adare_Test_Layers\Splice Closures,stru_label,0,255;placement "Placement" true true false 0 Long 0 0,First,#,Adare_Test_Layers\Splice Closures,placement,-1,-1;sp1_type "Sp1 Type" true true false 0 Long 0 0,First,#,Adare_Test_Layers\Splice Closures,sp1_type,-1,-1;sp1_label "Sp1 Label" true true false 255 Text 0 0,First,#,Adare_Test_Layers\Splice Closures,sp1_label,0,255;sp2_type "Sp2 Type" true true false 0 Long 0 0,First,#,Adare_Test_Layers\Splice Closures,sp2_type,-1,-1;sp2_label "Sp2 Label" true true false 255 Text 0 0,First,#,Adare_Test_Layers\Splice Closures,sp2_label,0,255;status "Status" true true false 0 Long 0 0,First,#,Adare_Test_Layers\Splice Closures,status,-1,-1;reel_end "Reel End" true true false 0 Long 0 0,First,#,Adare_Test_Layers\Splice Closures,reel_end,-1,-1;comments "Comments" true true false 255 Text 0 0,First,#,Adare_Test_Layers\Splice Closures,comments,0,255;GlobalID "GlobalID" false false true 38 GlobalID 0 0,First,#,Adare_Test_Layers\Splice Closures,GlobalID,-1,-1;CreationDate "CreationDate" false true false 8 Date 0 0,First,#,Adare_Test_Layers\Splice Closures,CreationDate,-1,-1;Creator "Creator" false true false 128 Text 0 0,First,#,Adare_Test_Layers\Splice Closures,Creator,0,128;EditDate "EditDate" false true false 8 Date 0 0,First,#,Adare_Test_Layers\Splice Closures,EditDate,-1,-1;Editor "Editor" false true false 128 Text 0 0,First,#,Adare_Test_Layers\Splice Closures,Editor,0,128;temp_drops "Number of Drops" true true false 0 Long 0 0,First,#,Adare_Test_Layers\Splice Closures,temp_drops,-1,-1;temp_maxcable "Max Cable Size" true true false 0 Long 0 0,First,#,Adare_Test_Layers\Splice Closures,temp_maxcable,-1,-1;temp_ribbon "Ribbon" true true false 256 Text 0 0,First,#,Adare_Test_Layers\Splice Closures,temp_ribbon,0,256;OBJ1 "OBJ1" true true false 255 Text 0 0,First,#,Adare_Test_Layers\Splice Closures,OBJECTID,-1,-1', '')
feats = arcpy.FeatureSet(table=Splice_Closures_new)
feats.save(p2)
arcpy.SelectLayerByAttribute_management(Splice_Closures_new,"CLEAR_SELECTION")  # Clear any current selection from chambers

# Local copy of Fibre Sheath
localFibre = os.path.join(gdbLocal, Fibre_Sheath)  # set the local feature class path
if arcpy.Exists(localFibre):
   arcpy.Delete_management(localFibre)
#arcpy.FeatureClassToFeatureClass_conversion(Fibre_Sheath_new, gdbLocal,Fibre_Sheath)  # Export the layer to the local geodatabase
#arcpy.conversion.FeatureClassToFeatureClass(Fibre_Sheath_new, gdbLocal, Fibre_Sheath, '', r'label "Label" true true false 255 Text 0 0,First,#;type "Type" true true false 0 Long 0 0,First,#,Adare_Test_Layers\Fibre Sheath,type,-1,-1;calc_lengt "Calculated Length" true true false 0 Long 0 0,First,#,Adare_Test_Layers\Fibre Sheath,calc_lengt,-1,-1;meas_lengt "Measured Length" true true false 0 Long 0 0,First,#,Adare_Test_Layers\Fibre Sheath,meas_lengt,-1,-1;from_ "From Enclosure" true true false 255 Text 0 0,First,#,Adare_Test_Layers\Fibre Sheath,from_,0,255;placement "Placement" true true false 0 Long 0 0,First,#,Adare_Test_Layers\Fibre Sheath,placement,-1,-1;backhaul "Backhaul" true true false 0 Long 0 0,First,#,Adare_Test_Layers\Fibre Sheath,backhaul,-1,-1;temp_ribbon "Ribbon" true true false 256 Text 0 0,First,#,Adare_Test_Layers\Fibre Sheath,temp_ribbon,0,256;temp_cable_class "Cable Class" true true false 0 Long 0 0,First,#,Adare_Test_Layers\Fibre Sheath,temp_cable_class,-1,-1;temp_spatial_index "Spatial Index" true true false 0 Long 0 0,First,#,Adare_Test_Layers\Fibre Sheath,temp_spatial_index,-1,-1;GlobalID "GlobalID" false false true 38 GlobalID 0 0,First,#,Adare_Test_Layers\Fibre Sheath,GlobalID,-1,-1;CreationDate "CreationDate" false true false 8 Date 0 0,First,#,Adare_Test_Layers\Fibre Sheath,CreationDate,-1,-1;Creator "Creator" false true false 128 Text 0 0,First,#,Adare_Test_Layers\Fibre Sheath,Creator,0,128;EditDate "EditDate" false true false 8 Date 0 0,First,#,Adare_Test_Layers\Fibre Sheath,EditDate,-1,-1;Editor "Editor" false true false 128 Text 0 0,First,#,Adare_Test_Layers\Fibre Sheath,Editor,0,128;ug_adss "Length of Underground ADSS sections" true true false 0 Long 0 0,First,#,Adare_Test_Layers\Fibre Sheath,ug_adss,-1,-1;Shape__Length "Shape__Length" false true true 0 Double 0 0,First,#,Adare_Test_Layers\Fibre Sheath,Shape__Length,-1,-1;OBJ1 "OBJ1" true true false 255 Text 0 0,First,#,Adare_Test_Layers\Fibre Sheath,OBJECTID,-1,-1', '')
feats1 = arcpy.FeatureSet(table=Fibre_Sheath_new)
feats1.save(p3)
arcpy.SelectLayerByAttribute_management(Fibre_Sheath_new,"CLEAR_SELECTION")  # Clear any current selection from chambers


arcpy.AddMessage("Create Copy Feature Layer")

#################### Snapping Checks #############################

# if arcpy.Exists(Boundaries):
#     arcpy.Select_analysis(Boundaries, "Bounday_ribbon", '"level_" = 4')
#     arcpy.AddMessage("Choose Boundary Ribbon")

array_ribbon = arcpy.da.FeatureClassToNumPyArray(OLT, ["label"], skip_nulls=True)

location = array_ribbon[0][0]


fields_sc = ("OriginalOID", "label", "sp1_label", "sp2_label", "SHAPE@XY", "enc_type")
fields_fs = ("OriginalOID", "type", "ODP1_Count", "SHAPE@XY", "Cable_type", "ODP2_Count")
fields_fs1 = ("OriginalOID", "label", "from_", "SHAPE@XY")
SR = arcpy.Describe(Fibre_Sheath).spatialReference
print(SR.name)
#Fibre_Sheath_New = "QC4_Fibre_Sheath"


arcpy.AddMessage("Create Temp Filed on copy feature")
lstFields_fs = arcpy.ListFields(Fibre_Sheath)
field_names_fs = [f.name for f in lstFields_fs]
if "OriginalOID" not in field_names_fs:
    arcpy.AddField_management(Fibre_Sheath, "OriginalOID", "LONG", 10)
    arcpy.CalculateField_management(Fibre_Sheath, "OriginalOID", "!OBJECTID!", "PYTHON3")
else:
    pass

lstFields_sc = arcpy.ListFields(Splice_Closures)
field_names_sc = [f.name for f in lstFields_sc]
if "OriginalOID" not in field_names_sc:
    arcpy.AddField_management(Splice_Closures, "OriginalOID", "LONG", 10)
    arcpy.CalculateField_management(Splice_Closures, "OriginalOID", "!OBJECTID!", "PYTHON3")
else:
    pass

Status_check = False
arcpy.AddMessage("Make Spatial Relation from fibre sheath to splice closure to check snapping.")
arcpy.AddMessage("Validate Snapping issue is there or not")
if arcpy.Exists(Splice_Closures) and arcpy.Exists(Fibre_Sheath):
    arcpy.SpatialJoin_analysis(Fibre_Sheath, Splice_Closures, "snapinng_fibre_splice", join_operation="JOIN_ONE_TO_ONE", join_type="KEEP_ALL", match_option="INTERSECT")

array_snapping_id = []

if arcpy.Exists("snapinng_fibre_splice"):
    array_snapp_data = arcpy.da.FeatureClassToNumPyArray("snapinng_fibre_splice", ("OriginalOID", "Join_Count"), skip_nulls=True)
    print(array_snapp_data)
    for datas in array_snapp_data:
        if str(datas[1]) == "0":
            array_snapping_id.append(datas[0])
    arcpy.AddMessage(array_snapping_id)
    if len(array_snapping_id) != 0:
        Status_check = True

def getOverlapingCoord(coordinate, array):
    geom = []
    for rec in array:
        geom.append(rec[1][0])
    count = geom.count(coordinate[0])
    if count >= 2:
        status = True
    else:
        status = False
    return status

arcpy.AddMessage("Checking Overlapping Splice enclosure")

array_splice = arcpy.da.FeatureClassToNumPyArray(Splice_Closures, ("OriginalOID", "SHAPE@XY"))

array_overlaping_splice = []
with arcpy.da.SearchCursor(Splice_Closures, ("OriginalOID", "SHAPE@XY")) as cursor:
    for row in cursor:
        data = getOverlapingCoord(row[1], array_splice)
        if data:
            Status_check = True
            array_overlaping_splice.append(row[0])
    del cursor

arcpy.AddMessage("Checking Fibre Sheath loophole in the data")
loop_check = False
loop_hole_id = []
with arcpy.da.SearchCursor(Fibre_Sheath, ("OriginalOID", "SHAPE@")) as cursor:
    for row in cursor:
        f_point = row[1].firstPoint
        l_point = row[1].lastPoint
        if f_point.X == l_point.X:
            print(row[0])
            loop_hole_id.append(row[0])
            Status_check = True
            loop_check = True
    del cursor

arcpy.AddMessage("Checking Splice Closure temp_ribbon field to be filled with all ribbon")
array_splice_ribbon = arcpy.da.FeatureClassToNumPyArray(p2, ("OriginalOID", "temp_ribbon"), skip_nulls=False)
array_ribbon = []
for data in array_splice_ribbon:
    if str(data[1]) == "None" or str(data[1]) == "":
        Status_check = True
        array_ribbon.append(data[0])

if Status_check == False:
    arcpy.AddMessage("Create ODP1_Count & ODP2_Count field ")
    #try:
    if "ODP1_Count" not in field_names_fs:
        arcpy.AddField_management(Fibre_Sheath, "ODP1_Count", "LONG", 4)
    if "ODP2_Count" not in field_names_fs:
        arcpy.AddField_management(Fibre_Sheath, "ODP2_Count", "LONG", 4)
    if "Cable_type" not in field_names_fs:
        arcpy.AddField_management(Fibre_Sheath, "Cable_type", "Text", 100)
    if "new_label" not in field_names_fs:
        arcpy.AddField_management(Fibre_Sheath, "new_label", "Text", 100)
    # if "ribbon" not in field_names_fs:
    #     arcpy.AddField_management(Fibre_Sheath, "ribbon", "Text", 100)

    if "new_label" not in field_names_sc:
        arcpy.AddField_management(Splice_Closures, "new_label", "Text", 100)
    # except Exception as e:
    #     print(e)


    def ApplyUpdates_old(arr_list, fls, field):
        status = False
        me = mygis.users.me
        for i in field:
            arcpy.AddMessage("Updating "+str(i)+" field to Server.")
        try:
            for rec in arr_list:
                arcpy.AddMessage(rec)
                fset = fls.query(where="OBJECTID=" + str(rec[0]))
                if len(fset.features) != 0:
                    sfo_feature = [f for f in fset][0]
                    arcpy.AddMessage(rec[1])
                    sfo_feature.attributes

                    sfo_edit = sfo_feature
                    sfo_edit.attributes[field[0]] = rec[1]
                    if len(field) > 1:
                        sfo_edit.attributes[field[1]] = rec[2]
                    sfo_edit.attributes['Editor'] = me.fullName

                    update = fls.edit_features(updates=[sfo_edit])
                    arcpy.AddMessage(update)
                    status = update["updateResults"][0]["success"]
        except Exception as ex:
            arcpy.AddMessage(ex)

        return status

    # Function to apply updates
    def ApplyUpdates(fl, ft, flds, dict):
        features_to_be_updated = []  # Create empty list for features to be updated
        # Create list of all features that match globalid in gID list
        cntTotalUpdates = 0  # Reset total rumber of updates
        cntPacket = 0  # Reset data packet number
        cntFeatures = 0  # Count total features searched
        while cntTotalUpdates < len(list(dict.keys())):
            cntUpdates = 0  # Set updates for new data packet to zero
            features_to_be_updated = []  # Create empty list for features to be updated
            # for edit_item in features: # Loop items in the features object
            while cntUpdates <= 2000 and cntFeatures < len(ft):  # Loop through 2000 records at a time
                edit_item = ft[cntFeatures]  # Set the item to edit
                gID = edit_item.attributes['GlobalID']  # Set the gloabl id
                if gID in list(dict.keys()):  # If feature gid is in the list of Gids
                    toEdit = 0  # Reset flag to inditcate if record is to be updated/edited
                    # if there is only one field to update...
                    if len(flds) == 1:
                        item = flds[0]
                        valExist = edit_item.attributes[item]  # Exising value
                        valProp = dict[gID]  # Proposed value
                        if str(valExist) != str(valProp):  # Check if the existing value is the same as the proposed
                            edit_item.attributes[
                                item] = valProp  # If not then update the existing value to the proposed
                            toEdit = 1
                    else:  # If there is more than one field to update
                        cntLstFlds = 0  # Set list item count to 0
                        for item in flds:  # loop all items in list of fields
                            valExist = edit_item.attributes[item]  # Exising value
                            valProp = dict[gID][cntLstFlds]  # Proposed value
                            if str(valExist) != str(valProp):  # Check if the existing value is the same as the proposed
                                edit_item.attributes[
                                    item] = valProp  # If not then update the existing value to the proposed
                                toEdit = 1
                            cntLstFlds = cntLstFlds + 1  # increwment the list of fields index
                    if toEdit == 1:
                        features_to_be_updated.append(edit_item)  # Add this edit to list of edits
                        cntUpdates = cntUpdates + 1  # increment the updates by packet counter
                        cntTotalUpdates = cntTotalUpdates + 1  # increment the total updates counter

                cntFeatures = cntFeatures + 1  # Increment the feature count

            if cntFeatures == len(
                    ft) and cntUpdates == 0:  # if all records have been checked and there are no records to update then break out of while loop
                break
            cntPacket = cntPacket + 1  # increment the count of number of data packets
            # update the feature with the relevant edits
            cnt = 1  # Set the update attempt counter to 1
            while True and len(
                    features_to_be_updated) > 0:  # While the features to be updated list has values perform the update
                arcpy.AddMessage(
                    "Updating Feature Layer: data packet " + str(cntPacket) + ", attempt " + str(cnt) + "...")
                try:
                    update_result = fl.edit_features(updates=features_to_be_updated)  # Apply updates to feature layer
                except Exception as e:  # Exception if there is an error updating
                    arcpy.AddMessage(str(e))
                    cnt = cnt + 1
                else:
                    break
                if cnt == 20:
                    print("Exiting after %s failed attempts" % cnt, ERROR=True)
                    exit()

        arcpy.AddMessage("Total features updated: " + str(cntTotalUpdates))

    def remove_dup_by_first_element(list1):
        first_element_list = []
        list2 = []
        for x in list1:
            if x[0] not in first_element_list:
                list2.append(x)
                first_element_list.append(x[0])
        return list2
    #
    # ##find next splice closure id by comparing coordinates
    def find_next_sc1(coor_end1, arr_sc1):
        global label
        for x in arr_sc1:
            if same(coor_end1, x[4], 0.1):
                label = x[1]
        return label

    def find_next_sc(coor_end1, arr_sc1):
        global id_sc1
        for x in arr_sc1:
            if same(coor_end1, x[4], 0.1):
                #arcpy.AddMessage("match"+str(x[4]))
                id_sc1 = x[0]
        return id_sc1
    #
    #Find Enc_type of the splice closure
    def find_enc_type(id_sc1,arr_sc1):
        global enc_type1
        for x in arr_sc1:
            if x[0] == id_sc1:
                enc_type1 = x[5]
        return enc_type1

    #Find coordinate of the splice closure
    def find_coor_from_id_sc(id_sc1,arr_sc1):
        global coor1
        for x in arr_sc1:
            if x[0] == id_sc1:
                coor1 = x[4]
        return coor1
    #
    #Find coordinate of the fibre sheath
    def find_coor_from_id_fs(id_fs1,arr_fs1):
        global coor1
        for x in arr_fs1:
            if x[0] == id_fs1:
                coor1 = x[3]
        return coor1

    #Find coordinate of the primary fibre sheath
    def find_coor_from_id_fs_primary(id_fs1,arr_fs1):
        global coor1
        for x in arr_fs1:
            if x[0] == id_fs1 and x[4] == "Primary":
                coor1 = x[3]
        return coor1

    #Find coordinate of the secondary fibre sheath
    def find_coor_from_id_fs_secondary(id_fs1,arr_fs1):
        global coor1
        for x in arr_fs1:
            if x[0] == id_fs1 and x[4] == "Secondary":
                coor1 = x[3]
        return coor1

    #Find next splice closure id from id of fiber sheath
    def find_next_sc_id_from_fs_id(id_fs1, arr_sc1, arr_fs_end):
        global id_sc1
        coor_sc1 = find_coor_from_id_fs(id_fs1,arr_fs_end)
        for x in arr_sc1:
            if same(x[4], coor_sc1, 0.1):
                #print("fs id"+str(id_fs1)+"sc id"+str(x[0]))
                id_sc1 = x[0]
        return id_sc1

    #Find next Primary splice closure id from id of fiber sheath
    def find_next_sc_id_from_fs_id_primary(id_fs1, arr_sc1, arr_fs_end):
        global id_sc1
        coor_sc1 = find_coor_from_id_fs_primary(id_fs1,arr_fs_end)
        for x in arr_sc1:
            if same(x[4], coor_sc1, 0.1):
                id_sc1 = x[0]
        return id_sc1

    def find_next_sc_id_from_fs_id_secondary(id_fs1, arr_sc1, arr_fs_end):
        global id_sc1
        coor_sc1 = find_coor_from_id_fs_secondary(id_fs1,arr_fs_end)
        for x in arr_sc1:
            if same(x[4], coor_sc1, 0.1):
                id_sc1 = x[0]
        return id_sc1

    #Find out going fiber sheaths from label of splice closure
    # in downward direction
    def find_front_fs_from_sc(id_sc1, arr_sc1, arr_fs_start):
        #create a blank array
        id_fs1_list = []
        coor_sc1 = find_coor_from_id_sc(id_sc1, arr_sc1)
        #print(coor_sc1)
        ##check equality with start coordinate of fiber sheath array
        for x in arr_fs_start:
            if  same(coor_sc1, x[3], 0.1):
                if x[0] not in id_fs1_list:
                    id_fs1_list.append(x[0])
        return id_fs1_list

    def find_front_fs_from_sc_primary(id_sc1, arr_sc1, arr_fs_start):
        #create a blank array
        id_fs1_list = []
        coor_sc1 = find_coor_from_id_sc(id_sc1, arr_sc1)
        ##check equality with start coordinate of fiber sheath array
        for x in arr_fs_start:
            if  same(coor_sc1, x[3], 0.1):
                if x[0] not in id_fs1_list and str(x[4]) == "Primary":
                    id_fs1_list.append(x[0])
        return id_fs1_list

    def find_front_fs_from_sc_secondary(id_sc1, arr_sc1, arr_fs_start):
        #create a blank array
        id_fs1_list = []
        coor_sc1 = find_coor_from_id_sc(id_sc1, arr_sc1)
        ##check equality with start coordinate of fiber sheath array
        for x in arr_fs_start:
            if same(coor_sc1, x[3], 0.1):
                if x[0] not in id_fs1_list and str(x[4]) == "Secondary":
                    id_fs1_list.append(x[0])
        return id_fs1_list


    def find_front_fs_from_sc_all(id_sc1, arr_sc1, arr_fs_start):
        #create a blank array
        id_fs1_list = []
        coor_sc1 = find_coor_from_id_sc(id_sc1, arr_sc1)
        ##check equality with start coordinate of fiber sheath array
        for x in arr_fs_start:
            if same(coor_sc1, x[3], 0.1):
                if x[0] not in id_fs1_list:
                    id_fs1_list.append(x[0])
        return id_fs1_list

    def sort_fibre_id_list_all(fibre_id_list, arr_fs):
        array = []
        val_check = []
        if len(fibre_id_list) > 0:
            for id in fibre_id_list:
                for rec in arr_fs:
                    if str(rec[0]) == str(id) and id not in val_check:
                        val_check.append(id)
                        array.append([id, rec[2], rec[4]])

        sort_array = sorted(array, key=lambda x: x[1], reverse=True)
        return sort_array

    def sort_fibre_id_list_all_sec(fibre_id_list, arr_fs):
        array = []
        val_check = []
        if len(fibre_id_list) > 0:
            for id in fibre_id_list:
                for rec in arr_fs:
                    if str(rec[0]) == str(id) and id not in val_check:
                        val_check.append(id)
                        array.append([id, rec[5], rec[4]])

        sort_array = sorted(array, key=lambda x: x[1], reverse=True)
        return sort_array

    def sort_fibre_id_list(fibre_id_list, arr_fs):
        array = []
        if len(fibre_id_list) > 0:
            for id in fibre_id_list:
                for rec in arr_fs:
                    if rec[4] == "Primary" and str(rec[0]) == str(id):
                        array.append([id, rec[2]])

        sort_array = sorted(array, key=lambda x: x[1], reverse=True)
        return sort_array

    def sort_fibre_id_list_sec(fibre_id_list, arr_fs):
        array = []
        if len(fibre_id_list) > 0:
            for id in fibre_id_list:
                for rec in arr_fs:
                    if rec[4] == "Secondary" and str(rec[0]) == str(id):
                        array.append([id, rec[5]])

        sort_array = sorted(array, key=lambda x: x[1], reverse=True)
        return sort_array

    def sort_unique_ids(lists):
        array = []
        records = []
        for lst in lists:
            if len(array) == 0:
                array.append(lst[0])
                records.append(lst)
            elif lst[0] not in array:
                records.append(lst)
                array.append(lst[0])
        return records

    def get_fs_cordinate(id, arr_fs_end):
        global cord
        for x in arr_fs_end:
            if x[0] == id:
                cord = x[3]
        return cord

    # Recursive function to collect all ODP1 splice clolsure
    # it will take input of splice closure and find the sheaths connected with from Label and From fields
    def find_next_ODP1(fiber_list_checked, id_sc1, arr_sc1, arr_fs_end, ODP1_list):
        #print("id sc "+str(id_sc1))
        fiber_front_list = find_front_fs_from_sc(id_sc1, arr_sc1, arr_fs_start)
        #print("fiber_front_list")
        #print(fiber_front_list)

        # remove fibers which are already covered
        for x in fiber_front_list:
            if x in fiber_list_checked:
                fiber_front_list.remove(x)
        # drop_path_list = drop_path_list1
        if len(fiber_front_list) > 0:
            for x in fiber_front_list:
                if x not in fiber_list_checked:
                    fiber_list_checked.append(x)
                    id_sc1 = find_next_sc_id_from_fs_id(x, arr_sc1, arr_fs_end)
                    #print(id_sc1)
                    if id_sc1 != None:
                        enclosure_type = find_enc_type(id_sc1, arr_sc)
                        if enclosure_type == 1 or enclosure_type == 5:
                            ODP1_list.append(id_sc1)
                        ODP1_list = find_next_ODP1(fiber_list_checked, id_sc1, arr_sc1, arr_fs_end, ODP1_list)
        #print(ODP1_list)
        #print("check")
        return ODP1_list

    def find_next_ODP2(fiber_list_checked, id_sc1, arr_sc1, arr_fs_end, ODP2_list):
        fiber_front_list = find_front_fs_from_sc(id_sc1, arr_sc1, arr_fs_start)
        # print(fiber_front_list)
        # remove fibers which are already covered
        for x in fiber_front_list:
            if x in fiber_list_checked:
                fiber_front_list.remove(x)
        # drop_path_list = drop_path_list1
        if len(fiber_front_list) > 0:
            for x in fiber_front_list:
                if x not in fiber_list_checked:
                    fiber_list_checked.append(x)
                    id_sc1 = find_next_sc_id_from_fs_id(x, arr_sc1, arr_fs_end)
                    #print(id_sc1)
                    if id_sc1 != None:
                        enclosure_type = find_enc_type(id_sc1, arr_sc)
                        if enclosure_type == 2 or enclosure_type == 5:
                            ODP2_list.append(id_sc1)
                        ODP2_list = find_next_ODP2(fiber_list_checked, id_sc1, arr_sc1, arr_fs_end, ODP2_list)
        # print(ODP1_list)
        # print("check")
        return ODP2_list

    ############## Core Function

    ################### Comented To test application ####################

    arr_sc = arcpy.da.FeatureClassToNumPyArray(Splice_Closures, fields_sc, skip_nulls=False)
    #print(arr_sc)

    arr_fs = arcpy.da.FeatureClassToNumPyArray(Fibre_Sheath, fields_fs1, skip_nulls=False, explode_to_points=True)
    #print(arr_fs)

    ## sapatially select cables touching OLT
    cable_copy_OLT = arcpy.SelectLayerByLocation_management(Fibre_Sheath, overlap_type='INTERSECT', select_features=OLT)
    arr_fs_OLT = arcpy.da.FeatureClassToNumPyArray(cable_copy_OLT, fields_fs1, skip_nulls=True, explode_to_points=True)

    ##########################
    arr_fs_start_olt = remove_dup_by_first_element(arr_fs_OLT)
    #print(arr_fs_start_olt)
    ## arr_fs has multiple entries as the number of vertices for each fiber polyline
    ## reverse it and remove duplicates to filter the entries with only one entry containing end coordinates for each fiber
    arr_fs_end1_olt = arr_fs_OLT
    arr_fs_end2_olt = arr_fs_end1_olt[::-1]
    arr_fs_end2_olt = remove_dup_by_first_element(arr_fs_end2_olt)
    arr_fs_end_olt = arr_fs_end2_olt[::-1]
    #print(arr_fs_end_olt)
    ############################

    ##########################
    arr_fs_start = remove_dup_by_first_element(arr_fs)
    #print(arr_fs_start)

    ## arr_fs has multiple entries as the number of vertices for each fiber polyline
    ## reverse it and remove duplicates to filter the entries with only one entry containing end coordinates for each fiber
    arr_fs_end1 = arr_fs
    arr_fs_end2 = arr_fs_end1[::-1]
    arr_fs_end2 = remove_dup_by_first_element(arr_fs_end2)
    arr_fs_end = arr_fs_end2[::-1]
    # arcpy.AddMessage(arr_fs)
    # arcpy.AddMessage(arr_fs_end)
    print(arr_fs_start)
    print(arr_fs_end)


    ODP1_list_all = []

    try:
        for x in arr_fs_end:
            fiber_list_checked = []
            ODP1_list = []
            coor_end = x[3]
            next_splice_closure = find_next_sc(coor_end, arr_sc)
            enclosure_type = find_enc_type(next_splice_closure, arr_sc)
            #print(next_splice_closure)
            if enclosure_type == 1 or enclosure_type == 5:
                ODP1_list.append(next_splice_closure)
            ODP1_list = find_next_ODP1(fiber_list_checked, next_splice_closure, arr_sc, arr_fs_end, ODP1_list)
            print(ODP1_list)
            ODP1_list_all.append([x[0], len(ODP1_list)])

        arcpy.AddMessage(ODP1_list_all)
    except Exception as e:
        arcpy.AddMessage(e)


    ODP2_list_all = []

    try:
        for x in arr_fs_end:
            # print(x)
            fiber_list_checked = []
            ODP2_list = []
            coor_end = x[3]
            next_splice_closure = find_next_sc(coor_end, arr_sc)
            enclosure_type = find_enc_type(next_splice_closure, arr_sc)
            # print(next_splice_closure)
            if enclosure_type == 2 or enclosure_type == 5:
                #if next_splice_closure != None:
                ODP2_list.append(next_splice_closure)
                #print(next_splice_closure)
            ODP2_list = find_next_ODP2(fiber_list_checked, next_splice_closure, arr_sc, arr_fs_end, ODP2_list)
            print(ODP2_list)

            ODP2_list_all.append([x[0], len(ODP2_list)])
    except Exception as e:
        arcpy.AddMessage(e)
    arcpy.AddMessage("ODP2_list")
    arcpy.AddMessage(ODP2_list_all)



    # writing geometries to drop_path feature class
    for x in ODP1_list_all:
        # print(x)
        fs_id = x[0]
        with arcpy.da.UpdateCursor(Fibre_Sheath, ['OBJECTID', 'ODP1_Count', 'Cable_type', 'ODP2_Count', 'temp_cable_class']) as cur:
            for row in cur:
                if row[0] == fs_id:
                    row[1] = x[1]
                    row[3] = 0
                    if x[1] == 0:
                        row[2] = "Secondary"
                        row[4] = 2
                    if x[1] > 0:
                        row[2] = "Primary"
                        row[4] = 1
                cur.updateRow(row)
            del cur

    for x in ODP2_list_all:
        # print(x)
        fs_id = x[0]
        with arcpy.da.UpdateCursor(Fibre_Sheath, ['OBJECTID', 'ODP2_Count']) as cur:
            for row in cur:
                if row[0] == fs_id:
                    if str(x[1]) == 'None':
                        row[1] = 0
                    else:
                        row[1] = x[1]
                cur.updateRow(row)
            del cur

    try:
        arr_sc = arcpy.da.FeatureClassToNumPyArray(Splice_Closures, fields_sc, skip_nulls=False)
        # print(arr_sc)

        arr_fs = arcpy.da.FeatureClassToNumPyArray(Fibre_Sheath, fields_fs, skip_nulls=False, explode_to_points=True)
        arcpy.AddMessage(arr_fs)
    except Exception as ex:
        arcpy.AddMessage(ex)
        sys.exit()

    print(arr_fs)

    # sapatially select cables touching OLT
    cable_copy_OLT = arcpy.SelectLayerByLocation_management(Fibre_Sheath, overlap_type='INTERSECT', select_features=OLT)
    arr_fs_OLT = arcpy.da.FeatureClassToNumPyArray(cable_copy_OLT, fields_fs, skip_nulls=True, explode_to_points=True)

    ##########################
    arr_fs_start_olt = remove_dup_by_first_element(arr_fs_OLT)
    #print(arr_fs_start_olt)
    ## arr_fs has multiple entries as the number of vertices for each fiber polyline
    ## reverse it and remove duplicates to filter the entries with only one entry containing end coordinates for each fiber
    arr_fs_end1_olt = arr_fs_OLT
    arr_fs_end2_olt = arr_fs_end1_olt[::-1]
    arr_fs_end2_olt = remove_dup_by_first_element(arr_fs_end2_olt)
    arr_fs_end_olt = arr_fs_end2_olt[::-1]
    #print(arr_fs_end_olt)
    ############################

    ##########################
    arr_fs_start = remove_dup_by_first_element(arr_fs)
    #print(arr_fs_start)
    ## arr_fs has multiple entries as the number of vertices for each fiber polyline
    ## reverse it and remove duplicates to filter the entries with only one entry containing end coordinates for each fiber
    arr_fs_end1 = arr_fs
    arr_fs_end2 = arr_fs_end1[::-1]
    arr_fs_end2 = remove_dup_by_first_element(arr_fs_end2)
    arr_fs_end = arr_fs_end2[::-1]



    ############################
    #
    # # Recursive function to collect all ODP1 splice closure
    # # it will take input of splice closure and find the sheaths connected with from Label and From fields
    def find_next_secondary(fiber_list_checked, id_sc1, arr_sc1, arr_fs_end, ODP1_list):
        fiber_front_list = find_front_fs_from_sc_secondary(id_sc1, arr_sc1, arr_fs_start)
        # print("primary")
        # print(fiber_front_list)
        #remove fibers which are already covered
        for x in fiber_front_list:
            if x in fiber_list_checked:
                fiber_front_list.remove(x)
        #drop_path_list = drop_path_list1
        if len(fiber_front_list) > 0:
            for x in fiber_front_list:
                if x not in fiber_list_checked:
                    fiber_list_checked.append(x)
                    id_sc1 = find_next_sc_id_from_fs_id_primary(x, arr_sc1, arr_fs_end)
                    #print(id_sc1)
                    if id_sc1 != None:
                        # enclosure_type = find_enc_type(id_sc1, arr_sc)
                        # if enclosure_type == 1 or enclosure_type == 5:
                        ODP1_list.append(id_sc1)
                        ODP1_list = find_next_secondary(fiber_list_checked, id_sc1, arr_sc1, arr_fs_end, ODP1_list)
        #print(ODP1_list)
        #print("check")
        return ODP1_list

    ############################
    #
    # # Recursive function to collect all ODP1 splice closure
    # # it will take input of splice closure and find the sheaths connected with from Label and From fields
    def find_next_primary(fiber_list_checked, id_sc1, arr_sc1, arr_fs_end, ODP1_list):
        fiber_front_list = find_front_fs_from_sc_primary(id_sc1, arr_sc1, arr_fs_start)
        # print("primary")
        # print(fiber_front_list)
        sorted_list = sort_fibre_id_list(fiber_front_list, arr_fs)
        #remove fibers which are already covered
        for x in fiber_front_list:
            if x in fiber_list_checked:
                fiber_front_list.remove(x)
        #drop_path_list = drop_path_list1
        if len(fiber_front_list) > 0:
            for x in fiber_front_list:
                if x not in fiber_list_checked:
                    fiber_list_checked.append(x)
                    id_sc1 = find_next_sc_id_from_fs_id_primary(x, arr_sc1, arr_fs_end)
                    #print(id_sc1)
                    if id_sc1 != None:
                        # enclosure_type = find_enc_type(id_sc1, arr_sc)
                        # if enclosure_type == 1 or enclosure_type == 5:
                        ODP1_list.append(id_sc1)
                        ODP1_list = find_next_primary(fiber_list_checked, id_sc1, arr_sc1, arr_fs_end, ODP1_list)
        #print(ODP1_list)
        #print("check")
        return ODP1_list

    #
    # # Recursive function to collect all ODP1 splice closure
    # # it will take input of splice closure and find the sheaths connected with from Label and From fields
    def find_next(fiber_list_checked, id_sc1, arr_sc1, arr_fs_end, ODP1_list):
        fiber_front_list = find_front_fs_from_sc(id_sc1, arr_sc1, arr_fs_start)
        #print(fiber_front_list)
        #remove fibers which are already covered
        for x in fiber_front_list:
            if x in fiber_list_checked:
                fiber_front_list.remove(x)
        #drop_path_list = drop_path_list1
        if len(fiber_front_list) > 0:
            for x in fiber_front_list:
                if x not in fiber_list_checked:
                    fiber_list_checked.append(x)
                    id_sc1 = find_next_sc_id_from_fs_id(x, arr_sc1, arr_fs_end)
                    #print(id_sc1)
                    if id_sc1 != None:
                        # enclosure_type = find_enc_type(id_sc1, arr_sc)
                        # if enclosure_type == 1 or enclosure_type == 5:
                        ODP1_list.append(id_sc1)
                        ODP1_list = find_next(fiber_list_checked, id_sc1, arr_sc1, arr_fs_end, ODP1_list)
        #print(ODP1_list)
        #print("check")
        return ODP1_list
    #
    #
    ###############

    ###########################

    # Recursive function to collect all ODP1 splice closure
    # it will take input of splice closure and find the sheaths connected with from Label and From fields
    def find_next_olt(fiber_list_checked, id_sc1, arr_sc1, arr_fs_end, counter_list, branch_list, counter, counter_list_secondary, counter_sec, fiber_list_checked_sec, branch_list_sec):
        #global counter
        fiber_front_list = find_front_fs_from_sc(id_sc1, arr_sc1, arr_fs_start)

        sorted_list = sort_fibre_id_list(fiber_front_list, arr_fs)

        find_unique_list = sort_unique_ids(sorted_list)

        ###### For Secondary List #######
        fiber_front_list_sec = find_front_fs_from_sc_all(id_sc1, arr_sc1, arr_fs_start)

        sorted_list_sec = sort_fibre_id_list_all(fiber_front_list_sec, arr_fs)

        find_unique_list_sec = sort_unique_ids(sorted_list_sec)
        # arcpy.AddMessage("branch sorting sec")
        # arcpy.AddMessage(find_unique_list_sec)
        #
        if len(find_unique_list) > 1:
            for i in range(1, len(find_unique_list)):
                branch_list.append(find_unique_list[i])
        #
        #         if len(branch_list) > 0:
        #             for fs_id in branch_list:
        #                 sc_id_arrays = find_next_secondary_branch(fs_id[0], arr_sc, arr_fs_end, arr_fs_start)
        #                 #print(sc_id_arrays)
        #                 for rec in sc_id_arrays:
        #                     if len(branch_list_sec) == 0:
        #                         branch_list_sec.append(rec)
        #                     elif rec not in branch_list_sec:
        #                         branch_list_sec.append(rec)
        #             #branch_list_sec.append(sc_id_arrays)
        #
        # check_sec = []

        if len(find_unique_list_sec) > 1:
            for i in range(1, len(find_unique_list_sec)):
                for j in sorted_list_sec:
                    if find_unique_list_sec[i][0] in j:
                        #print(j[2])
                        branch_list_sec.append(find_unique_list_sec[i][0])

        #print(branch_list_sec)


            # if len(branch_list_sec) > 0:
            #     for fs_id in branch_list_sec:
            #         sc_id_arrays = find_next_secondary_branch(fs_id[0], arr_sc, arr_fs_end, arr_fs_start)
            #         print(sc_id_arrays)
            #         for rec in sc_id_arrays:
            #             if len(branch_list_sec) == 0:
            #                 branch_list_sec.append(rec)
            #             elif rec not in branch_list_sec:
            #                 branch_list_sec.append(rec)
            #
            #     print(branch_list_sec)


        #
        # #remove fibers which are already covered
        # for x in sorted_list:
        #     if x in fiber_list_checked:
        #         sorted_list.remove(x)
        #
        # remove fibers sec which are already covered
        for x in sorted_list_sec:
            if x in fiber_list_checked_sec:
                sorted_list_sec.remove(x)
        # #drop_path_list = drop_path_list1
        # array_sec = []
        #
        if len(sorted_list) > 0:
            for x in sorted_list:
                if x not in fiber_list_checked:
                    fiber_list_checked.append(x)
                    id_sc1 = find_next_sc_id_from_fs_id(x[0], arr_sc1, arr_fs_end)
                    #print(id_sc1)
                    if id_sc1 != None:
                        counter = counter + 1
                        # enclosure_type = find_enc_type(id_sc1, arr_sc)
                        # if enclosure_type == 1 or enclosure_type == 5:
                        #     ODP1_list.append(id_sc1)

                        counter_list.append([x, id_sc1, counter])
                        counter_list1 = find_next_olt(fiber_list_checked, id_sc1, arr_sc1, arr_fs_end, counter_list, branch_list, counter, counter_list_secondary, counter_sec, fiber_list_checked_sec, branch_list_sec)
                        print(counter_list1)

                        if len(counter_list1) > 0:
                            counter_list = counter_list1[0]
                            branch_list = counter_list1[1]
                            counter_list_secondary = counter_list1[2]
                            branch_list_sec = counter_list1[3]

                        else:
                            counter_list = ''
                            branch_list = ''
                            counter_list_secondary = ''
                            branch_list_sec = ''

        # if len(sorted_list_sec) > 0:
        #     for x in sorted_list_sec:
        #         if x not in fiber_list_checked_sec:
        #             fiber_list_checked_sec.append(x)
        #             id_sc1 = find_next_sc_id_from_fs_id(x[0], arr_sc1, arr_fs_end)
        #             #print(id_sc1)
        #             if id_sc1 != None:
        #                 counter_sec = counter_sec + 1
        #                 counter_list_secondary.append([x, id_sc1, counter_sec])
        #
        #                 counter_list2 = find_next_olt(fiber_list_checked, id_sc1, arr_sc1, arr_fs_end, counter_list,
        #                                               branch_list, counter, counter_list_secondary, counter_sec, fiber_list_checked_sec, branch_list_sec)
        #                 if len(counter_list2) > 0:
        #                     counter_list = counter_list2[0]
        #                     branch_list = counter_list2[1]
        #                     counter_list_secondary = counter_list2[2]
        #                     branch_list_sec = counter_list2[3]
        #                     # try:
        #                     #     print("line 433")
        #                     #     print(counter_list2[3])
        #                     #     if len(counter_list2[3]) > 0:
        #                     #         for i in counter_list2[3]:
        #                     #             for j in i:
        #                     #                 if len(array_sec) == 0:
        #                     #                     array_sec.append(j)
        #                     #                 elif j not in array_sec:
        #                     #                     array_sec.append(j)
        #                     # except Exception as ex:
        #                     #     print(ex)
        #                 else:
        #                     counter_list = ''
        #                     branch_list = ''
        #                     counter_list_secondary = ''
        #                     branch_list_sec = ''

        #print(counter_list)
        #print("check")
        return [counter_list, branch_list, counter_list_secondary, branch_list_sec]

    def find_next_all_sc_id(id_fs1, arr_sc, arr_fs_end, arr_fs_start):
        array = []
        id_sc = find_next_sc_id_from_fs_id(id_fs1, arr_sc, arr_fs_end)
        array.append(id_sc)
        array = find_next([], id_sc, arr_sc, arr_fs_end, array)
        return array

    def find_next_all_sc_id_primary(id_fs1, arr_sc, arr_fs_end, arr_fs_start):
        array = []
        id_sc = find_next_sc_id_from_fs_id_primary(id_fs1, arr_sc, arr_fs_end)
        array.append(id_sc)
        array = find_next_primary([], id_sc, arr_sc, arr_fs_end, array)
        return array

    ############ New Function To Find All Secondary #############
    def find_next_all_sc_id_secondary(id_fs1, arr_sc, arr_fs_end, arr_fs_start, fs_lists):
        array = []
        branching = ''
        cable_type = ''
        coor_end = ''
        for i in arr_fs_end:
            if id_fs1 == i[0]:
                cable_type = i[4]
                coor_end = i[3]
                break
        if cable_type == "Secondary":
            branching = 'Secondary'
            id_sc = find_next_sc_id_from_fs_id_secondary(id_fs1, arr_sc, arr_fs_end)
            array.append(id_sc)
            array = find_next([], id_sc, arr_sc, arr_fs_end, array)
            record = find_secondary_sorted_lists(array, arr_sc, arr_fs_end, arr_fs_start)
            data = {"type": branching, "rec": record[0], "branch": record[1]}
        elif cable_type == "Primary":
            branching = 'Primary'
            array = find_next_all_secondary_splice([], id_fs1, coor_end, arr_sc, arr_fs_end, fs_lists, arr_fs_start)
            print(array)

            data = {"type": branching, "rec": array[0], "branch": array[1], "fs_chk_list": array[2], "status": array[3]}
        return data

    def find_next_secondary_branch(fs_id, arr_sc, arr_fs_end, arr_fs_start):
        array = []
        id_sc = find_next_sc_id_from_fs_id_secondary(fs_id, arr_sc, arr_fs_end)
        array.append(id_sc)
        array = find_next_secondary([], id_sc, arr_sc, arr_fs_end, array)
        return array

    ##############

    def find_secondary_sorted_lists(lists, arr_sc, arr_fs_end, arr_fs_start):
        records = []
        arrays_first_chks = []
        array_all_check = []
        branching_list_sec = []
        for i in lists:
            primary_list = []
            fiber_front_list_sec = find_front_fs_from_sc_all(i, arr_sc, arr_fs_start)
            sorted_list_sec = sort_fibre_id_list_all_sec(fiber_front_list_sec, arr_fs)
            find_unique_list_sec = sort_unique_ids(sorted_list_sec)
            arcpy.AddMessage('secondary')
            arcpy.AddMessage(find_unique_list_sec)
            if len(find_unique_list_sec) > 0:
                for j in range(1, len(find_unique_list_sec)):
                    branching_list_sec.append(find_unique_list_sec[j])

            if i not in array_all_check:
                #print(i)
                coor_sc1 = find_coor_from_id_sc(i, arr_sc)
                ##check equality with start coordinate of fiber sheath array
                for x in arr_fs_end:
                    if same(coor_sc1, x[3], 0.1) and str(x[4]) == "Secondary":
                        arrays_first_chks.append([i, x[5]])
                        array_all_check.append(i)


            for j in sorted_list_sec:
                if j[2] == "Secondary":
                    sc_id = find_next_sc_id_from_fs_id_secondary(j[0], arr_sc, arr_fs_end)
                    records.append([sc_id, j[1]])
                    array_all_check.append(sc_id)



        if len(arrays_first_chks) > 0:
            for lists in arrays_first_chks:
                records.append([lists[0], lists[1]])
        sorted_list = sorted(records, key=lambda x: x[1], reverse=True)
        return [sorted_list, branching_list_sec]

    ####################### Secondary Branching Find All Splice Used ################################
    def find_next_all_secondary_splice(records, fs_id, coor_end, arr_sc, arr_fs_end, fs_lists, arr_fs_start):
        next_splice_closure_id = find_next_sc(coor_end, arr_sc)
        print(next_splice_closure_id)
        # print("fs_id - "+ str(fs_id))
        # print(next_splice_closure_id)
        array = find_next([], next_splice_closure_id, arr_sc, arr_fs_end, [next_splice_closure_id])
        arcpy.AddMessage(array)
        array_check = []
        array_all_check = []
        data = []
        branching_list_sec = []
        arrays_first_chks = []
        end_splice_list = []
        loop_passed = False
        fs_id_chk = ''
        for i in array:
            primary_list = []
            fiber_front_list_sec = find_front_fs_from_sc_all(i, arr_sc, arr_fs_start)
            sorted_list_sec = sort_fibre_id_list_all_sec(fiber_front_list_sec, arr_fs)
            print(sorted_list_sec)
            # print(len(sorted_list_sec))
            #
            # if len(sorted_list_sec) > 2:
            #     a_del = sorted_list_sec.pop(len(sorted_list_sec))
            #     sorted_list_sec = a_del



            find_unique_list_sec = sort_unique_ids(sorted_list_sec)

            if len(find_unique_list_sec) > 1:
                for j in range(1, len(find_unique_list_sec)):
                    branching_list_sec.append(find_unique_list_sec[j])

            if i not in array_all_check and len(sorted_list_sec) == 0:
                #print(i)
                coor_sc1 = find_coor_from_id_sc(i, arr_sc)
                ##check equality with start coordinate of fiber sheath array
                for x in arr_fs_end:
                    if same(coor_sc1, x[3], 0.1) and str(x[4]) == "Secondary":
                        if i not in data:
                            arrays_first_chks.append([i, x[5]])
                            array_check.append(i)
                            array_all_check.append(i)
                            data.append(i)

            if len(sorted_list_sec) > 0 and i not in array_all_check:
                coor_sc1 = find_coor_from_id_sc(i, arr_sc)
                ##check equality with start coordinate of fiber sheath array
                for x in arr_fs_end:
                    if same(coor_sc1, x[3], 0.1) and str(x[4]) == "Secondary":
                        if i not in data:
                            records.append([i, x[5]])
                            array_check.append(i)
                            array_all_check.append(i)
                            data.append(i)

            for j in sorted_list_sec:
                if j[2] == "Primary":
                    primary_list.append(1)
                if j[2] == "Secondary":
                    #print(j)
                    sc_id = find_next_sc_id_from_fs_id_secondary(j[0], arr_sc, arr_fs_end)
                    # print(sc_id)
                    # print(data)
                    if sc_id in array:
                        if sc_id not in data:
                            array_check = find_next([], sc_id, arr_sc, arr_fs_end, array_check)
                            records.append([sc_id, j[1]])
                            array_all_check.append(sc_id)
                            #array_check.append(sc_id)
                            data.append(sc_id)
                        #print(array_check)
                        for k in array_check:
                            if k not in data:
                                coor_sc1 = find_coor_from_id_sc(k, arr_sc)
                                for x in arr_fs_end:
                                    if same(coor_sc1, x[3], 0.1) and str(x[4]) == "Secondary":
                                        data.append(k)
                                        #array_check.append(k)
                                        array_all_check.append(k)
                                        records.append([k, x[5]])

            if len(primary_list) > 1:
                loop_passed = True
                break

            # if len(sorted_list_sec)  > 2:
            #     sorted_list = sorted(records, key=lambda x: x[1], reverse=True)
            #     loop_passed = True
            #     break;
        #print(records)

        #if loop_passed != True:
        sorted_list = sorted(records, key=lambda x: x[1], reverse=True)
        if len(arrays_first_chks) > 0:
            for lists in arrays_first_chks:
                sorted_list.append([lists[0], lists[1]])
        arcpy.AddMessage("f chks")
        arcpy.AddMessage(arrays_first_chks)
        arcpy.AddMessage(sorted_list)
        # else:
        #     fs_id_chk = fs_id
        #     if len(arrays_first_chks) > 0:
        #         for lists in arrays_first_chks:
        #             sorted_list.append([lists[0], lists[1]])


        return [sorted_list, branching_list_sec, fs_id, loop_passed]


    def find_odp2_list_splice(fs_id, arr_sc, arr_fs_end, arr_fs_start, branch_list_sec):
        coor_end = ''
        records = []
        for i in arr_fs_end:
            if fs_id == i[0]:
                cable_type = i[4]
                coor_end = i[3]
                break
        next_splice_closure_id = find_next_sc(coor_end, arr_sc)
        # print("fs_id - "+ str(fs_id))
        # print(next_splice_closure_id)
        array = find_next([], next_splice_closure_id, arr_sc, arr_fs_end, [])
        # print(array)
        array_check = []
        branching_list_sec = []
        data = []
        for i in array:
            fiber_front_list_sec = find_front_fs_from_sc_all(i, arr_sc, arr_fs_start)
            sorted_list_sec = sort_fibre_id_list_all_sec(fiber_front_list_sec, arr_fs)
            # print(sorted_list_sec)

            find_unique_list_sec = sort_unique_ids(sorted_list_sec)

            if len(find_unique_list_sec) > 1:
                for j in range(1, len(find_unique_list_sec)):
                    branching_list_sec.append(find_unique_list_sec[j])

            if i not in array_check:
                coor_sc1 = find_coor_from_id_sc(i, arr_sc)
                ##check equality with start coordinate of fiber sheath array
                for x in arr_fs_end:
                    if same(coor_sc1, x[3], 0.1) and str(x[4]) == "Secondary":
                        if i not in data:
                            records.append([i, x[5]])
                            array_check.append(i)
                            data.append(i)

            for j in sorted_list_sec:
                if j[2] == "Secondary":
                    # print(j)
                    sc_id = find_next_sc_id_from_fs_id_secondary(j[0], arr_sc, arr_fs_end)
                    # print(sc_id)
                    # print(data)
                    if sc_id in array:
                        if sc_id not in data:
                            array_check = find_next([], sc_id, arr_sc, arr_fs_end, array_check)
                            records.append([sc_id, j[1]])
                            array_check.append(sc_id)
                            data.append(sc_id)
                        # print(array_check)
                        for k in array_check:
                            if k not in data:
                                data.append(k)
                                array_check.append(k)
                                records.append([k, 0])

                        # print(sc_id)

        return [records, branching_list_sec]


    arcpy.AddMessage("arr_fs_end_olt")
    arcpy.AddMessage(arr_fs_end_olt)
    counter_list_all = []
    branch_list_all = []
    main_branch_counter = []
    def splice_labeling():
        arcpy.AddMessage("Splice Closure Labeling started")
        for x in arr_fs_end_olt:
            arcpy.AddMessage("OLT cable id - "+ str(x[0]))
            counter = 1
            counter_sec = 1
            print(x)
            fiber_list_checked = []
            fiber_list_checked_sec = []
            branch_list = []
            branch_list_sec = []
            counter_list = []
            counter_list_secondary = []
            coor_end =  x[3]
            arcpy.AddMessage(coor_end)
            next_splice_closure = find_next_sc(coor_end, arr_sc)
            #enclosure_type = find_enc_type(next_splice_closure,arr_sc)

            arcpy.AddMessage(next_splice_closure)
            #sys.exit()
            ######## GET Cable Type
            if str(x[4]) == "Primary":
                #  append on counter_list - [nextspliceclosureID, counter]
                if next_splice_closure:
                    counter_list.append([x, next_splice_closure, counter])

            if str(x[4]) == "Secondary":
                #  append on counter_list - [nextspliceclosureID, counter]
                if next_splice_closure:
                    counter_list_secondary.append([x, next_splice_closure, counter_sec])
            #counter = counter + 1
            if next_splice_closure:
            #goto findnext_olt()
                counter_list1 = find_next_olt(fiber_list_checked, next_splice_closure, arr_sc, arr_fs_end, counter_list, branch_list, counter, counter_list_secondary, counter_sec, fiber_list_checked_sec, branch_list_sec)
                counter_list = counter_list1[0]
                branch_list = counter_list1[1]
                counter_list_secondary = counter_list1[2]
                branch_list_sec = counter_list1[3]

                # print(counter_list_secondary)
                print("branch list")
                # arcpy.AddMessage(branch_list)
                # arcpy.AddMessage(counter_list)
                # arcpy.AddMessage("counter_list_secondary")
                # arcpy.AddMessage(counter_list_secondary)

                branching_list = []
                branching_splice_list = []
                main_branch_counter = 0
                counter_primary = 0

                if len(branch_list) > 0:
                    for i in branch_list:
                        branching_list.append(i[0])

                for fs_id in branching_list:
                    sc_id_array = find_next_all_sc_id(fs_id, arr_sc, arr_fs_end, arr_fs_start)
                    for id in sc_id_array:
                        branching_splice_list.append(id)

                ############################## Main Branch Cable Labeling ##################################
                arcpy.AddMessage("Splice Closure Labeling for Primary Main Cable")
                last_sc_id = ''
                counters = 0
                for x in counter_list:
                    fs_id = int(x[1])
                    with arcpy.da.UpdateCursor(Splice_Closures, ['OBJECTID', 'label', "temp_ribbon"]) as cur:
                        for row in cur:
                            if row[0] == fs_id and row[0] not in branching_splice_list:
                                label = location + '/' + str(row[2]) + f'{x[2]:03}'
                                last_sc_id = fs_id
                                row[1] = label
                                counters = x[2]
                            cur.updateRow(row)
                    del cur
                    main_branch_counter = counters

                ############################ Branch Cable Labeling ####################################
                # for y in branching_splice_list:
                #     sc_id = int(y)
                #     main_branch_counter = main_branch_counter + 1
                #     with arcpy.da.UpdateCursor(Splice_Closures, ['OriginalOID', 'new_label', "ribbon"]) as cur:
                #         for row in cur:
                #             if row[0] == sc_id and row[0] in branching_splice_list:
                #                 label = "PGS" + '/' + str(row[2]) + f'{main_branch_counter:03}'
                #                 row[1] = label
                #             cur.updateRow(row)
                #     del cur

                ########## Primary Branching Lists ############
                counters_branching = 0
                arcpy.AddMessage("Splice Closure Labeling for Primary Main Branch Cable")
                for fs_id in branching_list:
                    sc_id_array = find_next_all_sc_id_primary(fs_id, arr_sc, arr_fs_end, arr_fs_start)
                    #branch_splice_labeling(main_branch_counter, sc_id_array)

                    for x in sc_id_array:
                        sc_id = int(x)

                        with arcpy.da.UpdateCursor(Splice_Closures, ['OBJECTID', 'label', "temp_ribbon"]) as cur:
                            for row in cur:
                                if row[0] == sc_id and str(row[1]) == "":
                                    main_branch_counter = main_branch_counter + 1

                                    label = location + '/' + str(row[2]) + f'{main_branch_counter:03}'
                                    row[1] = label
                                cur.updateRow(row)
                        del cur
                    counter_primary = main_branch_counter

                    #print(counter_primary)

                #print("last main splice")
                #print(last_sc_id)
                ############### Find Main Branch For Secondary #####################
                # splice_list = []
                # for x in counter_list:
                #
                #     fs_id = int(x[1])
                #     secnodary_splice_list = find_next_secondary_branch(fs_id, arr_sc, arr_fs_end, arr_fs_start)
                #     if len(secnodary_splice_list) > 0:
                #         for i in secnodary_splice_list:
                #             if i not in splice_list and i not in branching_splice_list:
                #                 splice_list.append(i)
                #
                # print(splice_list)

                # for x in counter_list_secondary:
                #     sc_id = int(x[1])
                #     with arcpy.da.UpdateCursor(Splice_Closures, ['OriginalOID', 'new_label', "ribbon"]) as cur:
                #         for row in cur:
                #             if row[0] == sc_id and row[0] not in branching_splice_list:
                #                 label = "PGS" + '/' + str(row[2]) + f'{x[2]:03}'
                #                 row[1] = label
                #             cur.updateRow(row)
                #     del cur


                ########################## 15.07.2021(secondary cable branching) #########################
                arcpy.AddMessage("Splice Closure Labeling for Secondary Branch Cable")
                for fs_id in branch_list_sec:
                    #arcpy.AddMessage(fs_id)
                    records = find_next_all_sc_id_secondary(fs_id, arr_sc, arr_fs_end, arr_fs_start, branch_list_sec)
                    #branch_splice_labeling(main_branch_counter, sc_id_array)
                    #arcpy.AddMessage(records)
                    branching_list1 = []
                    branching_splice_list1 = []
                    if records["type"] == "Secondary":
                        sc_id_array = records["rec"]
                        branch_list1 = records["branch"]
                        if len(branch_list1) > 0:
                            for i in branch_list1:
                                print(i)
                                if i[2] == "Secondary":
                                    branching_list1.append(i[0])
                        for fs_id in branching_list1:
                            sc_id_array1 = find_next_all_sc_id(fs_id, arr_sc, arr_fs_end, arr_fs_start)
                            print(sc_id_array1)
                            for id in sc_id_array1:
                                branching_splice_list1.append(id)
                        for x in sc_id_array:
                            sc_id = int(x[0])
                            with arcpy.da.UpdateCursor(Splice_Closures, ['OBJECTID', 'label', "temp_ribbon"]) as cur:
                                for row in cur:
                                    if row[0] == sc_id and str(row[1]) == "" and sc_id not in branching_splice_list1:
                                        main_branch_counter = main_branch_counter + 1
                                        label = location + '/' + str(row[2]) + f'{main_branch_counter:03}'
                                        row[1] = label
                                    cur.updateRow(row)
                            del cur
                        for x in branching_splice_list1:
                            sc_id = int(x)
                            with arcpy.da.UpdateCursor(Splice_Closures, ['OBJECTID', 'label', "temp_ribbon"]) as cur:
                                for row in cur:
                                    if row[0] == sc_id and str(row[1]) == "":
                                        main_branch_counter = main_branch_counter + 1
                                        label = location + '/' + str(row[2]) + f'{main_branch_counter:03}'
                                        row[1] = label
                                    cur.updateRow(row)
                            del cur

                    elif records["type"] == "Primary":
                        if records["status"] == False:
                            sc_id_array = records["rec"]
                            branch_list1 = records["branch"]
                            if len(branch_list1) > 0:
                                for i in branch_list1:
                                    print(i)
                                    if i[2] == "Secondary":
                                        branching_list1.append(i[0])
                            #arcpy.AddMessage(branching_list1)
                            for fs_id in branching_list1:
                                sc_id_array1 = find_next_all_sc_id(fs_id, arr_sc, arr_fs_end, arr_fs_start)
                                print(sc_id_array1)
                                for id in sc_id_array1:
                                    branching_splice_list1.append(id)
                            # arcpy.AddMessage("secondary branching list")
                            # arcpy.AddMessage(branching_splice_list1)
                            for x in sc_id_array:
                                sc_id = int(x[0])
                                with arcpy.da.UpdateCursor(Splice_Closures, ['OBJECTID', 'label', "temp_ribbon"]) as cur:
                                    for row in cur:
                                        if row[0] == sc_id and str(row[1]) == "" and sc_id not in branching_splice_list1:
                                            main_branch_counter = main_branch_counter + 1
                                            label = location + '/' + str(row[2]) + f'{main_branch_counter:03}'
                                            row[1] = label
                                        cur.updateRow(row)
                                del cur

                            for x in branching_splice_list1:
                                sc_id = int(x)
                                with arcpy.da.UpdateCursor(Splice_Closures, ['OBJECTID', 'label', "temp_ribbon"]) as cur:
                                    for row in cur:
                                        if row[0] == sc_id and str(row[1]) == "":
                                            main_branch_counter = main_branch_counter + 1
                                            label = location + '/' + str(row[2]) + f'{main_branch_counter:03}'
                                            row[1] = label
                                        cur.updateRow(row)
                                del cur
                        else:

                            new_lists = find_odp2_list_splice(records["fs_chk_list"], arr_sc, arr_fs_end, arr_fs_start, branch_list_sec)
                            #print(new_lists)
                            if len(new_lists) > 0:
                                sc_id_array = new_lists[0]
                                if len(new_lists[1]) > 0:
                                    for i in new_lists[1]:
                                        print(i)
                                        if i[2] == "Secondary":
                                            branching_list1.append(i[0])
                                # arcpy.AddMessage(branching_list1)
                                for fs_id in branching_list1:
                                    sc_id_array1 = find_next_all_sc_id(fs_id, arr_sc, arr_fs_end, arr_fs_start)
                                    print(sc_id_array1)
                                    for id in sc_id_array1:
                                        branching_splice_list1.append(id)
                                # arcpy.AddMessage("secondary branching list")
                                # arcpy.AddMessage(branching_splice_list1)
                                for x in sc_id_array:
                                    sc_id = int(x[0])
                                    with arcpy.da.UpdateCursor(Splice_Closures, ['OBJECTID', 'label', "temp_ribbon"]) as cur:
                                        for row in cur:
                                            if row[0] == sc_id and str(row[1]) == "" and sc_id not in branching_splice_list1:
                                                main_branch_counter = main_branch_counter + 1
                                                label = location + '/' + str(row[2]) + f'{main_branch_counter:03}'
                                                row[1] = label
                                            cur.updateRow(row)
                                    del cur

                                for x in branching_splice_list1:
                                    sc_id = int(x)
                                    with arcpy.da.UpdateCursor(Splice_Closures, ['OBJECTID', 'label', "temp_ribbon"]) as cur:
                                        for row in cur:
                                            if row[0] == sc_id and str(row[1]) == "":
                                                main_branch_counter = main_branch_counter + 1
                                                label = location + '/' + str(row[2]) + f'{main_branch_counter:03}'
                                                row[1] = label
                                            cur.updateRow(row)
                                    del cur

                ###################### Main Branch Cable ID ####################
                array_last = []
                array_last = find_next([], last_sc_id, arr_sc, arr_fs_end, array_last)
                #print(array_last)
                for x in array_last:
                    sc_id = int(x)
                    with arcpy.da.UpdateCursor(Splice_Closures, ['OBJECTID', 'label', "temp_ribbon"]) as cur:
                        for row in cur:
                            if row[0] == sc_id and str(row[1]) == "":
                                main_branch_counter = main_branch_counter + 1
                                label = location + '/' + str(row[2]) + f'{main_branch_counter:03}'
                                row[1] = label
                            cur.updateRow(row)
                    del cur

                if len(counter_list_secondary) == 1:
                    for x in counter_list_secondary:
                        arcpy.AddMessage("ish splice id "+str(x[1]))
                        sc_id = int(x[1])
                        ish_counter = 1
                        with arcpy.da.UpdateCursor(Splice_Closures, ['OBJECTID', 'label', "temp_ribbon"]) as cur:
                            for row in cur:
                                if row[0] == sc_id and str(row[1]) == "":
                                    label = location + '/' + str(row[2]) + f'{ish_counter:03}'
                                    row[1] = label
                                cur.updateRow(row)
                        del cur


    splice_labeling()

    ################ Update FIbre Sheath & Splice Closures ############################
    arcpy.AddMessage("Splice Closure Label Update on Shape File")
    # arr_splc_list = arcpy.da.FeatureClassToNumPyArray(Splice_Closures, ("new_label", "OriginalOID"), skip_nulls=True)
    # arr_fibre_list = arcpy.da.FeatureClassToNumPyArray(Fibre_Sheath, ("Cable_type", "ribbon", "OriginalOID"), skip_nulls=True)

    ############ Update Tool ############
    #Wrong approach of choosing ISH cable
    # try:
    #     array_ish_splice = []
    #     arcpy.Select_analysis(Fibre_Sheath, "ISH_CABLE", "ODP1_Count = 0 and ODP2_Count = 0")
    #     arry_ish = arcpy.da.FeatureClassToNumPyArray("ISH_CABLE", ("Cable_type", "temp_ribbon", "OriginalOID", "type"), skip_nulls=True)
    #     for i in arry_ish:
    #         splice_id = find_next_sc_id_from_fs_id(i[2], arr_sc, arr_fs_end)
    #         array_ish_splice.append([splice_id, i[3]])
    #
    #     for rec in array_ish_splice:
    #         with arcpy.da.UpdateCursor(Splice_Closures, ['OBJECTID', 'label']) as cur:
    #             for row in cur:
    #                 if row[0] == int(rec[0]):
    #                     label = location + '/' + "Z001"
    #                     row[1] = label
    #                 cur.updateRow(row)
    #         del cur
    # except arcpy.ExecuteError as e:
    #     print(e)

    #arr_sc_updated = arcpy.da.FeatureClassToNumPyArray(Splice_Closures, ("OBJECTID", "label"), skip_nulls=False)
    arr_sc_updated = arcpy.da.FeatureClassToNumPyArray(Splice_Closures, ("GlobalID", "label"), skip_nulls=False)
    # arcpy.AddMessage("arr_sc_updated")
    # arcpy.AddMessage(arr_sc_updated)



    cable_size = {
        1: 12,
        2: 24,
        3: 48,
        4: 96,
        5: 144,
        6: 12,
        7: 24,
        8: 48,
        9: 96,
        10: 144
    }


    ########################## Update Fibre Sheath ############################
    arr_sc_new = arcpy.da.FeatureClassToNumPyArray(Splice_Closures, fields_sc, skip_nulls=False)
    #arcpy.AddMessage(arr_sc_new)
    with arcpy.da.UpdateCursor(Fibre_Sheath, ['OBJECTID', 'label', 'SHAPE@XY', 'SHAPE@', 'type']) as cur:
        for row in cur:
            sc_label1 = ""
            label = ""
            if row[0] != None:
                endpt = row[3].lastPoint
                endgmt = (endpt.X, endpt.Y)
                # arcpy.AddMessage("OID"+str(row[0]))
                # arcpy.AddMessage(str(row[2]))
                # arcpy.AddMessage(endgmt)
                #print(str(row[0]))
                sc_label = find_next_sc1(endgmt, arr_sc_new)
                sc_label1 = find_next_sc1(row[2], arr_sc_new)
                #arcpy.AddMessage("sclabel" + str(sc_label))
                #arcpy.AddMessage("sclabel1" + str(sc_label1))
                #if sc_label != None:
                type = f'{cable_size[row[4]]:03}'
                label = str(sc_label1) +'/'+ str(type)+'F'
                row[1] = label
                cur.updateRow(row)
        del cur



    # sts1 = ApplyUpdates(arr_sc_updated, fl1, ['label'])
    # if sts1 == True:
    #     arcpy.AddMessage("Label Updated Successfully on Splice Closure Live Data.")

    #arr_fs_updated = arcpy.da.FeatureClassToNumPyArray(Fibre_Sheath, ("OBJECTID", "temp_cable_class", "label",), skip_nulls=True)

    fset1 = fl1.query()  # Return a set of features from the layer
    features1 = fset1.features  # Get features object
    if len(features1) == 0:
        print("No features found in '%s' layer", warning=True)
        print('Exiting ...')
        exit()
    else:
        # print("Extracted %s features from '%s' layer" % (len(features)))
        pass

    dictLengths1 = {}
    for item in features1:
        gID = item.attributes["GlobalID"]
        match_str = '{' + gID + '}'
        #arcpy.AddMessage(match_str)
        gID1 = match_str.upper()
        #arcpy.AddMessage(str(gID1))
        for data in arr_sc_updated:
            if str(gID1) == str(data[0]):
                dictLengths1[gID] = data[1]
    # do not round all dictionary items to the nearest integer
    # for item in dictLengths:
    #     dictLengths[item] = int(round(dictLengths[item]))

    arcpy.AddMessage(len(dictLengths1))
    # Update feature layer
    if len(list(dictLengths1.keys())) > 0:
        ApplyUpdates(fl1, features1, ["label"], dictLengths1)
    else:
        arcpy.AddMessage("No records to process in Splice Closure")

    #arr_fs_updated = arcpy.da.FeatureClassToNumPyArray(Fibre_Sheath, ("GlobalID", "temp_cable_class", "label",), skip_nulls=True)
    arr_fs_updated = arcpy.da.FeatureClassToNumPyArray(Fibre_Sheath, ("OBJECTID", "temp_cable_class", "label",), skip_nulls=True)

    # arcpy.AddMessage(len(arr_fs_updated))
    # arcpy.AddMessage(arr_fs_updated)
    #
    sts = ApplyUpdates_old(arr_fs_updated, fl, ['temp_cable_class', 'label'])
    # ########### Update Live Data ############
    if sts == True:
        arcpy.AddMessage("Cable Class Updated Successfully on Fibre Sheath Live Data.")

    # fset = fl.query()  # Return a set of features from the layer
    # features = fset.features  # Get features object
    # if len(features) == 0:
    #     print("No features found in '%s' layer", warning=True)
    #     print('Exiting ...')
    #     exit()
    # else:
    #     # print("Extracted %s features from '%s' layer" % (len(features)))
    #     pass
    #
    # dictLengths = {}
    # for item in features:
    #     gID2 = item.attributes["GlobalID"]
    #     match_str1 = '{' + gID2 + '}'
    #     arcpy.AddMessage(match_str1)
    #     gID3 = match_str1.upper()
    #     arcpy.AddMessage(gID3)
    #     for datas in arr_fs_updated,        :
    #         if str(gID3) == str(datas[0]):
    #             arcpy.AddMessage("matched "+str(gID2))
    #             dictLengths[gID2] = (datas[1], datas[2])
    # # do not round all dictionary items to the nearest integer
    # # for item in dictLengths:
    # #     dictLengths[item] = int(round(dictLengths[item]))
    #
    # arcpy.AddMessage("len(dictLengths)")
    # arcpy.AddMessage(len(dictLengths))
    # # Update feature layer
    # if len(list(dictLengths.keys())) > 0:
    #     ApplyUpdates(fl, features, ["temp_cable_class","label"], dictLengths)
    else:
        arcpy.AddMessage("No records to process in Fibre Sheath.")

else:
    if len(array_overlaping_splice) != 0:
        arcpy.AddMessage("overlapping splice_closures present in the dataset")
        for sp_id in array_overlaping_splice:
            arcpy.AddMessage("overlapping splice_closures id - "+str(sp_id)+" present in the dataset")

    for id in array_snapping_id:
        arcpy.AddMessage("Fibre Sheath ID "+str(id)+" not Snapped to Splice Closure")

    if loop_check == True:
        arcpy.AddMessage("Fibre Sheath Network has loophole exist, Please correct it and then run the tool.")
        for id in loop_hole_id:
            arcpy.AddMessage("Fibre Sheath Network id - "+str(id)+" has loophole exist, Please correct it and then run the tool.")

    if len(array_ribbon) != 0:
        for rec_id in array_ribbon:
            arcpy.AddMessage("splice_closures id - "+str(rec_id)+" temp_ribbon field not filled with ribbon.")

if arcpy.Exists("snapinng_fibre_splice"):
    arcpy.Delete_management("snapinng_fibre_splice")

if arcpy.Exists("ISH_CABLE"):
    arcpy.Delete_management("ISH_CABLE")

if arcpy.Exists(localSplice):
    arcpy.Delete_management(localSplice)

if arcpy.Exists(localFibre):
    arcpy.Delete_management(localFibre)


now = datetime.now()
end_time = now.strftime("%H:%M:%S")
arcpy.AddMessage("\nStart Time = {0}".format(start_time))
arcpy.AddMessage("\nEnd Time = {0}".format(end_time))