ToolBox Name: NetworkLabling_v1o4.tbx

ToolSet Name: Overall

Script Name: Drop Labeling New_13.10.2021.py

Input: Splice Closure, Fibre Sheath, Boundaries, Fibre Drop

Tool test: 7. Drop Labeling

Output: Fibre Drop (layer present in the Input geodatabse layer present in the Input geodatabse which is the live layer)

Field(s) to look for: label (Fibre Drop)

Important Note:
1. Tool takes nearly 1 hour approximately to run based on the number of features.
2. Tool is dependent on "name" field of Boundaries, in case "name" field is not filled or not updated then "label" field on Fibre Drop wont be updated correctly.
3. After running the tool remove and add the entire geodatabase again in the TOC to view the changes.
4. Splice Should be properly snapped with the Fibre Drop