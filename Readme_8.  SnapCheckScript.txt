ToolBox Name: NetworkLabling_v1o4.tbx

ToolSet Name: Overall

Script Name: cable_start_end_snap_checks_13.10.2021.py

Input: Splice Closure, Fibre Sheath, Boundaries, Fibre Drop

Tool test: SnapCheckScript

Output: Fibre Sheath ID "(OBJECTID)" not snapped (layer present in the Input geodatabse layer present in the Input geodatabse which is the live layer)

Field(s) to look for: POPUP Message of the Fibre Sheath ID (Fibre Sheath)

Important Note:
1. Tool to check the start and end geometry of the FibreSheath should be properly snapped to a splice closure.
