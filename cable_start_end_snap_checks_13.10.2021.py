import arcpy
import math
import sys
from datetime import datetime
from operator import sub
import os
import arcgis
from arcgis.gis import GIS

x=10000
sys.setrecursionlimit(x)

now = datetime.now()
start_time = now.strftime("%H:%M:%S")


## function to check equality of two coodiate pairs
## returns True or False
## p1, p2 = point coordinates, tol = tolerance
same = lambda p1, p2, tol: sum(x**2 for x in map(sub, p1, p2)) <= tol**2
arcpy.env.overwriteOutput = True
#####################
# arcpy.env.workspace = r"C:\Users\user\Documents\ArcGIS\Projects\Test\Test.gdb"
# #arcpy.env.workspace = "E:\\Rajesh\\Development_Internal\\Development.gdb"
# Splice_Closures = "Splice_Closure"
# Fibre_Sheath = "Fibre_Sheath"
# OLT = "OLT"

Splice_Closures = arcpy.GetParameterAsText(0)
FsName = arcpy.GetParameterAsText(1)
OLT = arcpy.GetParameterAsText(2)
check_label = arcpy.GetParameterAsText(3)

aprx = arcpy.mp.ArcGISProject("CURRENT")

gdbLocal = aprx.defaultGeodatabase
arcpy.AddMessage(gdbLocal)
gdbPath = aprx.homeFolder
gdbName = os.path.basename(gdbPath)
if not arcpy.Exists(gdbLocal):
    arcpy.CreateFileGDB_management(gdbPath, gdbName)

OutputName = "Output"
FcName = "Fibre_Sheath"

p1 = aprx.defaultGeodatabase

p2 = p1 + "\\" + OutputName
Fibre_Sheath = p1 + "\\" + FcName


# Local Copy of Fibre Sheath
Fibre_Sheath = os.path.join(gdbLocal, Fibre_Sheath)  # set the local feature class path
if arcpy.Exists(Fibre_Sheath):
    arcpy.Delete_management(Fibre_Sheath)
feats1 = arcpy.FeatureSet(table=FsName)
feats1.save(Fibre_Sheath)

feats = arcpy.FeatureSet(table=FsName)
feats.save(p2)
arcpy.SelectLayerByAttribute_management(FsName,"CLEAR_SELECTION")

if arcpy.Exists(p2):
    arcpy.AddField_management(p2, "Error", "Text", 50)


arcpy.Merge_management([Splice_Closures, OLT], "splice_olt_merged_record")

if arcpy.Exists(Fibre_Sheath):
    arcpy.AddField_management(Fibre_Sheath, "start_point_x", "Text", 50)
    arcpy.AddField_management(Fibre_Sheath, "start_point_y", "Text", 50)
    arcpy.AddField_management(Fibre_Sheath, "end_point_x", "Text", 50)
    arcpy.AddField_management(Fibre_Sheath, "end_point_y", "Text", 50)



with arcpy.da.UpdateCursor(Fibre_Sheath, ["start_point_x", "start_point_y", "end_point_x", "end_point_y", "SHAPE@"]) as cursor:
    for row in cursor:
        if row[4] != None:
            start_point = row[4].firstPoint
            end_point = row[4].lastPoint
            row[0] = str(start_point.X)
            row[1] = str(start_point.Y)
            row[2] = str(end_point.X)
            row[3] = str(end_point.Y)
            cursor.updateRow(row)
    del cursor

array_fibre = arcpy.da.FeatureClassToNumPyArray(Fibre_Sheath, ["start_point_x", "start_point_y", "end_point_x", "end_point_y"])

spatial_ref = arcpy.Describe(Fibre_Sheath).spatialReference
if spatial_ref.name == "Unknown":
    arcpy.AddMessage("{0} has an unknown spatial reference".format(Fibre_Sheath))

else:
    arcpy.AddMessage("{0} : {1}".format(Fibre_Sheath, spatial_ref.PCSCode))

pointList = arcpy.Array()
for record in array_fibre:
    if str(record[0]) != 'None' and str(record[1]) != 'None' and str(record[2]) != 'None' and str(record[3]) != 'None':
        start_point = arcpy.Point(record[0], record[1])
        end_point = arcpy.Point(record[2], record[3])
        pointList.append([start_point, end_point])


# Create an empty Point object
point = arcpy.Point()

# A list to hold the PointGeometry objects
pointGeometryList = []

sr = arcpy.SpatialReference(spatial_ref.PCSCode)

# For each coordinate pair, populate the Point object and create a new
# PointGeometry object
for pt in pointList:
    for data in pt:
        pointGeometry = arcpy.PointGeometry(data, sr)
        pointGeometryList.append(pointGeometry)

# Create a copy of the PointGeometry objects, by using pointGeometryList as
# input to the CopyFeatures tool.

arcpy.CopyFeatures_management(pointGeometryList, "Test_Geometry")


########## Spatial Join OF ONE TO ONE  ###############

arcpy.SpatialJoin_analysis("Test_Geometry", "splice_olt_merged_record", "final_output", join_operation='JOIN_ONE_TO_ONE', join_type ='KEEP_ALL', field_mapping = '', match_option = 'INTERSECT')

arcpy.AddField_management("final_output", "Error", "Text", 50)

if arcpy.Exists("splice_olt_merged_record"):
    arcpy.AddField_management("splice_olt_merged_record", "x_cord", "Text", 50)
    arcpy.AddField_management("splice_olt_merged_record", "y_cord", "Text", 50)
    with arcpy.da.UpdateCursor("splice_olt_merged_record", ["SHAPE@XY", "x_cord", "y_cord"]) as cursor:
        for row in cursor:
            if row[0] != None:
                row[1] = str(row[0][0])
                row[2] = str(row[0][1])
                cursor.updateRow(row)
        del cursor


with arcpy.da.UpdateCursor("final_output", ["Join_Count", "Error"]) as cursor:
     for row in cursor:
         if row[0] != None:
             if int(row[0]) == 0:
                 row[1] = "Fibre sheath not snapped to the OLT/ enclosure"
             cursor.updateRow(row)
     del cursor

arcpy.SpatialJoin_analysis(Fibre_Sheath, "final_output", "final_output_fibre", join_operation='JOIN_ONE_TO_ONE', join_type='KEEP_ALL', field_mapping='', match_option='CONTAINS')

with arcpy.da.UpdateCursor("final_output_fibre", ["Error"]) as cursor:
     for row in cursor:
         if row[0] == None:
             cursor.deleteRow()
     del cursor

array_fibre = arcpy.da.FeatureClassToNumPyArray("final_output_fibre", ["OBJECTID", "Error"], skip_nulls=False)

#arcpy.AddMessage(array_fibre)
arcpy.AddMessage(len(array_fibre))

for data in array_fibre:
    arcpy.AddMessage("Fibre Sheath ID "+str(data[0])+ " not snapped")

for data in array_fibre:
    with arcpy.da.UpdateCursor(p2, ["OBJECTID", "Error"]) as cursor:
        for row in cursor:
            if row[0] != None or row[0] != "":
                if str(row[0]) == str(data[0]):
                    if row[1] != None:
                        row[1] = str(row[1]) +','+ data[1]
                    else:
                        row[1] = data[1]
                cursor.updateRow(row)
        del cursor

with arcpy.da.UpdateCursor(p2, ["Error"]) as cursor:
    for row in cursor:
        if row[0] == None:
            cursor.deleteRow()
    del cursor


if arcpy.Exists("final_output_fibre"):
    arcpy.Delete_management("final_output_fibre")
if arcpy.Exists("final_output"):
    arcpy.Delete_management("final_output")
if arcpy.Exists("splice_olt_merged_record"):
    arcpy.Delete_management("splice_olt_merged_record")
if arcpy.Exists("Test_Geometry"):
    arcpy.Delete_management("Test_Geometry")
if arcpy.Exists(FcName):
    arcpy.Delete_management(FcName)
if arcpy.Exists(p2):
    arcpy.Delete_management(p2)

now = datetime.now()
end_time = now.strftime("%H:%M:%S")
arcpy.AddMessage("\nStart Time = {0}".format(start_time))
arcpy.AddMessage("\nEnd Time = {0}".format(end_time))