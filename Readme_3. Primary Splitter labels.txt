ToolBox Name: NetworkLabling_v1o4.tbx

ToolSet Name: Overall

Script Name: Primary Splitter Labels New_13.10.2021.py

Input: Splice Closure, Fibre Sheath & OLT

Tool test: 1. Primary Splitter labels

Output: Splice_Closure (layer present in the Input geodatabse layer present in the Input geodatabse which is the live layer))

Field(s) to look for: sp1_label (Splice_Closure)

Important Note:
1. Tool takes 30 minutes approximately to run based on the number of features.
2. Tool is dependent on "label" field of the Splice_Closures, in case "label" field is not filled or not updated, "sp1_label" wont be updated correctly.
3. After running the tool remove and add the entire geodatabase again in the TOC to view the changes.
4. Tool checks for Overlapping Splice_Closures and generates message and fails to run further if present.
5. Tool checks for looped Fibre_sheath cable conditions and generates message and fails to run if present.
6. Tool checks if Fibre_Sheath snapped to Splice_Closure and generates message and fails to run if present.
7. In some cases Ribbon field will not be updated correctly for both Fibre_Sheath and Splice_Closures due to geometry issues, those cases need to be inspected and checked for 
	-snapping issue (both Fibre_sheath start and end points with splice_closure)
	-direction issue (fibre_sheath cable is in opposite direction to what it should be).
8. Checks added to show message where Primary Splitter label field for the splice_closure is filled or not.