import arcpy
import os
import arcgis
import sys
from arcgis.gis import GIS


arcpy.env.overwriteOutput = True
arcpy.env.preserveGlobalIds = True



class ParentItem2(object):
    def __init__(self, name):
        ### Object Intialize Here ####
        self.Output_name = "FibreSheath"

    def ItemNo(self):
        arcpy.AddMessage('Tool Update Ribbon on local file..:')



        ################## New Approach To Update Fibre Sheath  ##############
        cable_copy_OLT = arcpy.SelectLayerByLocation_management(fibre_sheath, overlap_type='INTERSECT', select_features=olt)
        arr_fs_OLT = arcpy.da.FeatureClassToNumPyArray(cable_copy_OLT,
                                                       ("temp_ribbon", "GlobalID"),
                                                       skip_nulls=True, explode_to_points=True)
        arcpy.AddMessage(arr_fs_OLT)

        for rows in arr_fs_OLT:
            ribbon = rows[0]
            gid = rows[1]
            array_fbr = []

            #print(first_point)
            #print(last_point)
            array = []
            loop_array = []
            first_point, last_point = self.get_first_last_point(gid)
            arcpy.AddMessage(first_point)
            arcpy.AddMessage(last_point)
            array.append([gid, ribbon])
            # if arcpy.Exists("ribbon_sheath"):
            #     arcpy.Delete_management("ribbon_sheath")
            # arcpy.Select_analysis(fibre_sheath, "ribbon_sheath", "ribbon = '"+ribbon+"'")

            all_objectID, loop_ary = self.find_next_all_line(first_point, last_point, array, [], ribbon, loop_array)
            arcpy.AddMessage(all_objectID)
            arcpy.AddMessage(loop_ary)
            for data in all_objectID:
                with arcpy.da.UpdateCursor(localFibre, ["GlobalID", "temp_ribbon"]) as cursor:
                    for row in cursor:
                        if row[0] == data[0]:
                            arcpy.AddMessage("Fs "+str(row[0])+" Ribbon "+ str(data[1]))
                            row[1] = str(data[1])
                            cursor.updateRow(row)
                    del cursor

            for data1 in loop_array:
                with arcpy.da.UpdateCursor(localFibre, ["GlobalID", "temp_ribbon"]) as cursor1:
                    for row1 in cursor1:
                        if row1[0] == data1[0]:
                            arcpy.AddMessage("Loop Fs " + str(row[0]) + " Ribbon " + str(data1[1]))
                            row1[1] = str(data1[1])
                            cursor1.updateRow(row1)
                    del cursor1

            ############################# Updating Ribbon on Enclosure #########################
            for data in all_objectID:
                f_point, l_point = self.get_first_last_point(data[0])
                enclosure_id = self.find_next_closure(f_point, l_point)
                with arcpy.da.UpdateCursor(localSplice, ["OriginalOID", "temp_ribbon"]) as cursor:
                    for row in cursor:
                        if enclosure_id != "":
                            if int(row[0]) == int(enclosure_id):
                                arcpy.AddMessage("SC " + str(row[0]) + " Ribbon " + str(data[1]))
                                row[1] = str(data[1])
                                cursor.updateRow(row)
                    del cursor


        ############ Update Live Data ############
        #arr_fibre_list = arcpy.da.FeatureClassToNumPyArray(localFibre, ["temp_ribbon", "OBJECTID"], skip_nulls=True)
        arr_fibre_list = arcpy.da.FeatureClassToNumPyArray(localFibre, ["temp_ribbon", "GlobalID"], skip_nulls=True)

        fset1 = fl_fs.query()  # Return a set of features from the layer
        features1 = fset1.features  # Get features object
        if len(features1) == 0:
            print("No features found in '%s' layer", warning=True)
            print('Exiting ...')
            exit()
        else:
            # print("Extracted %s features from '%s' layer" % (len(features)))
            pass

        dictLengths1 = {}
        for item in features1:
            gID = item.attributes["GlobalID"]
            match_str = '{' + gID + '}'
            # arcpy.AddMessage(match_str)
            gID1 = match_str.upper()
            # arcpy.AddMessage(gID1)
            for data in arr_fibre_list:
                if str(gID1) == str(data[1]):
                    arcpy.AddMessage("matched "+ str(gID))
                    dictLengths1[gID] = data[0]

        arcpy.AddMessage(len(dictLengths1))
        # Update feature layer
        if len(list(dictLengths1.keys())) > 0:
            self.ApplyUpdates(fl_fs, features1, ["temp_ribbon"], dictLengths1)
        else:
            arcpy.AddMessage("No records to process")

        # arcpy.AddMessage("Fiber List")
        # arcpy.AddMessage(arr_fibre_list)
        # sts = self.ApplyUpdates_old(arr_fibre_list, fl_fs)
        # if sts == True:
        #     arcpy.AddMessage("Ribbon Updated Successfully on Live Server's Fibre Sheath.")
        #
        # arr_closure_list = arcpy.da.FeatureClassToNumPyArray(localSplice, ["temp_ribbon", "OBJECTID"], skip_nulls=True)
        arr_closure_list = arcpy.da.FeatureClassToNumPyArray(localSplice, ["temp_ribbon", "GlobalID"], skip_nulls=True)
        # arcpy.AddMessage("Splice List")
        # arcpy.AddMessage(arr_closure_list)
        # sts1 = self.ApplyUpdates_old(arr_closure_list, fl_sc)
        # if sts1 == True:
        #     arcpy.AddMessage("Ribbon Updated Successfully on Live Server's Splice Closure .")

        fset = fl_sc.query()  # Return a set of features from the layer
        features = fset.features  # Get features object
        if len(features) == 0:
            print("No features found in '%s' layer", warning=True)
            print('Exiting ...')
            exit()
        else:
            # print("Extracted %s features from '%s' layer" % (len(features)))
            pass

        dictLengths = {}
        for item in features:
            gID = item.attributes["GlobalID"]
            match_str = '{' + gID + '}'
            #arcpy.AddMessage(match_str)
            gID1 = match_str.upper()
            #arcpy.AddMessage(gID1)
            for data in arr_closure_list:
                if str(gID1) == str(data[1]):
                    arcpy.AddMessage("matched string "+ str(gID))
                    dictLengths[gID] = data[0]

        arcpy.AddMessage(len(dictLengths))
        # Update feature layer
        if len(list(dictLengths.keys())) > 0:
            self.ApplyUpdates(fl_sc, features, ["temp_ribbon"], dictLengths)
        else:
            arcpy.AddMessage("No records to process")

    def __del__(self):
        #### Object Remove Here ######
        arcpy.AddMessage("deleting intermediate layers")
        if arcpy.Exists(localSplice):
            arcpy.Delete_management(localSplice)
        if arcpy.Exists(localFibre):
            arcpy.Delete_management(localFibre)

class BaseClass():
    def __init__(self):
        from datetime import datetime

        now = datetime.now()
        start_time = now.strftime("%H:%M:%S")

        arcpy.AddMessage("Script Run Start's @" + start_time)

        if item1 == 'true':
            ParentItem2.__init__(self, '')
            ParentItem2.ItemNo(self)
            ParentItem2.__del__(self)

    def truncate(self, f, n):
        '''Truncates/pads a float f to n decimal places without rounding'''
        s = '{}'.format(f)
        if 'e' in s or 'E' in s:
            return '{0:.{1}f}'.format(f, n)
        i, p, d = s.partition('.')
        return '.'.join([i, (d + '0' * n)[:n]])

    def get_first_last_point(self, globalID):
        f_point = ''
        l_point = ''
        with arcpy.da.SearchCursor(localFibre, ('GlobalID', 'SHAPE@'), "GlobalID = '"+globalID+"'") as cursor:
            for row in cursor:
                # if row[0] == str(globalID):
                #     arcpy.AddMessage(row[0])
                f_point = row[1].firstPoint
                l_point = row[1].lastPoint
            del cursor
        # arcpy.AddMessage(f_point)
        # arcpy.AddMessage(l_point)
        return f_point, l_point

    def find_next_all_line(self, f_point, l_point, array, f_array, ribbon, loop_array):
        with arcpy.da.SearchCursor(localFibre, ('GlobalID', 'SHAPE@')) as cursor:
            for row in cursor:
                fst_point = row[1].firstPoint
                lst_point = row[1].lastPoint
                fst_x_point = self.truncate(fst_point.X, 1)
                fst_y_point = self.truncate(fst_point.Y, 1)
                lst_x_point = self.truncate(l_point.X, 1)
                lst_y_point = self.truncate(l_point.Y, 1)
                if l_point:
                    if lst_x_point == fst_x_point and lst_y_point == fst_y_point:
                        #arcpy.AddMessage(row[0])
                        f_array.append(row[0])
                        array.append([row[0], ribbon])
            del cursor
        for i in f_array:
            first_point, last_point = self.get_first_last_point(i)
            if first_point.X != last_point.X:
                self.find_next_all_line(first_point, last_point, array, [], ribbon, loop_array)
            else:
                #loop_array.append()
                with arcpy.da.SearchCursor(localFibre, ('GlobalID', 'SHAPE@')) as cursor1:
                    for row1 in cursor1:
                        fst_point1 = row1[1].firstPoint
                        lst_point1 = row1[1].lastPoint
                        fst_x_point1 = self.truncate(fst_point1.X, 1)
                        fst_y_point1 = self.truncate(fst_point1.Y, 1)
                        lst_x_point1 = self.truncate(last_point.X, 1)
                        lst_y_point1 = self.truncate(last_point.Y, 1)
                        if lst_x_point1 == fst_x_point1 and lst_y_point1 == fst_y_point1:
                            loop_array.append([row1[0], ribbon])
                    del cursor1
                continue
        return array, loop_array

    def find_next_closure(self, f_point, l_point):
        id = ''
        with arcpy.da.SearchCursor(localSplice, ('OriginalOID', 'SHAPE@XY')) as cursor:
            for row in cursor:
                #print(row[1])
                if l_point:
                    fst_x_point2 = self.truncate(row[1][0], 1)
                    fst_y_point2 = self.truncate(row[1][1], 1)
                    lst_x_point2 = self.truncate(l_point.X, 1)
                    lst_y_point2 = self.truncate(l_point.Y, 1)
                    if lst_x_point2 == fst_x_point2 and lst_y_point2 == fst_y_point2:
                        id = row[0]
        return id
    def ApplyUpdates_old(self, arr_fibre_list, fl):
        status = False
        me = mygis.users.me
        arcpy.AddMessage("Updating temp_ribbon field  to Server.")

        try:
            for rec in arr_fibre_list:
                #arcpy.AddMessage(rec)
                #fset = fl.query(GlobalID = "'"+str(arr_fibre_list[1][1])+"'")
                fset = fl.query(where="OBJECTID=" + str(rec[1]))
                if len(fset.features) != 0:
                    sfo_feature = [f for f in fset][0]
                    #sfo_feature.attributes
                    arcpy.AddMessage(sfo_feature)
                    sfo_edit = sfo_feature
                    sfo_edit.attributes['temp_ribbon'] = str(rec[0])
                    sfo_edit.attributes['Editor'] = me.fullName

                    update = fl.edit_features(updates=[sfo_edit])
                    #arcpy.AddMessage(update)
                    status = update["updateResults"][0]["success"]
        except Exception as ex:
            arcpy.AddMessage(ex)

        return status

    # Function to apply updates
    def ApplyUpdates(self, fl, ft, flds, dict):
        features_to_be_updated = []  # Create empty list for features to be updated
        # Create list of all features that match globalid in gID list
        cntTotalUpdates = 0  # Reset total rumber of updates
        cntPacket = 0  # Reset data packet number
        cntFeatures = 0  # Count total features searched
        while cntTotalUpdates < len(list(dict.keys())):
            cntUpdates = 0  # Set updates for new data packet to zero
            features_to_be_updated = []  # Create empty list for features to be updated
            # for edit_item in features: # Loop items in the features object
            while cntUpdates <= 2000 and cntFeatures < len(ft):  # Loop through 2000 records at a time
                edit_item = ft[cntFeatures]  # Set the item to edit
                gID = edit_item.attributes['GlobalID']  # Set the gloabl id
                if gID in list(dict.keys()):  # If feature gid is in the list of Gids
                    toEdit = 0  # Reset flag to inditcate if record is to be updated/edited
                    # if there is only one field to update...
                    if len(flds) == 1:
                        item = flds[0]
                        valExist = edit_item.attributes[item]  # Exising value
                        valProp = dict[gID]  # Proposed value
                        if str(valExist) != str(valProp):  # Check if the existing value is the same as the proposed
                            edit_item.attributes[
                                item] = valProp  # If not then update the existing value to the proposed
                            toEdit = 1
                    else:  # If there is more than one field to update
                        cntLstFlds = 0  # Set list item count to 0
                        for item in flds:  # loop all items in list of fields
                            valExist = edit_item.attributes[item]  # Exising value
                            valProp = dict[gID][cntLstFlds]  # Proposed value
                            if str(valExist) != str(valProp):  # Check if the existing value is the same as the proposed
                                edit_item.attributes[
                                    item] = valProp  # If not then update the existing value to the proposed
                                toEdit = 1
                            cntLstFlds = cntLstFlds + 1  # increwment the list of fields index
                    if toEdit == 1:
                        features_to_be_updated.append(edit_item)  # Add this edit to list of edits
                        cntUpdates = cntUpdates + 1  # increment the updates by packet counter
                        cntTotalUpdates = cntTotalUpdates + 1  # increment the total updates counter

                cntFeatures = cntFeatures + 1  # Increment the feature count

            if cntFeatures == len(
                    ft) and cntUpdates == 0:  # if all records have been checked and there are no records to update then break out of while loop
                break
            cntPacket = cntPacket + 1  # increment the count of number of data packets
            # update the feature with the relevant edits
            cnt = 1  # Set the update attempt counter to 1
            while True and len(
                    features_to_be_updated) > 0:  # While the features to be updated list has values perform the update
                arcpy.AddMessage(
                    "Updating Feature Layer: data packet " + str(cntPacket) + ", attempt " + str(cnt) + "...")
                try:
                    update_result = fl.edit_features(updates=features_to_be_updated)  # Apply updates to feature layer
                except Exception as e:  # Exception if there is an error updating
                    arcpy.AddMessage(str(e))
                    cnt = cnt + 1
                else:
                    break
                if cnt == 20:
                    print("Exiting after %s failed attempts" % cnt, ERROR=True)
                    exit()

        arcpy.AddMessage("Total features updated: " + str(cntTotalUpdates))

    def __del__(self):
        # Object Remove Here ######

       from datetime import datetime
       now = datetime.now()
       end_time = now.strftime("%H:%M:%S")

       arcpy.AddMessage("Script Run End's @" + end_time)


def TruncateWebLayer(gis=None, target=None):
    try:
        lyr = arcgis.features.FeatureLayer(target, gis)
        arcpy.AddMessage(lyr)
        #lyr.manager.truncate()
        arcpy.AddMessage("Successfully truncated layer: " + str(target))
    except:
        arcpy.AddMessage("Failed truncating: exception")
        arcpy.AddMessage("Failed truncating: " + str(target))
        sys.exit()
######### Get All Input Parameter ###########

arcpy.env.overwriteOutput = True


splice_closure = arcpy.GetParameterAsText(0)
fibre_sheath = arcpy.GetParameterAsText(1)
olt = arcpy.GetParameterAsText(2)
####### Checked Input #########


item1 = arcpy.GetParameterAsText(3)
#item1 = 'true'


# arcpy.env.workspace = r"C:\Users\user\Documents\ArcGIS\Projects\MyProject2\MyProject2.gdb"
# splice_closure = "Splice_Closure"
# fibre_sheath = "Fibre_Sheath"
# olt = "OLT"


array_fbr = []

aprx = arcpy.mp.ArcGISProject("CURRENT")

# Get pole item from AGOL
desc = arcpy.Describe(fibre_sheath)
desc_sc = arcpy.Describe(splice_closure)
arcpy.AddMessage(desc)
arcpy.AddMessage(desc_sc.catalogPath.split('/'))

#path = desc_sc.catalogPath.split('/')

#arcpy.AddMessage(path[-1])
# to_array = [char for char in desc.name]
# to_array1 = [char for char in desc_sc.name]
# arcpy.AddMessage(to_array1)
# sys.exit()
fsPath = desc.catalogPath # Get url to pole feature service
scPath = desc_sc.catalogPath # Get url to pole feature service

arcpy.AddMessage("fspath")
arcpy.AddMessage(fsPath)
arcpy.AddMessage("scpath")
arcpy.AddMessage(scPath)

gdbLocal = aprx.defaultGeodatabase
arcpy.AddMessage(gdbLocal)
gdbPath = aprx.homeFolder
gdbName = os.path.basename(gdbPath)
if not arcpy.Exists(gdbLocal):
    arcpy.AddMessage("test if")
    arcpy.CreateFileGDB_management(gdbPath, gdbName)


def getOverlapingCoord(coordinate, array):
    geom = []
    for rec in array:
        geom.append(rec[1][0])
    count = geom.count(coordinate[0])
    if count >= 2:
        status = True
    else:
        status = False
    return status

######### Oject Creation #######
# main function
if __name__ == '__main__':
    # created the object for BaseClass
    #main()
    #Create local features and set these to variables
    mygis = arcgis.gis.GIS("Pro")

    # the URL of a single feature layer within a collection in an AGOL portal
    publishedWebLayer = fsPath
    publishedWebLayer1 = scPath

    # a feature class on the local system with the same schema as the portal layer
    updateFeatures = gdbPath

    TruncateWebLayer(mygis, publishedWebLayer)
    TruncateWebLayer(mygis, publishedWebLayer1)

    # reference the empty layer as FeatureLayer object from the ArcGIS Python API
    fl_fs = arcgis.features.FeatureLayer(publishedWebLayer, mygis)

    fl_sc = arcgis.features.FeatureLayer(publishedWebLayer1, mygis)

    arcpy.AddMessage("Creating local copies of the parameters...")

    # Export only chambers connecting to Sheaths or Drops
    arcpy.env.preserveGlobalIds = True  # Set the preserveGlobalIds environment to True
    ScName = "Splice_Closure"
    FcName = "Fibre_Sheath"
    OLT = "OLT"

    p1 = aprx.defaultGeodatabase

    p2 = p1 + "\\" + ScName
    p3 = p1 + "\\" + FcName

    #Local Copy of Splice Closure
    localSplice = os.path.join(gdbLocal, ScName)  # set the local feature class path
    arcpy.AddMessage(localSplice)
    if arcpy.Exists(localSplice):
        arcpy.Delete_management(localSplice)

    #arcpy.conversion.FeatureClassToFeatureClass(splice_closure, gdbLocal, ScName, '', r'label "Label" true true false 255 Text 0 0,First,#,Adare_Test_Layers\Splice Closures,label,0,255;enc_type "Enclosure Type" true true false 0 Long 0 0,First,#,Adare_Test_Layers\Splice Closures,enc_type,-1,-1;enc_size "Enclosure Size" true true false 0 Long 0 0,First,#,Adare_Test_Layers\Splice Closures,enc_size,-1,-1;stru_label "Structure Label" true true false 255 Text 0 0,First,#,Adare_Test_Layers\Splice Closures,stru_label,0,255;placement "Placement" true true false 0 Long 0 0,First,#,Adare_Test_Layers\Splice Closures,placement,-1,-1;sp1_type "Sp1 Type" true true false 0 Long 0 0,First,#,Adare_Test_Layers\Splice Closures,sp1_type,-1,-1;sp1_label "Sp1 Label" true true false 255 Text 0 0,First,#,Adare_Test_Layers\Splice Closures,sp1_label,0,255;sp2_type "Sp2 Type" true true false 0 Long 0 0,First,#,Adare_Test_Layers\Splice Closures,sp2_type,-1,-1;sp2_label "Sp2 Label" true true false 255 Text 0 0,First,#,Adare_Test_Layers\Splice Closures,sp2_label,0,255;status "Status" true true false 0 Long 0 0,First,#,Adare_Test_Layers\Splice Closures,status,-1,-1;reel_end "Reel End" true true false 0 Long 0 0,First,#,Adare_Test_Layers\Splice Closures,reel_end,-1,-1;comments "Comments" true true false 255 Text 0 0,First,#,Adare_Test_Layers\Splice Closures,comments,0,255;GlobalID "GlobalID" false false true 38 GlobalID 0 0,First,#,Adare_Test_Layers\Splice Closures,GlobalID,-1,-1;CreationDate "CreationDate" false true false 8 Date 0 0,First,#,Adare_Test_Layers\Splice Closures,CreationDate,-1,-1;Creator "Creator" false true false 128 Text 0 0,First,#,Adare_Test_Layers\Splice Closures,Creator,0,128;EditDate "EditDate" false true false 8 Date 0 0,First,#,Adare_Test_Layers\Splice Closures,EditDate,-1,-1;Editor "Editor" false true false 128 Text 0 0,First,#,Adare_Test_Layers\Splice Closures,Editor,0,128;temp_drops "Number of Drops" true true false 0 Long 0 0,First,#,Adare_Test_Layers\Splice Closures,temp_drops,-1,-1;temp_maxcable "Max Cable Size" true true false 0 Long 0 0,First,#,Adare_Test_Layers\Splice Closures,temp_maxcable,-1,-1;temp_ribbon "Ribbon" true true false 256 Text 0 0,First,#,Adare_Test_Layers\Splice Closures,temp_ribbon,0,256;OBJ1 "OBJ1" true true false 255 Text 0 0,First,#,Adare_Test_Layers\Splice Closures,OBJECTID,-1,-1', '')
    #arcpy.FeatureClassToFeatureClass_conversion(splice_closure, gdbLocal,ScName)  # Export the layer to the local geodatabase
    feats = arcpy.FeatureSet(table=splice_closure)
    feats.save(p2)
    arcpy.SelectLayerByAttribute_management(splice_closure,"CLEAR_SELECTION")  # Clear any current selection from chambers

    try:
        lstFields_sc = arcpy.ListFields(localSplice)
        field_names_sc = [f.name for f in lstFields_sc]
        if "OriginalOID" not in field_names_sc:
            arcpy.AddField_management(localSplice, "OriginalOID", "LONG", 10)
            arcpy.CalculateField_management(localSplice, "OriginalOID", "!OBJECTID!", "PYTHON3")
        else:
            pass
    except Exception as ex:
        arcpy.AddMessage(ex)

    # Local copy of Fibre Sheath
    localFibre = os.path.join(gdbLocal, FcName)  # set the local feature class path
    if arcpy.Exists(localFibre):
        arcpy.Delete_management(localFibre)
    #arcpy.FeatureClassToFeatureClass_conversion(fibre_sheath, gdbLocal,FcName)  # Export the layer to the local geodatabase
    #arcpy.conversion.FeatureClassToFeatureClass(fibre_sheath,gdbLocal,FcName, '',r'label "Label" true true false 255 Text 0 0,First,#,Adare_Test_Layers\Fibre Sheath,OBJECTID,-1,-1;enc_type "Enclosure Type" true true false 0 Long 0 0,First,#,Adare_Test_Layers\Splice Closures,enc_type,-1,-1;enc_size "Enclosure Size" true true false 0 Long 0 0,First,#,Adare_Test_Layers\Splice Closures,enc_size,-1,-1;stru_label "Structure Label" true true false 255 Text 0 0,First,#,Adare_Test_Layers\Splice Closures,stru_label,0,255;placement "Placement" true true false 0 Long 0 0,First,#,Adare_Test_Layers\Splice Closures,placement,-1,-1;sp1_type "Sp1 Type" true true false 0 Long 0 0,First,#,Adare_Test_Layers\Splice Closures,sp1_type,-1,-1;sp1_label "Sp1 Label" true true false 255 Text 0 0,First,#,Adare_Test_Layers\Splice Closures,sp1_label,0,255;sp2_type "Sp2 Type" true true false 0 Long 0 0,First,#,Adare_Test_Layers\Splice Closures,sp2_type,-1,-1;sp2_label "Sp2 Label" true true false 255 Text 0 0,First,#,Adare_Test_Layers\Splice Closures,sp2_label,0,255;status "Status" true true false 0 Long 0 0,First,#,Adare_Test_Layers\Splice Closures,status,-1,-1;reel_end "Reel End" true true false 0 Long 0 0,First,#,Adare_Test_Layers\Splice Closures,reel_end,-1,-1;comments "Comments" true true false 255 Text 0 0,First,#,Adare_Test_Layers\Splice Closures,comments,0,255;GlobalID "GlobalID" false false true 38 GlobalID 0 0,First,#,Adare_Test_Layers\Splice Closures,GlobalID,-1,-1;CreationDate "CreationDate" false true false 8 Date 0 0,First,#,Adare_Test_Layers\Splice Closures,CreationDate,-1,-1;Creator "Creator" false true false 128 Text 0 0,First,#,Adare_Test_Layers\Splice Closures,Creator,0,128;EditDate "EditDate" false true false 8 Date 0 0,First,#,Adare_Test_Layers\Splice Closures,EditDate,-1,-1;Editor "Editor" false true false 128 Text 0 0,First,#,Adare_Test_Layers\Splice Closures,Editor,0,128;temp_drops "Number of Drops" true true false 0 Long 0 0,First,#,Adare_Test_Layers\Splice Closures,temp_drops,-1,-1;temp_maxcable "Max Cable Size" true true false 0 Long 0 0,First,#,Adare_Test_Layers\Splice Closures,temp_maxcable,-1,-1;temp_ribbon "Ribbon" true true false 256 Text 0 0,First,#,Adare_Test_Layers\Splice Closures,temp_ribbon,0,256;OBJ1 "OBJ1" true true false 255 Text 0 0,First,#,Adare_Test_Layers\Fibre Sheath,OBJECTID,-1,-1','')

    feats1 = arcpy.FeatureSet(table=fibre_sheath)
    feats1.save(p3)
    arcpy.SelectLayerByAttribute_management(fibre_sheath,"CLEAR_SELECTION")  # Clear any current selection from chambers

    try:
        lstFields_fs = arcpy.ListFields(localFibre)
        field_names_fs = [f.name for f in lstFields_fs]
        if "OriginalFID_fb" not in field_names_fs:
            arcpy.AddField_management(localFibre, "OriginalFID_fb", "LONG", 10)
            arcpy.CalculateField_management(localFibre, "OriginalFID_fb", "!OBJECTID!", "PYTHON3")
        else:
            pass
    except Exception as ex:
        arcpy.AddMessage(ex)

    # Local copy of OLT
    # localOLT = os.path.join(gdbLocal, olt)  # set the local feature class path
    # if arcpy.Exists(localOLT):
    #     arcpy.Delete_management(localOLT)
    # arcpy.FeatureClassToFeatureClass_conversion(olt, gdbLocal,OLT)  # Export the layer to the local geodatabase
    # arcpy.SelectLayerByAttribute_management(olt,"CLEAR_SELECTION")  # Clear any current selection from chambers



    Status_check = False
    messages = ''

    ############# Checks 1. There is data in the ribbon field ###################
    cable_copy_OLT = arcpy.SelectLayerByLocation_management(fibre_sheath, overlap_type='INTERSECT', select_features=olt)
    arr_fs_OLT = arcpy.da.FeatureClassToNumPyArray(cable_copy_OLT, ("from_", "temp_ribbon", "OBJECTID"), skip_nulls=False, explode_to_points=True)

    #arcpy.AddMessage(arr_fs_OLT)
    array_ribbon = []
    for data in arr_fs_OLT:
        if str(data[1]) == "None" or str(data[1]) == "":
            Status_check = True
            array_ribbon.append(data[2])
    #arcpy.AddMessage(array_ribbon)

    ############# Checks 2. No overlapping splice_closures present in the dataset ###################

    array_splice = arcpy.da.FeatureClassToNumPyArray(localSplice, ("OriginalOID", "SHAPE@XY"))

    array_overlaping_splice = []
    with arcpy.da.SearchCursor(localSplice, ("OriginalOID", "SHAPE@XY")) as cursor:
        for row in cursor:
            data = getOverlapingCoord(row[1], array_splice)
            if data:
                Status_check = True
                array_overlaping_splice.append(row[0])
        del cursor

    #arcpy.AddMessage(array_overlaping_splice)
    if Status_check == False:
        ob = BaseClass()
    else:
        if len(array_overlaping_splice) != 0:
            for i in array_overlaping_splice:
                arcpy.AddMessage("overlapping splice_closures id "+str(i)+" present in the dataset")
        if len(array_ribbon) != 0:
            for i in array_ribbon:
                arcpy.AddMessage("Fill 'temp_ribbon' filled with Ribbon using OBJECTID of "+str(i))
        if arcpy.Exists(localSplice):
            arcpy.Delete_management(localSplice)
        if arcpy.Exists(localFibre):
            arcpy.Delete_management(localFibre)
