ToolBox Name: NetworkLabling_v1o4.tbx

ToolSet Name: Overall

Script Name: 1. Ribbon field in Cable Sheath_13.10.2021.py

Input: Splice Closure, Fibre Sheath, OLT

Tool test: 1. Feeding Ribbon Label

Output: Fibre Sheath (layer present in the Input geodatabse which is the live layer) & Splice_Closure (ayer present in the Input geodatabse which is the live layer)

Field(s) to look for: temp_ribbon (Fibre_Sheath) and temp_ribbon (Splice_Closure)

Important Note:
1. Tool takes 1 hour 30 minutes approximately to run based on the number of features.
2. After running the tool remove and add the entire geodatabase again in the TOC to view the changes.
3. Before running tool, make sure that the Fibre_Sheath feeding to respective Boundaries is connected directly to OLT and not from another Boundary(ribbon).
4. Tool checks and warns about Overlapping Splice_closures in the messages; if not it fails automatically.
5. Tool checks whether (From_) "OLT" field in Fibre_Sheath must have respective values in their "temp_ribbon" field. (e.g- If From_ "OLT" temp_ribbon "A")
6. In some cases Ribbon field will not be updated for both Fibre_Sheath and Splice_Closures due to geometry issues, those cases need to be inspected and checked for 
	-snapping issue (both Fibre_sheath start and end points with splice_closure)
	-direction issue (fibre_sheath cable is in opposite direction to what it should be).
7. In case a geometry issue is encountered; all the Fibre_sheath and Splice_Closures "temp_ribbon" downstream will not get filled.