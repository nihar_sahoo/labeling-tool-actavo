import arcpy
import math
import sys
from datetime import datetime
from operator import sub
import os
import arcgis
from arcgis.gis import GIS
from arcgis.geometry import Geometry
from arcgis.geometry import *

now = datetime.now()
start_time = now.strftime("%H:%M:%S")

arcpy.env.preserveGlobalIds = True  # Set the preserveGlobalIds environment to True

# arcpy.env.workspace = r"C:\Users\user\Documents\ArcGIS\Projects\Test\Test.gdb"
# Splice_Closures = "SpliceClosure"
# Fibre_Sheath = "FibreSheath"
# Boundaries = "Boundaries"
# FibreDrop = "Fibre_Drop"


Splice_Closures = arcpy.GetParameterAsText(0)
Fibre_Sheath = arcpy.GetParameterAsText(1)
Boundaries = arcpy.GetParameterAsText(2)
FibreDrop = arcpy.GetParameterAsText(3)
check_label = arcpy.GetParameterAsText(4)
#

def remove_dup_by_first_element(list1):
    first_element_list = []
    list2 = []
    for x in list1:
        if x[0] not in first_element_list:
            list2.append(x)
            first_element_list.append(x[0])
    return list2

def TruncateWebLayer(gis=None, target=None):
    try:
        lyr = arcgis.features.FeatureLayer(target, gis)
        arcpy.AddMessage(lyr)
        #lyr.manager.truncate()
        arcpy.AddMessage("Successfully truncated layer: " + str(target))
    except:
        arcpy.AddMessage("Failed truncating: exception")
        arcpy.AddMessage("Failed truncating: " + str(target))
        sys.exit()

# def ApplyUpdates(arr_list, fls, field):
#     status = False
#     me = mygis.users.me
#     arcpy.AddMessage("Updating drop label field to Server.")
#     try:
#         for rec in arr_list:
#             fset = fls.query(where="OBJECTID=" + str(rec[0]))
#             if len(fset.features) != 0:
#                 sfo_feature = [f for f in fset][0]
#                 sfo_feature.attributes
#
#                 sfo_edit = sfo_feature
#                 sfo_edit.attributes[field[0]] = rec[1]
#                 sfo_edit.attributes['Editor'] = me.fullName
#
#                 update = fls.edit_features(updates=[sfo_edit])
#                 arcpy.AddMessage(update)
#                 status = update["updateResults"][0]["success"]
#     except Exception as ex:
#         arcpy.AddMessage(ex)
#
#     return status

#Function to apply updates
def ApplyUpdates(fl, ft, flds, dict):
    features_to_be_updated = []  # Create empty list for features to be updated
    # Create list of all features that match globalid in gID list
    cntTotalUpdates = 0  # Reset total rumber of updates
    cntPacket = 0  # Reset data packet number
    cntFeatures = 0  # Count total features searched
    while cntTotalUpdates < len(list(dict.keys())):
        cntUpdates = 0  # Set updates for new data packet to zero
        features_to_be_updated = []  # Create empty list for features to be updated
        # for edit_item in features: # Loop items in the features object
        while cntUpdates <= 2000 and cntFeatures < len(ft):  # Loop through 2000 records at a time
            edit_item = ft[cntFeatures]  # Set the item to edit
            gID = edit_item.attributes['GlobalID']  # Set the gloabl id
            if gID in list(dict.keys()):  # If feature gid is in the list of Gids
                toEdit = 0  # Reset flag to inditcate if record is to be updated/edited
                # if there is only one field to update...
                if len(flds) == 1:
                    item = flds[0]
                    valExist = edit_item.attributes[item]  # Exising value
                    valProp = dict[gID]  # Proposed value
                    if str(valExist) != str(valProp):  # Check if the existing value is the same as the proposed
                        edit_item.attributes[item] = valProp  # If not then update the existing value to the proposed
                        toEdit = 1
                else:  # If there is more than one field to update
                    cntLstFlds = 0  # Set list item count to 0
                    for item in flds:  # loop all items in list of fields
                        valExist = edit_item.attributes[item]  # Exising value
                        valProp = dict[gID][cntLstFlds]  # Proposed value
                        if str(valExist) != str(valProp):  # Check if the existing value is the same as the proposed
                            edit_item.attributes[
                                item] = valProp  # If not then update the existing value to the proposed
                            toEdit = 1
                        cntLstFlds = cntLstFlds + 1  # increwment the list of fields index
                if toEdit == 1:
                    features_to_be_updated.append(edit_item)  # Add this edit to list of edits
                    cntUpdates = cntUpdates + 1  # increment the updates by packet counter
                    cntTotalUpdates = cntTotalUpdates + 1  # increment the total updates counter

            cntFeatures = cntFeatures + 1  # Increment the feature count

        if cntFeatures == len(
                ft) and cntUpdates == 0:  # if all records have been checked and there are no records to update then break out of while loop
            break
        cntPacket = cntPacket + 1  # increment the count of number of data packets
        # update the feature with the relevant edits
        cnt = 1  # Set the update attempt counter to 1
        while True and len(
                features_to_be_updated) > 0:  # While the features to be updated list has values perform the update
            arcpy.AddMessage("Updating Feature Layer: data packet " + str(cntPacket) + ", attempt " + str(cnt) + "...")
            try:
                update_result = fl.edit_features(updates=features_to_be_updated)  # Apply updates to feature layer
            except Exception as e:  # Exception if there is an error updating
                arcpy.AddMessage(str(e))
                cnt = cnt + 1
            else:
                break
            if cnt == 20:
                print("Exiting after %s failed attempts" % cnt, ERROR=True)
                exit()

    arcpy.AddMessage("Total features updated: " + str(cntTotalUpdates))

def get_geom(array, id):
    geom = ''
    if len(array) > 0:
        for i in array:
            if int(i[1]) == id:
                geom = i[0]
    return geom

aprx = arcpy.mp.ArcGISProject("CURRENT")

# Get pole item from AGOL
desc = arcpy.Describe(FibreDrop)

DPath = desc.catalogPath # Get url to pole feature service

# arcpy.AddMessage("Dpath")
# arcpy.AddMessage(DPath)

gdbLocal = aprx.defaultGeodatabase
arcpy.AddMessage(gdbLocal)
gdbPath = aprx.homeFolder
gdbName = os.path.basename(gdbPath)
if not arcpy.Exists(gdbLocal):
    arcpy.CreateFileGDB_management(gdbPath, gdbName)

# Create local features and set these to variables
mygis = arcgis.gis.GIS("Pro")

# the URL of a single feature layer within a collection in an AGOL portal
publishedWebLayer = DPath


TruncateWebLayer(mygis, publishedWebLayer)

# reference the empty layer as FeatureLayer object from the ArcGIS Python API
fl = arcgis.features.FeatureLayer(publishedWebLayer, mygis)

arcpy.AddMessage("Creating local copies of the parameters...")

# Export only chambers connecting to Sheaths or Drops
arcpy.env.preserveGlobalIds = True  # Set the preserveGlobalIds environment to True


Boundary = "Boundaries"
ScName = "SpliceClosure"
FsName = "FibreSheath"
FdName = "FibreDrop"

localDrop = "FibreDrop"

localBoundary = Boundary

localSplice = ScName

p = aprx.defaultGeodatabase

localSplice = p + "\\" + ScName
localBoundary = p + "\\" + Boundary
localDrop = p + "\\" + FdName

#Local Copy of Splice Closure
#localSplice = os.path.join(gdbLocal, ScName)  # set the local feature class path
feat_sc = arcpy.FeatureSet(table=Splice_Closures)
feat_sc.save(localSplice)
if arcpy.Exists(localSplice):
    arcpy.env.workspace = gdbLocal
    arcpy.env.overwriteOutput = True
    arcpy.SelectLayerByAttribute_management(ScName,"CLEAR_SELECTION")  # Clear any current selection from chambers
else:
    arcpy.AddMessage("Please Run The Splice Labeling Tool 1st")
    sys.exit()

#Local Copy of Boundary Checks
#localBoundary = os.path.join(gdbLocal, Boundary)  # set the local feature class path
feat_b = arcpy.FeatureSet(table=Boundaries)
feat_b.save(localBoundary)
if arcpy.Exists(localBoundary):
    arcpy.env.workspace = gdbLocal
    arcpy.env.overwriteOutput = True
    arcpy.SelectLayerByAttribute_management(localBoundary,"CLEAR_SELECTION")  # Clear any current selection from chambers
else:
    arcpy.AddMessage("Please Run The Ribbon Labeling")
    sys.exit()

#localDrop = os.path.join (gdbLocal, FdName)  # set the local feature class path
feat_d = arcpy.FeatureSet(table=FibreDrop)
feat_d.save(localDrop)
if arcpy.Exists(localDrop):
    arcpy.env.workspace = gdbLocal
    arcpy.env.overwriteOutput = True
    arcpy.SelectLayerByAttribute_management(FibreDrop,"CLEAR_SELECTION")  # Clear any current selection from chambers
else:
    #arcpy.FeatureClassToFeatureClass_conversion(FibreDrop, gdbLocal,FdName)  # Export the layer to the local geodatabase
    feat_d = arcpy.FeatureSet(table=FibreDrop)
    feat_d.save(localDrop)
    arcpy.SelectLayerByAttribute_management(FibreDrop, "CLEAR_SELECTION")  # Clear any current selection from Boundary

lstFields_drop = arcpy.ListFields(localDrop)
field_names_drop = [f.name for f in lstFields_drop]
if "OriginalID_D" not in field_names_drop:
    arcpy.AddField_management(localDrop, "OriginalID_D", "LONG", 10)
if "drop_label" not in field_names_drop:
    print("drop label")
    arcpy.AddField_management(localDrop, "drop_label", "Text", 100)
if "drop_length" not in field_names_drop:
    print("drop length")
    arcpy.AddField_management(localDrop, "drop_length", "LONG", 100)

try:
    lstFields_sc = arcpy.ListFields(ScName)
    field_names_sc = [f.name for f in lstFields_sc]
    if "OriginalOID" not in field_names_sc:
        arcpy.AddField_management(ScName, "OriginalOID", "LONG", 10)
        arcpy.CalculateField_management(ScName, "OriginalOID", "!OBJECTID!", "PYTHON3")
    else:
        pass
except Exception as ex:
    arcpy.AddMessage(ex)

spatial_ref = arcpy.Describe(localSplice).spatialReference
# print("{0} : {1}".format(localSplice, spatial_ref.name))
# print(spatial_ref.factoryCode)
#
#
arcpy.CalculateField_management(localDrop, "OriginalID_D", "!OBJECTID!", "PYTHON3")
#
geometries = arcpy.CopyFeatures_management(localDrop, arcpy.Geometry())

# Walk through each geometry, totaling the length
#
length = sum([g.length for g in geometries])

arcpy.CalculateField_management(localDrop, "drop_length", "!shape.length@METERS!", "PYTHON3")

arcpy.Select_analysis(localBoundary, "secondary_boundary", '"level_" in (2, 3)')

array_sc = arcpy.da.FeatureClassToNumPyArray(localSplice, ("SHAPE@XY", "OriginalOID"), skip_nulls=True)

arcpy.Select_analysis(localSplice, "secondary_splitter_2", '"enc_type" in (2)')
arcpy.Select_analysis(localSplice, "secondary_splitter_3", '"enc_type" in (3)')
arcpy.Select_analysis(localSplice, "secondary_splitter_5", '"enc_type" in (5)')
arcpy.Select_analysis(localSplice, "primary_splitter", '"enc_type" in (1)')

################## Make Spatial Join with Primary Splitter #####################

arcpy.SpatialJoin_analysis("secondary_boundary", "secondary_splitter_2", "boundary_splitter2_join", join_operation="JOIN_ONE_TO_ONE", join_type="KEEP_ALL", match_option="INTERSECT")
arcpy.SpatialJoin_analysis("boundary_splitter2_join", "secondary_splitter_3", "boundary_splitter23_join", join_operation="JOIN_ONE_TO_ONE", join_type="KEEP_ALL", match_option="INTERSECT")
arcpy.SpatialJoin_analysis("boundary_splitter23_join", "secondary_splitter_5", "boundary_splitter235_join", join_operation="JOIN_ONE_TO_ONE", join_type="KEEP_ALL", match_option="INTERSECT")
arcpy.SpatialJoin_analysis("boundary_splitter235_join", "primary_splitter", "boundary_splitter2351_join", join_operation="JOIN_ONE_TO_ONE", join_type="KEEP_ALL", match_option="INTERSECT")

array_ribbon_check = []

array_records = arcpy.da.FeatureClassToNumPyArray("boundary_splitter235_join", ('name', 'Join_Count', 'Join_Count_1', 'Join_Count_12', 'sp1_type', 'sp2_type', 'sp1_label', 'sp2_label', 'OriginalOID', 'OriginalOID_1'), skip_nulls=True)
array_boundary = []

for i in array_records:
    array_boundary.append(i[0])
    array_ribbon_check.append({i[0]:[i[1], i[3], i[2], i[4], i[5], i[6], i[7], i[8], i[9]]})


array_ribbon_check1 = []

array_records1 = arcpy.da.FeatureClassToNumPyArray("boundary_splitter235_join", ('name', 'Join_Count', 'Join_Count_1', 'Join_Count_12','sp2_type_12','sp2_label_12', 'OriginalOID_12', 'OriginalOID_1'), skip_nulls=True)
array_boundary1 = []

for i in array_records1:
    array_boundary1.append(i[0])
    array_ribbon_check1.append({i[0]:[i[1], i[2], i[3], i[4], i[5], i[6], i[7]]})

####### for Null Case###################################
array_ribbon_check_N = []

array_records_N = arcpy.da.FeatureClassToNumPyArray("boundary_splitter235_join", ('name', 'Join_Count', 'Join_Count_1', 'Join_Count_12', 'sp1_type', 'sp2_type', 'sp1_label', 'sp2_label'), skip_nulls=True)
array_boundary_N = []

for i in array_records_N:
    array_boundary_N.append(i[0])
    array_ribbon_check_N.append({i[0]:[i[1], i[3], i[2], i[4], i[5], i[6], i[7]]})

array_ribbon_check1_N = []

array_records1_N = arcpy.da.FeatureClassToNumPyArray("boundary_splitter235_join", ('name', 'Join_Count', 'Join_Count_1', 'Join_Count_12','sp2_type_12','sp2_label_12'), skip_nulls=True)
array_boundary1_N = []

for i in array_records1_N:
    array_boundary1_N.append(i[0])
    array_ribbon_check1_N.append({i[0]:[i[1], i[2], i[3], i[4], i[5]]})

##########END#########################################
#############case1,case2 Checked##############################
array_1odp2_1sp = []
array_1odp2_2sp = []
array_odp2_odp3 = []
for i in range(0, len(array_ribbon_check_N)):
    sec_boundary_N = array_boundary_N[i]
    arcpy.AddMessage("secondary boundary "+str(sec_boundary_N))
    data_N = array_ribbon_check_N[i][array_boundary_N[i]]
    arcpy.AddMessage(data_N)
    # 1. One ODP2, No ODP5, One splitter, No ODP3
    if data_N[0] == 1 and data_N[2] == 0 and data_N[1] == 0 and data_N[3] != 5 and data_N[4] == 5:
        arcpy.AddMessage("case 1")
        arcpy.AddMessage(sec_boundary_N)
        array_1odp2_1sp.append([sec_boundary_N, data_N[3], data_N[5]])
    # 2. One ODP2, No ODP5, Two splitter, No ODP3
    elif data_N[0] == 1 and data_N[2] == 0 and data_N[1] == 0 and data_N[3] != 5 and data_N[4] != 5:
        arcpy.AddMessage("case 2")
        arcpy.AddMessage(sec_boundary_N)
        array_1odp2_2sp.append([sec_boundary_N, data_N[3], data_N[4], data_N[5], data_N[6]])
    elif data_N[0] == 1 and data_N[2] == 0 and data_N[1] == 0 and data_N[3] == 5 and data_N[4] == 5:
        arcpy.AddMessage("case Null -SP1_Label and SP2_Label")

    else:
        arcpy.AddMessage("Some Error Occoured on "+ str(sec_boundary_N)+ " the secondary boundary.")


for i in range(0, len(array_ribbon_check1_N)):
    sec_boundary1_N = array_boundary1_N[i]
    arcpy.AddMessage("secondary boundary "+str(sec_boundary1_N))
    data1_N = array_ribbon_check1_N[i][array_boundary1_N[i]]
    print(data1_N)
    # 7. No ODP2, One ODP5, No ODP3
    if data1_N[0] == 0 and data1_N[2] == 0 and data1_N[1] == 1 and data1_N[3] != 5:
        array_odp2_odp3.append([sec_boundary1_N, data1_N[3], data1_N[4]])
    else:
        arcpy.AddMessage("Some Error Occoured on "+ str(sec_boundary1_N)+ " the secondary boundary.")
###########################################################
####### Join_Count/ODP2, Join_Count_1/ODP1, Join_Count_12/ODP5, Join_Count_12_13/ODP3 , OriginalOID_12_13/ODP 5 - ObjectID, OriginalOID_1/ODP 3 - ObjectID, OriginalOID_12_13/ODP 1 - ObjectID

array_records2 = arcpy.da.FeatureClassToNumPyArray("boundary_splitter2351_join", ('name', 'Join_Count', 'Join_Count_1', 'Join_Count_12', 'Join_Count_12_13','sp1_type_12_13','sp2_label_12_13', 'OriginalOID_12_13', 'OriginalOID_1', 'sp2_type_12_13'), skip_nulls=True)
array_boundary2 = []
arcpy.AddMessage("11/12")
arcpy.AddMessage(array_records2)
array_ribbon_check2 = []
for i in array_records2:
    array_boundary2.append(i[0])
    array_ribbon_check2.append({i[0]:[i[1], i[2], i[3], i[4], i[5], i[6], i[7], i[8], i[9]]})

array_records3 = arcpy.da.FeatureClassToNumPyArray("boundary_splitter2351_join", ('name', 'Join_Count', 'Join_Count_1', 'Join_Count_12', 'Join_Count_12_13','sp1_type_12_13','sp2_label_12_13', 'OriginalOID_12_13', 'sp2_type_12_13'), skip_nulls=True)
array_boundary3 = []
arcpy.AddMessage("10")
arcpy.AddMessage(array_records3)
array_ribbon_check3 = []
for i in array_records3:
    array_boundary3.append(i[0])
    array_ribbon_check3.append({i[0]:[i[1], i[2], i[3], i[4], i[5], i[6], i[7], i[8]]})


#print(array_ribbon_check)

########## Cases To Be checked ##############

array_1odp2_1odp3_1sp = []
array_1odp2_1odp3_2sp = []
array_1odp2_2odp3_1sp = []
array_1odp2_2odp3_2sp = []

array_odp2_1odp3 = []
array_odp2_2odp3 = []
for i in range(0, len(array_ribbon_check)):
    sec_boundary = array_boundary[i]
    print("secondary boundary "+str(sec_boundary))
    data = array_ribbon_check[i][array_boundary[i]]
    print(data)

    # 3. One ODP2, No ODP5, One splitter, One ODP3
    if data[0] == 1 and data[2] == 0 and data[1] == 1 and data[3] != 5 and data[4] == 5:
        arcpy.AddMessage("case 3")
        arcpy.AddMessage(sec_boundary)
        array_1odp2_1odp3_1sp.append([sec_boundary, data[3], data[4], data[5], data[6]])
    # 4. One ODP2, No ODP5, Two splitter, One ODP3
    elif data[0] == 1 and data[2] == 0 and data[1] == 1 and data[3] != 5 and data[4] != 5:
        arcpy.AddMessage("case 4")
        arcpy.AddMessage(sec_boundary)
        array_1odp2_1odp3_2sp.append([sec_boundary, data[3], data[4], data[5], data[6]])
    # 5. One ODP2, No ODP5, one splitter, Many ODP3
    elif data[0] == 1 and data[2] == 0 and data[1] > 1 and data[3] != 5 and data[4] == 5:
        arcpy.AddMessage("case 5")
        arcpy.AddMessage(sec_boundary)
        #array_1odp2_2odp3_1sp.append([sec_boundary, data[8]])
        array_1odp2_2odp3_1sp.append([sec_boundary, data[3], data[4], data[5], data[6], data[7], data[8]])
    # 6. One ODP2, No ODP5, two splitter, Many ODP3
    elif data[0] == 1 and data[2] == 0 and data[1] > 1 and data[3] != 5 and data[4] != 5:
        arcpy.AddMessage("case 6")
        arcpy.AddMessage(sec_boundary)
        array_1odp2_2odp3_2sp.append([sec_boundary, data[3], data[4], data[5], data[6], data[7], data[8]])
    # 7. No ODP2, One ODP5, No ODP3
    elif data[0] == 0 and data[2] == 0 and data[1] == 0:
        arcpy.AddMessage("case 7")
        arcpy.AddMessage(sec_boundary)
        #array_odp2_odp3.append(sec_boundary)
    # 8. No ODP2, One ODP5, One ODP3
    # elif data[0] == 0 and data[2] == 1 and data[1] == 1:
    #     arcpy.AddMessage("case 8")
    #     arcpy.AddMessage(sec_boundary)
    #     #array_odp2_1odp3.append(sec_boundary)
    # # 9. No ODP2, One ODP5, Many ODP3
    # elif data[0] == 0 and data[2] == 1 and data[1] > 1:
    #     arcpy.AddMessage("case 9")
    #     arcpy.AddMessage(sec_boundary)
    #     #array_odp2_2odp3.append(sec_boundary)
    else:
        arcpy.AddMessage("Some Error Occoured on "+ str(sec_boundary)+ " secondary boundary.")

####### Case 7, 8, 9
for i in range(0, len(array_ribbon_check1)):
    sec_boundary1 = array_boundary1[i]
    arcpy.AddMessage("secondary boundary "+str(sec_boundary1))
    data1 = array_ribbon_check1[i][array_boundary1[i]]
    print(data1)

    if data1[0] == 0 and data1[2] == 1 and data1[1] == 1:
        arcpy.AddMessage("case 8")
        arcpy.AddMessage(sec_boundary)
        array_odp2_1odp3.append([sec_boundary1, data1[3], data1[4], data1[5], data1[6]])
    # 9. No ODP2, One ODP5, Many ODP3
    elif data1[0] == 0 and data1[1] == 1 and data1[2] > 1:
        arcpy.AddMessage("case 9")
        array_odp2_2odp3.append([sec_boundary1, data1[3], data1[4], data1[5], data1[6]])
    else:
        arcpy.AddMessage("Some Error Occoured on "+ str(sec_boundary1)+ " the secondary boundary.")


array_odp1 = []
####### Case 10
print(array_ribbon_check3)
print(array_ribbon_check2)
for i in range(0, len(array_ribbon_check3)):
    sec_boundary3 = array_boundary3[i]
    print("secondary boundary "+str(sec_boundary3))
    data3 = array_ribbon_check3[i][array_boundary3[i]]
    print(data3)
    # 10. One ODP1, No ODP2,No ODP5, One splitter, No ODP3
    if data3[1] == 1 and data3[0] == 0 and data3[2] == 0 and data3[7] == 5 and data3[3] == 0:
        arcpy.AddMessage("case 10")
        arcpy.AddMessage(sec_boundary3)
        array_odp1.append([sec_boundary3, data3[4], data3[5]])
    else:
        arcpy.AddMessage("No Records found for case 10 on "+ str(sec_boundary3)+ " secondary boundary.")

array_odp1_1odp3 = []
array_odp1_2odp3 = []
####### Case 11, 12
for i in range(0, len(array_ribbon_check2)):
    sec_boundary2 = array_boundary2[i]
    print("secondary boundary "+str(sec_boundary2))
    data2 = array_ribbon_check2[i][array_boundary2[i]]
    print(data2)
    # 11. One ODP1,No ODP2, No ODP5, One splitter, One ODP3
    if data2[1] == 1 and data2[0] == 0 and data2[2] == 0 and data2[8] == 5 and data2[3] == 1:
        arcpy.AddMessage("case 11")
        arcpy.AddMessage(sec_boundary2)
        array_odp1_1odp3.append([sec_boundary2, data2[4], data2[5]])
    # 12. One ODP1,No ODP2, No ODP5, one splitter, Many ODP3
    elif data2[1] == 1 and data2[0] == 0 and data2[2] == 0 and data2[8] == 5 and data2[3] > 1:
        arcpy.AddMessage("case 12")
        arcpy.AddMessage(sec_boundary2)
        array_odp1_2odp3.append([sec_boundary2, data2[4], data2[5], data2[6], data2[7]])
    else:
        arcpy.AddMessage("No Records found for case 11/12 on "+ str(sec_boundary2)+ " secondary boundary.")

arcpy.SpatialJoin_analysis(ScName, FdName, "drop_splice_join", join_operation="JOIN_ONE_TO_MANY", join_type="KEEP_ALL", match_option="INTERSECT")
arcpy.SpatialJoin_analysis("secondary_boundary", "drop_splice_join", "drop_splice_boundary", join_operation="JOIN_ONE_TO_MANY", join_type="KEEP_ALL", match_option="INTERSECT")

#arcpy.SpatialJoin_analysis("drop_splice_join", FsName, "drop_splice_sheath_join", join_operation="JOIN_ONE_TO_ONE", join_type="KEEP_ALL", match_option="INTERSECT")

#arcpy.SpatialJoin_analysis("secondary_boundary", "drop_splice_sheath_join", "splitter_boundary", join_operation="JOIN_ONE_TO_ONE", join_type="KEEP_ALL", match_option="INTERSECT")

#arcpy.SpatialJoin_analysis("splitter_boundary", FsName, "splitter_boundary_fsheath", join_operation="JOIN_ONE_TO_ONE", join_type="KEEP_ALL", match_option="SHARE_A_LINE_SEGMENT_WITH")

array_drops_boundary_splitr = arcpy.da.FeatureClassToNumPyArray("drop_splice_boundary", ("OriginalID_D", "name", "drop_length"), skip_nulls=True)

#array_drops_boundary = arcpy.da.FeatureClassToNumPyArray("splitter_boundary_fsheath", ("OriginalID_D", "name", "Shape_Length"), skip_nulls=True)


######## Case 1 (One ODP2, No ODP5, One splitter, No ODP3) #############
print(array_1odp2_1sp)
if len(array_1odp2_1sp) > 0:
    for rec in array_1odp2_1sp:
        array_drop1 = []
        for data in array_drops_boundary_splitr:
            if str(rec[0]) == str(data[1]):
                array_drop1.append([data[0], data[2], rec[1], rec[2]])
        sort_array1 = sorted(array_drop1, key=lambda x: x[1], reverse=True)
        # print(array_drop)
        # sort_array = sorted(array_drop, key=lambda x: x[1], reverse=True)
        # print(sort_array1)
        counter_sp1 = 0

        for recs in sort_array1:
            with arcpy.da.UpdateCursor(localDrop, ['OriginalID_D', 'label']) as cur:
                for row in cur:
                    if row[0] == recs[0]:
                        if int(recs[2]) in (1, 2) and counter_sp1 < 4:
                            counter_sp1 = counter_sp1 + 1
                            label = str(recs[3]) + '/' + str(counter_sp1)
                            print(label)
                        elif int(recs[2]) in (3, 4) and counter_sp1 < 8:
                            counter_sp1 = counter_sp1 + 1
                            label = str(recs[3]) + '/' + str(counter_sp1)
                            print(label)
                        # elif int(rec[3]) in (1, 2) and counter_sp2 <= 4:
                        #     counter_sp2 = counter_sp2 + 1
                        #     label = rec[5] + '/' + str(counter_sp2)
                        #     print(label)
                        # elif int(rec[3]) in (3, 4) and counter_sp2 <= 8:
                        #     counter_sp2 = counter_sp2 + 1
                        #     label = rec[5] + '/' + str(counter_sp2)
                        #     print(label)
                        if len(label) > 17:
                            msg = "MoreThan17"
                            row[1] = msg
                        else:
                            row[1] = label
                    cur.updateRow(row)
            del cur
######## Case 2 (One ODP2, No ODP5, Two splitter, No ODP3) #############
if len(array_1odp2_2sp) > 0:
    for rec in array_1odp2_2sp:
        array_drop = []
        for data in array_drops_boundary_splitr:
            if str(rec[0]) == str(data[1]):
                array_drop.append([data[0], data[2], rec[1], rec[2],rec[3], rec[4]])
        sort_array = sorted(array_drop, key=lambda x: x[1], reverse=True)

        counter_sp1 = 0
        counter_sp2 = 0
        for recs in sort_array:
            with arcpy.da.UpdateCursor(localDrop, ['OriginalID_D', 'label']) as cur:
                for row in cur:
                    if row[0] == recs[0]:
                        if int(recs[2]) in (1, 2) and counter_sp1 < 4:
                            counter_sp1 = counter_sp1 + 1
                            label = str(recs[4]) + '/' + str(counter_sp1)

                        elif int(recs[2]) in (3, 4) and counter_sp1 < 8:
                            counter_sp1 = counter_sp1 + 1
                            label = str(recs[4]) + '/' + str(counter_sp1)

                        elif int(recs[3]) in (1, 2) and counter_sp2 < 4:
                            counter_sp2 = counter_sp2 + 1
                            label = str(recs[5]) + '/' + str(counter_sp2)

                        elif int(recs[3]) in (3, 4) and counter_sp2 < 8:
                            counter_sp2 = counter_sp2 + 1
                            label = str(recs[5]) + '/' + str(counter_sp2)
                        print(label)
                        if len(label) > 17:
                            msg = "MoreThan17"
                            row[1] = msg
                        else:
                            row[1] = label
                    cur.updateRow(row)
            del cur

######## Case 3 (One ODP2, No ODP5, One splitter, One ODP3) #############
if len(array_1odp2_1odp3_1sp) > 0:
    for rec in array_1odp2_1odp3_1sp:
        array_drop = []
        for data in array_drops_boundary_splitr:
            if str(rec[0]) == str(data[1]):
                array_drop.append([data[0], data[2], rec[1], rec[2],rec[3], rec[4]])

                print(array_drop)
        sort_array = sorted(array_drop, key=lambda x: x[1], reverse=True)
        arcpy.AddMessage("sorted array")
        print(sort_array)
        unique_sorted_list = remove_dup_by_first_element(sort_array)
        arcpy.AddMessage(unique_sorted_list)
        counter_sp1 = 0
        for recs in unique_sorted_list:
            with arcpy.da.UpdateCursor(localDrop, ['OriginalID_D', 'label']) as cur:
                for row in cur:
                    if row[0] == int(recs[0]):
                        if int(recs[2]) in (1, 2) and counter_sp1 < 4:
                            counter_sp1 = counter_sp1 + 1
                            label = recs[4] + '/' + str(counter_sp1)
                        elif int(recs[2]) in (3, 4) and counter_sp1 < 8:
                            counter_sp1 = counter_sp1 + 1
                            label = recs[4] + '/' + str(counter_sp1)
                        print("object id "+str(row[0]))
                        print(label)
                        if len(label) > 17:
                            msg = "MoreThan17"
                            row[1] = msg
                        else:
                            row[1] = label
                    cur.updateRow(row)
            del cur
######## Case 4 (One ODP2, No ODP5, Two splitter, One ODP3) #############
if len(array_1odp2_1odp3_2sp) > 0:
    for rec in array_1odp2_1odp3_2sp:
        array_drop = []
        for data in array_drops_boundary_splitr:
            if str(rec[0]) == str(data[1]):
                array_drop.append([data[0], data[2], rec[1], rec[2],rec[3], rec[4]])

                #print(array_drop)
        sort_array = sorted(array_drop, key=lambda x: x[1], reverse=True)
        # print("sorted array")
        # print(sort_array)
        unique_sorted_list = remove_dup_by_first_element(sort_array)
        arcpy.AddMessage(unique_sorted_list)
        counter_sp1 = 0
        counter_sp2 = 0
        for recs in unique_sorted_list:
            with arcpy.da.UpdateCursor(localDrop, ['OriginalID_D', 'label']) as cur:
                for row in cur:
                    if row[0] == int(recs[0]):
                        if int(recs[2]) in (1, 2) and counter_sp1 < 4:
                            counter_sp1 = counter_sp1 + 1
                            label = recs[4] + '/' + str(counter_sp1)
                        elif int(recs[2]) in (3, 4) and counter_sp1 < 8:
                            counter_sp1 = counter_sp1 + 1
                            label = recs[4] + '/' + str(counter_sp1)
                        elif int(recs[3]) in (1, 2) and counter_sp2 < 4:
                            counter_sp2 = counter_sp2 + 1
                            label = recs[5] + '/' + str(counter_sp2)
                        elif int(recs[3]) in (3, 4) and counter_sp2 < 8:
                            counter_sp2 = counter_sp2 + 1
                            label = recs[5] + '/' + str(counter_sp2)
                        print("object id "+str(row[0]))
                        print(label)
                        if len(label) > 17:
                            msg = "MoreThan17"
                            row[1] = msg
                        else:
                            row[1] = label
                    cur.updateRow(row)
            del cur
####################### Case 5 (One ODP2, No ODP5, one splitter, Many ODP3) ##############################
print("Case 5 Data")
if len(array_1odp2_2odp3_1sp) > 0:
    for rec in array_1odp2_2odp3_1sp:
        array_drop = []
        for data in array_drops_boundary_splitr:
            if str(rec[0]) == str(data[1]):
                sp2_id = rec[5]
                sp3_id = rec[6]
                geom_sp2 = get_geom(array_sc, sp2_id)
                geom_sp3 = get_geom(array_sc, sp3_id)
                # print("geom")
                # print(geom_sp2)
                geom = Point({"x": geom_sp2[0], "y": geom_sp2[1], "spatialReference": {"wkid": spatial_ref.factoryCode}})
                geom2 = Point({"x": geom_sp3[0], "y": geom_sp3[1], "spatialReference": {"wkid": spatial_ref.factoryCode}})
                distance = geom.angle_distance_to(second_geometry=geom2, method="PLANAR")
                #print("distance - "+ str(distance[1]))
                tot_distance = int(distance[1]) + int(data[2])
                #print("total distance - " + str(tot_distance))
                array_drop.append([data[0], tot_distance, rec[1], rec[2],rec[3], rec[4], rec[1]])

                #print(array_drop)
        sort_array = sorted(array_drop, key=lambda x: x[1], reverse=True)
        arcpy.AddMessage("sorted array 5")
        # print(sort_array)
        unique_sorted_list = remove_dup_by_first_element(sort_array)
        arcpy.AddMessage(unique_sorted_list)
        counter_sp1 = 0
        for recs in unique_sorted_list:
            with arcpy.da.UpdateCursor(localDrop, ['OriginalID_D', 'label']) as cur:
                for row in cur:
                    if row[0] == int(recs[0]):
                        if int(recs[2]) in (1, 2) and counter_sp1 < 4:
                            counter_sp1 = counter_sp1 + 1
                            label = recs[4] + '/' + str(counter_sp1)
                        elif int(recs[2]) in (3, 4) and counter_sp1 < 8:
                            counter_sp1 = counter_sp1 + 1
                            label = recs[4] + '/' + str(counter_sp1)
                        print("object id "+str(row[0]))
                        print(label)
                        if len(label) > 17:
                            msg = "MoreThan17"
                            row[1] = msg
                        else:
                            row[1] = label
                    cur.updateRow(row)
            del cur

##################### Case 6 (One ODP2, No ODP5, two splitter, Many ODP3)  ###############################
if len(array_1odp2_2odp3_2sp) > 0:
    for rec in array_1odp2_2odp3_2sp:
        array_drop = []
        for data in array_drops_boundary_splitr:
            if str(rec[0]) == str(data[1]):
                sp2_id = rec[5]
                sp3_id = rec[6]
                geom_sp2 = get_geom(array_sc, sp2_id)
                geom_sp3 = get_geom(array_sc, sp3_id)
                # print("geom")
                # print(geom_sp2)
                geom = Point(
                    {"x": geom_sp2[0], "y": geom_sp2[1], "spatialReference": {"wkid": spatial_ref.factoryCode}})
                geom2 = Point(
                    {"x": geom_sp3[0], "y": geom_sp3[1], "spatialReference": {"wkid": spatial_ref.factoryCode}})
                distance = geom.angle_distance_to(second_geometry=geom2, method="PLANAR")
                # print("distance - "+ str(distance[1]))
                tot_distance = int(distance[1]) + int(data[2])
                # print("total distance - " + str(tot_distance))
                array_drop.append([data[0], tot_distance, rec[1], rec[2], rec[3], rec[4], rec[1]])

                #print(array_drop)
        sort_array = sorted(array_drop, key=lambda x: x[1], reverse=True)
        print("sorted array 6")
        # print(sort_array)
        unique_sorted_list = remove_dup_by_first_element(sort_array)
        arcpy.AddMessage(unique_sorted_list)
        counter_sp1 = 0
        counter_sp2 = 0
        for rec in unique_sorted_list:
            with arcpy.da.UpdateCursor(localDrop, ['OriginalID_D', 'label']) as cur:
                for row in cur:
                    if row[0] == int(rec[0]):
                        if int(rec[2]) in (1, 2) and counter_sp1 < 4:
                            counter_sp1 = counter_sp1 + 1
                            label = rec[4] + '/' + str(counter_sp1)
                        elif int(rec[2]) in (3, 4) and counter_sp1 < 8:
                            counter_sp1 = counter_sp1 + 1
                            label = rec[4] + '/' + str(counter_sp1)
                        elif int(rec[3]) in (1, 2) and counter_sp2 < 4:
                            counter_sp2 = counter_sp2 + 1
                            label = rec[5] + '/' + str(counter_sp2)
                        elif int(rec[3]) in (3, 4) and counter_sp2 < 8:
                            counter_sp2 = counter_sp2 + 1
                            label = rec[5] + '/' + str(counter_sp2)
                        print("object id "+str(row[0]))
                        print(label)
                        if len(label) > 17:
                            msg = "MoreThan17"
                            row[1] = msg
                        else:
                            row[1] = label
                    cur.updateRow(row)
            del cur
######## Case 7 (No ODP2,One ODP5 ,No ODP3) ##################
arcpy.AddMessage("CASE 7 PROCESS START")
if len(array_odp2_odp3) > 0:
    for rec in array_odp2_odp3:
        array_drop7 = []
        for data in array_drops_boundary_splitr:
            if str(rec[0]) == str(data[1]):
                array_drop7.append([data[0], data[2], rec[1], rec[2]])
        sort_array7 = sorted(array_drop7, key=lambda x: x[1], reverse=True)
        # print(array_drop)
        # sort_array = sorted(array_drop, key=lambda x: x[1], reverse=True)
        arcpy.AddMessage(sort_array1)
        counter_sp2 = 0

        for recs in sort_array7:
            with arcpy.da.UpdateCursor(localDrop, ['OriginalID_D', 'label']) as cur:
                for row in cur:
                    if row[0] == recs[0]:
                        # print(int(row[0]))
                        # print(int(rec[0]))
                        if int(recs[2]) in (1, 2) and counter_sp2 < 4:
                            counter_sp2 = counter_sp2 + 1
                            label = str(recs[3]) + '/' + str(counter_sp2)
                            print(label)
                        elif int(recs[2]) in (3, 4) and counter_sp2 < 8:
                            counter_sp2 = counter_sp2 + 1
                            label = str(recs[3]) + '/' + str(counter_sp2)
                            print(label)
                        # elif int(rec[3]) in (1, 2) and counter_sp2 <= 4:
                        #     counter_sp2 = counter_sp2 + 1
                        #     label = rec[5] + '/' + str(counter_sp2)
                        #     print(label)
                        # elif int(rec[3]) in (3, 4) and counter_sp2 <= 8:
                        #     counter_sp2 = counter_sp2 + 1
                        #     label = rec[5] + '/' + str(counter_sp2)
                        #     print(label)
                        if len(label) > 17:
                            msg = "MoreThan17"
                            row[1] = msg
                        else:
                            row[1] = label
                    cur.updateRow(row)
            del cur
arcpy.AddMessage("CASE 7 Process End")

####################### Case 8 (No ODP2, One ODP5, One ODP3) ########################
if len(array_odp2_1odp3) > 0:
    for rec in array_odp2_1odp3:
        array_drop8 = []
        for data in array_drops_boundary_splitr:
            if str(rec[0]) == str(data[1]):
                sp5_id = rec[3]
                sp3_id = rec[4]
                geom_sp5 = get_geom(array_sc, sp5_id)
                geom_sp3 = get_geom(array_sc, sp3_id)
                # print("geom")
                # print(geom_sp2)
                geom = Point(
                    {"x": geom_sp5[0], "y": geom_sp5[1], "spatialReference": {"wkid": spatial_ref.factoryCode}})
                geom2 = Point(
                    {"x": geom_sp3[0], "y": geom_sp3[1], "spatialReference": {"wkid": spatial_ref.factoryCode}})
                distance = geom.angle_distance_to(second_geometry=geom2, method="PLANAR")
                # print("distance - "+ str(distance[1]))
                tot_distance = int(distance[1]) + int(data[2])
                array_drop8.append([data[0], tot_distance, rec[1], rec[2]])
        sort_array8 = sorted(array_drop8, key=lambda x: x[1], reverse=True)
        # print(array_drop)
        # sort_array = sorted(array_drop, key=lambda x: x[1], reverse=True)
        arcpy.AddMessage(sort_array8)
        counter_sp2 = 0

        for recs in sort_array8:
            with arcpy.da.UpdateCursor(localDrop, ['OriginalID_D', 'label']) as cur:
                for row in cur:
                    if row[0] == recs[0]:
                        if int(recs[2]) in (1, 2) and counter_sp2 < 4:
                            counter_sp2 = counter_sp2 + 1
                            label = str(recs[3]) + '/' + str(counter_sp2)
                            print(label)
                        elif int(recs[2]) in (3, 4) and counter_sp2 < 8:
                            counter_sp2 = counter_sp2 + 1
                            label = str(recs[3]) + '/' + str(counter_sp2)
                            print(label)
                        if len(label) > 17:
                            msg = "MoreThan17"
                            row[1] = msg
                        else:
                            row[1] = label
                    cur.updateRow(row)
            del cur
####################### Case 9 (No ODP2, One ODP5, One ODP3) ########################
if len(array_odp2_2odp3) > 0:
    for rec in array_odp2_2odp3:
        array_drop9 = []
        for data in array_drops_boundary_splitr:
            if str(rec[0]) == str(data[1]):
                sp5_id = rec[3]
                sp3_id = rec[4]
                geom_sp5 = get_geom(array_sc, sp5_id)
                geom_sp3 = get_geom(array_sc, sp3_id)
                # print("geom")
                # print(geom_sp2)
                geom = Point(
                    {"x": geom_sp5[0], "y": geom_sp5[1], "spatialReference": {"wkid": spatial_ref.factoryCode}})
                geom2 = Point(
                    {"x": geom_sp3[0], "y": geom_sp3[1], "spatialReference": {"wkid": spatial_ref.factoryCode}})
                distance = geom.angle_distance_to(second_geometry=geom2, method="PLANAR")
                # print("distance - "+ str(distance[1]))
                tot_distance = int(distance[1]) + int(data[2])
                array_drop9.append([data[0], tot_distance, rec[1], rec[2]])
        sort_array9 = sorted(array_drop9, key=lambda x: x[1], reverse=True)
        # print(array_drop)
        # sort_array = sorted(array_drop, key=lambda x: x[1], reverse=True)
        print(sort_array9)
        counter_sp2 = 0

        for recs in sort_array9:
            with arcpy.da.UpdateCursor(localDrop, ['OriginalID_D', 'label']) as cur:
                for row in cur:
                    if row[0] == recs[0]:
                        if int(recs[2]) in (1, 2) and counter_sp2 < 4:
                            counter_sp2 = counter_sp2 + 1
                            label = str(recs[3]) + '/' + str(counter_sp2)
                            print(label)
                        elif int(recs[2]) in (3, 4) and counter_sp2 < 8:
                            counter_sp2 = counter_sp2 + 1
                            label = str(recs[3]) + '/' + str(counter_sp2)
                            print(label)
                        if len(label) > 17:
                            msg = "MoreThan17"
                            row[1] = msg
                        else:
                            row[1] = label
                    cur.updateRow(row)
            del cur
####################### Case 10 (One ODP1, No ODP2,No ODP5, One splitter, No ODP3) ########################
arcpy.AddMessage("array_odp1")
arcpy.AddMessage(array_odp1)
if len(array_odp1) > 0:
    for rec in array_odp1:
        array_drop10 = []
        for data in array_drops_boundary_splitr:
            if str(rec[0]) == str(data[1]):
                array_drop10.append([data[0], data[2], rec[1], rec[0]])
        sort_array10 = sorted(array_drop10, key=lambda x: x[1], reverse=True)
        counter_sp1 = 0

        arcpy.AddMessage("sort_array10")
        arcpy.AddMessage(sort_array10)

        for recs in sort_array10:
            with arcpy.da.UpdateCursor(localDrop, ['OriginalID_D', 'label']) as cur:
                for row in cur:
                    if row[0] == recs[0]:
                        arcpy.AddMessage("case 10 object id")
                        arcpy.AddMessage(str(row[0]))
                        # print(int(rec[0]))
                        if int(recs[2]) in (1, 2) and counter_sp1 < 4:
                            counter_sp1 = counter_sp1 + 1
                            label = str(recs[3]) + '/' + str(counter_sp1) + '/-'
                            # print(label)
                        elif int(recs[2]) in (3, 4) and counter_sp1 < 8:
                            counter_sp1 = counter_sp1 + 1
                            label = str(recs[3]) + '/' + str(counter_sp1) + '/-'
                        if len(label) > 17:
                            msg = "MoreThan17"
                            row[1] = msg
                        else:
                            row[1] = label
                    cur.updateRow(row)
            del cur

####################### Case 11 (One ODP1,No ODP2, No ODP5, One splitter, One ODP3) ########################
arcpy.AddMessage("array_odp1_1odp3")
arcpy.AddMessage(array_odp1_1odp3)
if len(array_odp1_1odp3) > 0:
    for rec in array_odp1_1odp3:
        array_drop11 = []
        for data in array_drops_boundary_splitr:
            if str(rec[0]) == str(data[1]):
                array_drop11.append([data[0], data[2], rec[1], rec[0]])
        sort_array11 = sorted(array_drop11, key=lambda x: x[1], reverse=True)
        counter_sp1 = 0

        arcpy.AddMessage("sort_array11")
        arcpy.AddMessage(sort_array11)

        for recs in sort_array11:
            with arcpy.da.UpdateCursor(localDrop, ['OriginalID_D', 'label']) as cur:
                for row in cur:
                    if row[0] == recs[0]:
                        arcpy.AddMessage("case 11 object id")
                        arcpy.AddMessage(str(row[0]))
                        # print(int(rec[0]))
                        if int(recs[2]) in (1, 2) and counter_sp1 < 4:
                            counter_sp1 = counter_sp1 + 1
                            label = str(recs[3]) + '/' + str(counter_sp1) + '/-'
                            # print(label)
                        elif int(recs[2]) in (3, 4) and counter_sp1 < 8:
                            counter_sp1 = counter_sp1 + 1
                            label = str(recs[3]) + '/' + str(counter_sp1) + '/-'
                        if len(label) > 17:
                            msg = "MoreThan17"
                            row[1] = msg
                        else:
                            row[1] = label
                    cur.updateRow(row)
            del cur

####################### Case 12 (One ODP1,No ODP2, No ODP5, one splitter, Many ODP3) ########################
arcpy.AddMessage("array_odp1_2odp3")
arcpy.AddMessage(array_odp1_2odp3)
if len(array_odp1_2odp3) > 0:
    for rec in array_odp1_2odp3:
        array_drop12 = []
        for data in array_drops_boundary_splitr:
            if str(rec[0]) == str(data[1]):
                sp1_id = rec[3]
                sp3_id = rec[4]
                geom_sp1 = get_geom(array_sc, sp1_id)
                geom_sp3 = get_geom(array_sc, sp3_id)
                # print("geom")
                # print(geom_sp2)
                geom = Point({"x": geom_sp1[0], "y": geom_sp1[1], "spatialReference": {"wkid": spatial_ref.factoryCode}})
                geom2 = Point({"x": geom_sp3[0], "y": geom_sp3[1], "spatialReference": {"wkid": spatial_ref.factoryCode}})
                distance = geom.angle_distance_to(second_geometry=geom2, method="PLANAR")
                # print("distance - "+ str(distance[1]))
                tot_distance = int(distance[1]) + int(data[2])
                # print("total distance - " + str(tot_distance))
                array_drop12.append([data[0], tot_distance, rec[1], rec[0]])


        sort_array12 = sorted(array_drop12, key=lambda x: x[1], reverse=True)

        arcpy.AddMessage("sort_array12")
        arcpy.AddMessage(sort_array12)
        counter_sp1 = 0

        for recs in sort_array12:
            with arcpy.da.UpdateCursor(localDrop, ['OriginalID_D', 'label']) as cur:
                for row in cur:
                    if row[0] == recs[0]:
                        arcpy.AddMessage("case 12 object id")
                        arcpy.AddMessage(str(row[0]))
                        # print(int(rec[0]))
                        if int(recs[2]) in (1, 2) and counter_sp1 < 4:
                            counter_sp1 = counter_sp1 + 1
                            label = str(recs[3]) + '/' + str(counter_sp1) + '/-'
                            # print(label)
                        elif int(recs[2]) in (3, 4) and counter_sp1 < 8:
                            counter_sp1 = counter_sp1 + 1
                            label = str(recs[3]) + '/' + str(counter_sp1) + '/-'
                        if len(label) > 17:
                            msg = "MoreThan17"
                            row[1] = msg
                        else:
                            row[1] = label
                    cur.updateRow(row)
            del cur


################### Delete Unwanted Files from Local Server ################



if arcpy.Exists("drop_splice_boundary"):
    arcpy.Delete_management("drop_splice_boundary")
if arcpy.Exists("drop_splice_join"):
    arcpy.Delete_management("drop_splice_join")
if arcpy.Exists("secondary_boundary"):
    arcpy.Delete_management("secondary_boundary")
if arcpy.Exists("boundary_splitter235_join"):
    arcpy.Delete_management("boundary_splitter235_join")
if arcpy.Exists("secondary_splitter_5"):
    arcpy.Delete_management("secondary_splitter_5")
if arcpy.Exists("boundary_splitter23_join"):
    arcpy.Delete_management("boundary_splitter23_join")
if arcpy.Exists("secondary_splitter_3"):
    arcpy.Delete_management("secondary_splitter_3")
if arcpy.Exists("boundary_splitter2_join"):
    arcpy.Delete_management("boundary_splitter2_join")
if arcpy.Exists("secondary_splitter_2"):
    arcpy.Delete_management("secondary_splitter_2")
if arcpy.Exists("secondary_boundary"):
    arcpy.Delete_management("secondary_boundary")
if arcpy.Exists("boundary_splitter2351_join"):
    arcpy.Delete_management("boundary_splitter2351_join")
if arcpy.Exists("primary_splitter"):
    arcpy.Delete_management("primary_splitter")


########################## Write To Server Data #############################
arr_drop_record = []
try:
    #arr_drop_data = arcpy.da.FeatureClassToNumPyArray(localDrop, ("OBJECTID", "label"), skip_nulls=True)
    arr_drop_data = arcpy.da.FeatureClassToNumPyArray(localDrop, ("GlobalID", "label"), skip_nulls=True)
    arcpy.AddMessage(len(arr_drop_data))
    # for data in arr_drop_data:
    #     if data[1] != "":
    #         arr_drop_record.append([data[0], data[1]])

    fset = fl.query()  # Return a set of features from the layer
    features = fset.features  # Get features object
    if len(features) == 0:
        print("No features found in '%s' layer", warning=True)
        print('Exiting ...')
        exit()
    else:
        #print("Extracted %s features from '%s' layer" % (len(features)))
        pass

    dictLengths = {}
    for item in features:
        gID = item.attributes["GlobalID"]
        match_str = '{'+gID+'}'
        arcpy.AddMessage(match_str)
        gID1 = match_str.upper()
        arcpy.AddMessage(gID1)
        for data in arr_drop_data:
            if str(gID1) == str(data[0]):
                dictLengths[gID] = data[1]

    arcpy.AddMessage(len(dictLengths))
    # Update feature layer
    if len(list(dictLengths.keys())) > 0:
        ApplyUpdates(fl, features, ["label"], dictLengths)
    else:
        arcpy.AddMessage("No records to process")
# ApplyUpdates(fl, features, ['label'])
# sts = ApplyUpdates(arr_drop_record, fl, ['label'])
# ########## Update Live Data ############
# if sts == True:
#    arcpy.AddMessage("Drop labeling has been done.")
except Exception as ex:
    arcpy.AddMessage(ex)

if arcpy.Exists(localSplice):
    arcpy.Delete_management(localSplice)
if arcpy.Exists(localDrop):
    arcpy.Delete_management(localDrop)
if arcpy.Exists(localBoundary):
    arcpy.Delete_management(localBoundary)

