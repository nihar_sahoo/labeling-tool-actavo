ToolBox Name: NetworkLabling_v1o4.tbx

ToolSet Name: Overall

Script Name: Secondary Splitter Labeling New_13.10.2021.py

Input: Splice Closure, Fibre Sheath

Tool test: 4. Secondary Splitter Labels

Output: Splice_Closure (layer present in the Input geodatabse layer present in the Input geodatabse which is the live layer)

Field(s) to look for: sp1_label, sp2_label (Splice_Closure)

Important Note:
1. Tool takes 40 minutes approximately to run based on the number of features.
2. Tool is dependent on "sp1_label" field of Primar splitters, in case "sp1_label" field is not filled or not updated then "sp2_label" wont be updated correctly.
3. After running the tool remove and add the entire geodatabase again in the TOC to view the changes.
4. In some cases Ribbon field will not be updated correctly for both Fibre_Sheath and Splice_Closures due to geometry issues, those cases need to be inspected and checked for 
	-snapping issue (both Fibre_sheath start and end points with splice_closure)
	-direction issue (fibre_sheath cable is in opposite direction to what it should be).
5. Snap Checks between Splice & Fibre Sheath should be proper.
6. Checks added to show message where primary splitter "sp1_label" field for the splice_closure is filled or not.
7. No Overlapping Fibre Sheath should be there on the data else correct secondary splitter labeling will not happen.